# -------------------------------------------------------------------------------
# Name:        modClasses
# Purpose:     class definitions (message, field, model,...)
#
# Author:      nlrgo
#
# Created:     12-10-2013
# Copyright:   (c) nlrgo 2013
# Licence:     <your licence>
# -------------------------------------------------------------------------------
import io
import sys
import collections
import regex as re
import time
import ctypes

from modFunc import *  # possibly used by msgClass.calculate()
from LW_BUILD_INFO import app_name, version, build

# user procedure (to be re-defined by user)
def user_compile(msg):
    pass

def user_calculate(msg):
    pass

def user_finalize(msg):
    pass

def exec_script(name):
    """run complete query/python code if script name starts with 'name' """

    for scr in common.scripts:
        if scr.name.startswith(name):
        # if re.fullmatch(re.escape(name), scr.name):
            common.set_info("Running {} script: '{}'".format(scr.type, scr.name))

            try:
                exec('from common import *', globals())  # load common names to global

                if scr.type == 'query':  # execute SQL
                    common.query(scr.sql, scr.name)

                elif scr.type == 'python':  # execute python script
                    exec_python(scr.sql)

            except Exception as err:
                common.set_error("exec_script('{}'): {}".format(name, err))

def exec_code(code, type, table=''):
    """run query/python 'code' ('table' is optional) """
    txt = code.replace('\n', r'\n')
    common.set_info("Running {} code: '{}{}'".format(type, txt[:75], txt[75:] and '(...)'))
    # common.set_info("Running {} code: '{}   ~~~".format(type, code[:75].replace('\n', r'\n')))

    try:
        exec('from common import *', globals())  # load common names to global

        if type == 'query':  # execute SQL
            common.query(code, table)

        elif type == 'python':  # execute python script
            # exec(code, globals())  # execute the script
            exec_python(code)
    except Exception as err:
        common.set_error("exec_code('{}'): {}".format(type, err))

def exec_python(code):
    """execute python 'code' """
    exec('from common import *', globals())  # load common names to global
    try:
        res = eval(code)
        if res:             # print result (if any)
            print(res)
    except:                 # execute the script
        exec(code, globals())

def intervalClass(wait):
    """Helper generator, returns True if last call was more then 'wait' time ago"""

    tim = time.clock()
    while True:
        if time.clock() > tim + wait:
            tim = time.clock()
            yield True
        else:
            yield False

class settingsClass:
    values = {}

    def __init__(self, name='setting'):
        self.type = 'settings'
        self.name = name
        self.values = {}

    def __setstate__(self, state):  # for backward compatibility
        set = state['values']
        if not 'xml_enabled' in set: set['xml_enabled'] = True  # add attribute
        if not 'theme' in set: set['theme'] = 'Default'  # add attribute
        if not 'debug' in set: set['debug'] = 'True'  # add attribute

        self.__dict__.update(state)

    def __repr__(self):
        return "<Set: name='{}', {} values>".format(self.name, str(len(self.values)))

class locationClass:
    def __init__(self, name='location'):
        self.type = 'location'
        self.name = name
        self.enabled = True

    def __repr__(self):
        base = os.path.basename(self.name)
        enabled_stt = 'NAME' if self.enabled else 'name'
        return "<Loc: {}='{}'>".format(enabled_stt, str(base))

class fileClass:
    def __init__(self, path):
        self.type = 'file'
        self.path = path
        self.folder = ''
        self.name = ''
        self.member = ''
        self.size = 0
        self.growth = 0
        self.time = 0
        self.hnd = 0
        self.chunk = ''

    def __repr__(self):
        return "<File: name='{}', hnd={}, size={}, growth={}, time={}".format(self.name, self.hnd, self. size, self.growth, self.time)

    def close(self):
        if self.hnd:
            self.hnd.close()
            self.hnd = 0

class queryClass:
    def __init__(self, name='script'):
        self.type = 'query'
        self.name = name
        self.enabled = True
        self.sql = "SELECT * FROM "

    def __repr__(self):

        return "<Scr: name='{}'>".format(self.name)

class partClass():
	def __getattr__(self, name):
		return self.__dict__.get(name, '')

class fldClass:
    def __init__(self, name='field'):
        self.name = name
        self.id = ''    # field id
        self.edit = ''  # field calculation

        self._editc = []        # compiled regex, expressions or generators
        self.values = ('',)*4   # values from part
        self.value = ''         # calculated value
        self.hidden = False

    def __getstate__(self):  # pickle
        state = {k:v for k,v in self.__dict__.items() if k in ('name','id','edit')}     # only save configuration atrributes

        return state

    def __setstate__(self, state):  # unpickle
        self.__init__()     # init field variables

        if not 'edit' in state and '_edit' in state:
            state['edit'] = '/n'.join(state['_edit'])   # convert attribute
        self.__dict__.update(state)

    def __repr__(self):
        return "<Fld: name='" + self.name + "', edit=" + repr(self.edit) + ", value='" + self.value + "'>"

    def __getitem__(self, index):
        """return value fld[index]"""
        try:
            return self.values[index]
        except:
            return "Invalid index: '{}'".format(index)

class msgClass:
    def __init__(self, name='line'):
        self.name = name
        self.trigger = ''  # message id/selection
        self.edit = ''      # message calculations
        self.type = 'line'
        self.enabled = True
        self.wanted = True
        self.fields = []  # list of field objects

        self._trigger = []  # private list
        self._editc = []    # compiled regex, expressions or generators
        self.parts = []     # data parts
        self.range = ()     # beg,end of message
        # self.data = {}
        self.value = ''  # message text
        self.start = 0  # start position in file
        self.end = 0    # end position in file
        self.cnt = 0

        self.fields.append(fldClass('Name'))
        self.fields[0].edit = '=msg.name'
        # self.fields.append(fldClass('field'))
        self.globals = globals()

    def __getstate__(self):  # pickle
        state = {k:v for k,v in self.__dict__.items() if k in ('enabled', 'wanted', 'name','type','trigger','edit','fields')}  # only save configuration atrributes

        return state

    def __setstate__(self, state):  # unpickle
        self.__init__()     # init message variables

        if 'trigger' in state and type(state['trigger'])==list:
                state['trigger'] = state['trigger'][0]

        self.__dict__.update(state)

    def __repr__(self):
        enabled_stt = 'NAME' if self.enabled else 'name'
        return "<Msg: {}='{}', fields={}, type='{}'>".format(enabled_stt, self.name, len(self.fields), self.type)

    def __getitem__(self, keyindex):
        """return field msg['FieldName'] or msg[index]"""
        try:
            return next(fld for fld in self.fields if keyindex == fld.name)
        except:
            try:
                return self.fields[keyindex]
            except:
                return "Invalid key/index: '{}'".format(keyindex)

    def do_compile(self, type=''):

        if type:
            self.type = type

        try:
            user_compile(self)  # call user defined procedure (if any)
        except Exception as err:
            common.set_error('user_compile(): {}'.format(err))

        # set message parser
        if self.type == 'line':
            self.get_iter = self.get_regex_messages

            trig = self.trigger[1:] if self.trigger.startswith('@') else self.trigger   # remove possible '@'
            self._trigger = re.compile('.*(' + trig + ').*\n?', re.MULTILINE | re.V1)  # extend trigger and compile

        elif self.type == 'multi':
            self.get_iter = self.get_regex_messages

            trig = self.trigger[1:] if self.trigger.startswith('@') else self.trigger   # remove possible '@'

            beg_end = trig.split(',')
            trig = beg_end[0].strip() + '.*?' + beg_end[1].strip() if len(beg_end) == 2 else beg_end[0].strip()
            self._trigger = re.compile(trig, re.MULTILINE + re.DOTALL)  # compile with dot_all

        elif self.type == 'xml':
            self.get_iter = self.get_xml_messages

            trig = self.trigger[1:] if self.trigger.startswith('=') else self.trigger   # remove possible '='
            self._trigger = re.compile(trig)  # compile

        # set info fields
        show_chks = common.settings.get('show_info', [False]*6)
        for show, info in zip(reversed(show_chks), (['file.path','msg.value','msg.offset','msg.size','file.name','file.folder'])):

            fld = next((fld for fld in self.fields if fld.name==info), None)

            if fld and not show:            # info field exists but shouldn't -> remove
                self.fields.remove(fld)

            if not fld and show:        # info fields doesn't exist but should -> insert
                new = fldClass(info)
                new.edit = '=' + info
                self.fields.insert(1, new)


        # compile message
        msg_edit_list = [stp.strip() for stp in self.edit.split('\n')]

        self._editc = []
        for stp in msg_edit_list:
            if not stp: continue  # skip empty step

            try:
                if stp.isdigit():  # is digit
                    code = re.compile('.{0,' + stp + '}')

                elif stp.startswith('='):  # is expression
                    code = compile(stp[1:], '<exp>', 'eval')

                elif stp.startswith('@'):  # is generator
                    code = compile(stp[1:], '<gen>', 'eval')

                elif len(stp) == 1:  # is delimiter ([^stp]*?)(?:trig|$)
                    code = re.compile('([^' + re.escape(stp) + ']*?)(?:' + re.escape(stp) + '|$)')

                else:  # is regex
                    code = re.compile(stp)

                self._editc.append(code)

            except:
                exc_info = 'Compile error: ' + str(sys.exc_info()[1])
                self._editc = [compile("'" + exc_info + "'", '<exp>', 'eval')]


        # compile fields
        for fld in self.fields:
            fld_edit_list = [stp.strip() for stp in fld.edit.split('\n')]

            fld._editc = []
            for stp in fld_edit_list:
                if not stp: continue  # skip empty step

                try:
                    if stp.isdigit():  # is digit
                        code = re.compile('.{0,' + stp + '}')

                    elif stp.startswith('='):  # is expression
                        code = compile(stp[1:], '<exp>', 'eval')

                    elif stp.startswith('@'):  # is generator
                        code = compile(stp[1:], '<gen>', 'eval')

                    elif len(stp) == 1:  # is delimiter ([^stp]*?)(?:stp|$)
                        code = re.compile('([^' + re.escape(stp) + ']*?)(?:' + re.escape(stp) + '|$)')

                    else:  # is regex
                        code = re.compile(stp)

                    fld._editc.append(code)

                except:
                    exc_info = 'Compile error: ' + str(sys.exc_info()[1])
                    fld._editc = [compile("'" + exc_info + "'", '<exp>', 'eval')]

    def calculate_message(self):
        """calculate message"""

        msg = self
        self.offset = common.file_index_base + self.range[0]
        self.size = len(self.value)

        try:
            user_calculate(self)  # call user defined procedure (if any)
        except Exception as err:
            common.set_error('user_calculate(): {}'.format(err))

        for _stp in self._editc:    # for each step

            try:                            # regular expression
                res = _stp.search(str(self.value))
                if res:
                    self.value = res.group(len(res.groups()))
                else:
                    self.value = 'No result'
            except:
                if _stp.co_filename == '<exp>':  # python expression
                    try:
                        self.value = str(eval(_stp))
                    except:
                        self.value = 'Error in expression: ' + str(sys.exc_info()[1])

                elif _stp.co_filename == '<gen>':  # python generator
                    try:
                        yield from eval(_stp)
                    except:
                        self.value = 'Error in generator: ' + str(sys.exc_info()[1])

                    return  # generator yields new message(s) so don't yield original message

        yield from self.calculate_fields()

    def calculate_fields(self):
        """calculate field values"""

        file = common.current_file
        prev = common.prev_msg
        msg = self

        for fld in msg.fields:      # reset field values
            fld.values = ('',)*4
            fld.value = ''

        # assign parts (if any) to fields
        pointer = {fld.id:fld for fld in self.fields}   # create pointer
        for part in self.parts:
            id = part['id']
            try:            # field with id already exists
                fld = pointer[id]
            except:         # create field and insert into pointer
                name = unique_name(part['name'], self.fields)
                fld = fldClass(name)    # create new field dynamical
                fld.id = id
                pointer[id] = fld
                self.fields.append(fld)
                common.set_debug('new field', fld.name)

            fld.values = part['values']

        # calculate each field
        pos = 0
        tail = self.value
        for fld_idx, fld in enumerate(self.fields):
            idx = fld_idx

            if fld.id:                      # copy field value
                val = fld.values[0] if fld.values else ''
            else:                           # copy (remainder of) message value
                val = tail = tail[pos:]

            if fld.hidden:  # don't calculate hidden field
                continue

            pos = 0
            for _stp in fld._editc:  # for each step

                try:  # regular expression
                    res = _stp.search(str(val))
                    if res:
                        val = res.group(len(res.groups()))
                        if not pos: pos = res.end()
                    else:
                        val = 'No result'

                except:
                    if _stp.co_filename == '<exp>':  # python expression
                        try:
                            val = str(eval(_stp))
                        except:
                            val = 'Error in expression: ' + str(sys.exc_info()[1])

                    elif _stp.co_filename == '<gen>':  # python generator
                        try:
                                yield from eval(_stp)
                        except:
                            val = 'Error in generator: ' + str(sys.exc_info()[1])

            fld.value = val.replace('"', "'")

        try:
            user_finalize(self)  # call user defined procedure (if any)
        except Exception as err:
            common.set_error('user_finalize(): {}'.format(err))

        common.prev_msg = self
        self.cnt += 1
        yield self      # yield message to loader

        # end calculate_fields()

    # message parsers
    re_strip = re.compile('\0')
    def get_regex_messages(self, txt, start, end):
        """return message data for each match"""

        trig = self.trigger
        for m in self._trigger.finditer(txt, start, end):
            txt = m.group(0).strip()
            if txt:
                data = {}
                data['id'] = m.group(len(m.groups())) if trig.startswith('@') else trig
                data['name'] = self.name
                data['value'] = self.re_strip.sub('', m.group(0))
                data['range'] = (m.start(), m.end())
                data['parts'] = []
                yield data

    def get_xml_messages(self, txt, start, end):
        """
        Build a list of xpath nodes. Each node saves the start tag and the index of the start tag .
        Build a list of start nodes. Each node saves the regular match object, index of the closing node and the xpath.
        If a close tag matches the start tag (on the same level), the index of the closing node is stored in the start node.
        """

        MATCH, CLOSE_MATCH, CLOSED_IDX, DEPTH = 0, 1, 2, 3
        re_xml = re.compile(
            r'<([\w-.]+:)?([\w-.]+)\s*([^<>]+)??>([^<>]*)|<\/([\w-.]+:)?([\w-.]+)>')  # <(ns)(tag)(attr)>(text) or </(ns)(tag)>
        nodes = []  # nodes[index] = [match, close_match, closed_index, xpath]
        level = []  # level[depth] = (start_tag, start_index)
        msg_index = 0

        def assemble_msg(start_idx):
            """walk nodes from start index to close index, build xpath and create data dictionary"""

            parts = []
            xp = []

            start_node = nodes[start_idx]
            start_depth = start_node[DEPTH]
            for idx in range(start_idx, start_node[CLOSED_IDX]):
                node = nodes[idx]
                m = node[MATCH]

                # build xpath
                tag = m.group(2)
                depth = node[DEPTH]
                if depth - start_depth > len(xp):
                    xp.append(tag)
                else:
                    xp = xp[:depth-start_depth] + [tag]
                xpath = '/'.join(xp)

                # create part
                part = {}
                part['id'] = xpath
                part['name'] = tag
                part['values'] = (m.group(4).strip(), m.group(3), m.group(1), xpath)  # text, attr, ns, xpath
                parts.append(part)

            # create data
            data = {}
            root_tag = start_node[MATCH].group(2)
            start, end = start_node[MATCH].start(), start_node[CLOSE_MATCH].end()

            data['id'] = self.trigger if self.trigger.startswith('=') else root_tag
            data['name'] = root_tag
            data['value'] = txt[start:end]
            data['range'] = (start, end)
            data['parts'] = parts

            return data

        for m in re_xml.finditer(txt, start, end):  # <(ns:)(start_tag) (attr)>(text) | </(ns:)(close_tag)>

            start_tag, attr, close_tag = m.group(2), m.group(3), m.group(6)

            if start_tag:  # starting tag =====================================
                if attr and attr[-1] == '/':  # empty tag!
                    close_tag = start_tag
                level.append((start_tag, len(nodes)))   # expand xpath (start_tag, start_index)
                nodes.append([m, None, 0, len(level)])  # add node [match, close_match, closed_index, depth)

            if close_tag and level:  # closing tag ==============================
                started_tag, started_idx = level[-1]  # get start tag,index of this level
                level.pop()  # reduce xpath

                if close_tag == started_tag:  # node is closed (close tag matches start tag)
                    nodes[started_idx][CLOSE_MATCH] = m  # set close_match of node
                    nodes[started_idx][CLOSED_IDX] = len(nodes)  # set closed_idx of node
                    if started_idx == msg_index:  # message node is closed now
                        if self._trigger.search(started_tag):
                            yield assemble_msg(msg_index)
                        msg_index = len(nodes)  # set message index after closed index

        # search for remaining messages (only for invalid formatted xml)
        common.set_debug('remaining nodes {} of {}'.format(msg_index, len(nodes)))
        if msg_index < len(nodes)/2:     # first syntax error occured in first half of all nodes
            while msg_index < len(nodes):

                if nodes[msg_index][CLOSED_IDX]:  # message node was closed already
                    if self._trigger.search(started_tag):
                        yield assemble_msg(msg_index)
                    msg_index = nodes[msg_index][CLOSED_IDX]  # set message index after closed index
                else:
                    msg_index += 1  # try next


    # def get_xml_messages(self, txt, start, end):
    #     """
    #     Build a list of xpath nodes. Each node saves the start tag and the index of the start tag .
    #     Build a list of start nodes. Each node saves the regular match object, index of the closing node and the xpath.
    #     If a close tag matches the start tag (on the same level), the index of the closing node is stored in the start node.
    #     """
    #
    #     MATCH, CLOSE_MATCH, CLOSED_IDX, XPATH = 0, 1, 2, 3
    #     re_xml = re.compile(r'<([\w-.]+:)?([\w-.]+)\s*([^<>]+)??>([^<>]*)|<\/([\w-.]+:)?([\w-.]+)>')  # <(ns)(tag)(attr)>(text) or </(ns)(tag)>
    #     nodes = []  # nodes[index] = [match, close_match, closed_index, xpath]
    #     xpath = []  # xpath[depth] = (start_tag, start_index)
    #     msg_index = 0
    #
    #     def assemble_msg(start_idx):
    #
    #         tag = nodes[start_idx][MATCH].group(2)
    #         close_idx = nodes[start_idx][CLOSED_IDX]
    #         start, end = nodes[start_idx][MATCH].start(), nodes[start_idx][CLOSE_MATCH].end()
    #
    #         parts = []
    #         for idx in range(start_idx, close_idx):
    #             part = {}
    #             m = nodes[idx][MATCH]
    #             xp = nodes[idx][XPATH]
    #             xpath = '/'.join([x[0] for x in xp])
    #             part['id'] = xpath
    #             part['name'] = m.group(2)  # tag
    #             part['values'] = (m.group(4).strip(), m.group(3), m.group(1), xpath)  # text, attr, ns, xpath
    #             parts.append(part)
    #
    #         data = {}
    #         data['id'] = self.trigger if self.trigger.startswith('=') else tag
    #         data['name'] = tag
    #         data['value'] = txt[start:end]
    #         data['range'] = (start, end)
    #         data['parts'] = parts
    #
    #         return data
    #
    #     for m in re_xml.finditer(txt, start, end):  # <(ns:)(start_tag) (attr)>(text) | </(ns:)(close_tag)>
    #
    #         start_tag, attr, close_tag = m.group(2), m.group(3), m.group(6)
    #
    #         if start_tag:  # starting tag =====================================
    #             if attr and attr[-1] == '/':  # empty tag!
    #                 close_tag = start_tag
    #             xpath.append((start_tag, len(nodes)))  # expand xpath (start_tag, start_index)
    #             print(start_tag, xpath)
    #             nodes.append([m, None, 0, xpath[:]])  # add node [match, close_match, closed_index, xpath)
    #             if len(xpath) > 10: xpath.pop(0)
    #
    #         if close_tag and xpath:  # closing tag ==============================
    #             started_tag, started_idx = xpath[-1]  # get start tag,index of this level
    #             xpath.pop()  # reduce xpath
    #
    #             if close_tag == started_tag:                    # node is closed (close tag matches start tag)
    #                 nodes[started_idx][CLOSE_MATCH] = m         # set close_match of node
    #                 nodes[started_idx][CLOSED_IDX] = len(nodes)  # set closed_idx of node
    #                 if started_idx == msg_index:                # message node is closed now
    #                     if self._trigger.search(started_tag):
    #                         yield assemble_msg(msg_index)
    #                     xpath.clear()
    #                     msg_index = len(nodes)  # set message index after closed index
    #
    #     # search for remaining messages
    #     print('remains', msg_index, len(nodes))
    #     while msg_index < len(nodes):
    #
    #         if nodes[msg_index][CLOSED_IDX]:  # message node was closed already
    #             if self._trigger.search(started_tag):
    #                 # print( nodes[msg_index][MATCH].group(2), nodes[msg_index][XPATH])
    #                 yield assemble_msg(msg_index)
    #             msg_index = nodes[msg_index][CLOSED_IDX]  # set message index after closed index
    #         else:
    #             msg_index += 1  # try next

class OutputClass:
    def __init__(self, silent=False, frm=None):
        self.silent = silent
        self.frm = frm
        self.line = ''

        # redirect output to parent console
        if type(sys.stdout) != io.TextIOWrapper:  # stdout not connected
            if ctypes.windll.kernel32.AttachConsole(-1):  # attach parent console succes
                sys.stderr = sys.stdout = open('con', 'w')  # redirect output to parent console
                sys.stdout.write('\n')

        # redirect output to self
        self.olderr, self.oldout = sys.stderr, sys.stdout  # save old console
        sys.stderr = sys.stdout = self  # redirect output to self

        common.set_info('{} {} started (PID {})'.format('GUI' if frm else 'Console', version, os.getpid()))

    def write(self, txt):
        """Redirected stdout and stderr print to this function"""
        self.line += txt
        if '\n' in self.line:  # line is complete
            if not self.silent or (self.silent and not '[Info]' in self.line and not '[Debug]' in self.line):
                # print to table and to console
                common.db.insert_dict('#log', ['date', 'time', 'event'],
                                      [time.strftime('%m/%d'), time.strftime('%H:%M:%S'), self.line.strip()])
                self.oldout.write(self.line)
            self.line = ''

    def flush(self):  # necessary for console write
        pass


# === END of module===================

if __name__ == "__main__":
    pass
