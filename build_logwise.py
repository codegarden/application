import sys, os, datetime, platform, struct, collections, shutil
# from PyQt5.QtCore import QT_VERSION_STR
from PyQt5.Qt import PYQT_VERSION_STR
from sip import SIP_VERSION_STR
import regex, sqlite3, PyInstaller

app_name = "Logwise"
version_info = "1.3"

build_spec = r"""
# -*- mode: python -*-

block_cipher = None

a = Analysis(['Logwise.py'],
             pathex=[r'.'],
             binaries=None,
             datas=[(r'LW_BUILD_INFO.py',''),\
                    (r'manual.html',''),\
                    (r'configs\reference configuration.cfg', 'configs'),\
                    (r'stylesheets','stylesheets')],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None,
             excludes=None,
             win_no_prefer_redirects=None,
             win_private_assemblies=None,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='Logwise',
          debug=False,
          strip=None,
          upx=True,
          console=False, icon=r'icons\blade.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
#               upx=True,
               name='#DIST_FOLDER#')
"""


now = datetime.datetime.now()

lw_build_info = collections.OrderedDict()
lw_build_info["app_name"] = app_name
lw_build_info["version"] = version_info
lw_build_info["build"] = "{}{:02d}{:02d}".format(now.year, now.month, now.day)
lw_build_info["Python"] = sys.version
# lw_build_info["Python compiler"] = platform.python_compiler()
lw_build_info["PyQt"] = PYQT_VERSION_STR
# lw_build_info["Qt"] = QT_VERSION_STR
lw_build_info["regex"] = regex.__version__
lw_build_info["Sqlite"] = sqlite3.version
lw_build_info["QCustomPlot"] = '1.3.2 (2015-12-22)'
lw_build_info["PyInstaller"] = PyInstaller.__version__
lw_build_info["SIP"] = SIP_VERSION_STR
lw_build_info["platform"] = platform.platform()
lw_build_info["architecture"] = platform.architecture()
lw_build_info["processor"] = platform.processor()
lw_build_info["machine"] = platform.machine()
# lw_build_info["System"] = platform.system()
# lw_build_info["Struct"] = struct.calcsize('P') * 8
# lw_build_info["Proc Architecture"] = os.environ["PROCESSOR_ARCHITECTURE"]
# lw_build_info["Proc ArchiteW6432"] = os.environ.get("PROCESSOR_ARCHITEW6432 {}", "undefined")

with open('LW_BUILD_INFO.py', 'w') as f:      # write version file
    for k, v in lw_build_info.items():
        f.write('{:13s}= "{}"\n'.format(k, v))

with open('logwise.spec', 'w') as f:        # write pyinstaller spec file
    f.write(build_spec.replace('#DIST_FOLDER#', "Logwise" + version_info))

os.system(r'c:\python34\scripts\pyinstaller -y logwise.spec')    # build
os.remove('logwise.spec')

# copy replace smaller dll (without localisation)
shutil.copyfile('icudt53.dll', r'dist\logwise' + version_info + r'\icudt53.dll')

