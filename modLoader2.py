# -------------------------------------------------------------------------------
# Name:        modLoader2
# Purpose:     load messages from files
#
# Author:      nlrgo
#
# Created:     12-10-2013
# Copyright:   (c) nlrgo 2013
# Licence:     <your licence>
# -------------------------------------------------------------------------------

import datetime
import glob
import os
import stat
import regex as re

from PyQt5.QtWidgets import QApplication

import common
from modClasses import msgClass, fileClass, partClass
from modFunc import evalx, unique_name


class loaderClass():
    re_line = re.compile(r'([^\n]+)')  # line
    re_xml = re.compile(r'<([\w-.]+:)?([\w-.]+)\s*([^<>]+)??>([^<>]*)|<\/([\w-.]+:)?([\w-.]+)>')
    re_brackets = re.compile(r'([][])') # brackets

    def __init__(self, db):
        self.db = db

        self.file_iter = None

        self.files = []
        self.fileselect = ''    # required text in file

        self.msglist_wanted = []
        self.prev_messages = None

        self.bytes_total = 0
        self.bytes_processed = 0
        self.messages_loaded = 0
        self.time = 0

        # export to common
        common.total_index_base = 0
        common.generate_from_text = self.generate_from_text
        common.prev_msg = msgClass('empty')
        common.do_load = self.do_load

    def update_files(self, do_tail=False):
        """
        expand common.locations[] and store file properties to self.files[]
        tail: returns list of files that have grown
        """
        global prev_files
        if not 'prev_files' in globals():
            prev_files = {}

        path_specs = [self.re_brackets.sub(r'[\1]', loc.name) for loc in common.locations if loc.enabled]   # remove brackets

        curr_files = {}
        for path_spec in path_specs:  # get current files properties

            for path in glob.glob(path_spec):
                path = str(path)

                if not os.path.exists(path):
                    continue  # file doesn't exist

                if self.fileselect:
                    with open(path, encoding='utf-8', newline='', errors='ignore') as f:
                        if self.fileselect not in f.read(): continue

                file = fileClass(path)
                status = os.stat(path)
                file.size = status.st_size
                if not stat.S_ISREG(status.st_mode) or file.size == 0:
                    continue  # is not a regular file or is empty

                file.name = os.path.basename(path)
                file.folder = os.path.split(os.path.dirname(path))[1]
                dt = datetime.datetime.fromtimestamp(status.st_mtime)
                file.time = dt.strftime("%Y-%m-%d %H:%M:%S")

                curr_files[path] = file

        self.files = []
        if do_tail:  # select only grown files
            for path in set(curr_files).intersection(prev_files):
                growth = curr_files[path].size - prev_files[path].size
                if growth > 0:
                    curr_files[path].growth = growth  # growth = size - previous size
                elif growth < 0:
                    curr_files[path].growth = curr_files[path].size  # growth = size (file rewritten?)

                if curr_files[path].growth > 0:
                    self.files.append(curr_files[path])  # file has grown

        else:  # select all files
            for path in curr_files:
                curr_files[path].growth = curr_files[path].size  # growth = size
                self.files.append(curr_files[path])

        prev_files = curr_files

        # sort files
        if common.settings['file_name']:    # sort on file name
            self.files = sorted(self.files, key=lambda m: m.name.lower(), reverse=False)
        else:                               # sort on modification date
            self.files = sorted(self.files, key=lambda m: m.time.lower(), reverse=False)

        # select files after from_date
        from_date = common.settings['from_file_date']
        if from_date:
            if type(evalx(from_date)) == datetime.datetime:   # from_date is valid
                self.files = [f for f in self.files if evalx(f.time) > evalx(from_date)]
            else:
                self.files = []

        # keep the last max number of files
        max = int(common.settings['max_files'])
        self.files = self.files[-max:]

        # calculate total size
        if self.files:
            self.bytes_total = sum([f.size for f in self.files])  # total size of all files
        elif not do_tail:
            common.set_error('No files found')

    def do_load(self, load=False, reset=False , tail=False, test=None):
        """Parse self.messages from self.locations"""

        # initialize list of wanted messages
        self.msglist_wanted = [msg for msg in common.messages if msg.wanted and msg.enabled] if not test else test

        # reset if: unconditional, messages changed or iterator 'empty'
        if reset or self.msglist_wanted != self.prev_messages or not self.file_iter:
            self.prev_messages = self.msglist_wanted

            if common.current_file:
                common.current_file.close()     # close possibly open file
            # if hasattr(common.current_file, 'hnd') and not common.current_file.hnd.closed:
            #     common.current_file.close()     # close possibly open file

            self.file_iter = None if reset else self.generate_from_file()  # initialize iterator

            if load or test or reset:   # reset counters
                self.bytes_processed = common.total_index_base = 0
                self.messages_loaded = 0

                for msg in [msg for msg in common.messages if msg.enabled]:
                    msg.cnt = 0

            if load:    # drop tables
                if common.settings.get('mapped', False):  # drop tables
                    tables = [msg.name for msg in common.messages if msg.enabled]
                    self.db.drop_tables(tables)
                else:
                    self.db.drop_tables(['LOGDATA'])

        if load or test or tail:

            self.update_files(tail)  # get current files

            if not self.msglist_wanted:
                common.set_error('No messages to find')
                return

            try:
                common.cancel = False
                while True:
                    msg = next(self.file_iter)

                    if not msg:     # skip message
                        continue
                    elif msg == -1:  # canceled !!!
                        break

                    self.messages_loaded += 1
                    if test:    # only run once
                        break
                    else:       # store message in database
                        self.db.insert_msg(msg)

            except StopIteration:
                self.file_iter = None

            self.db.commit()

            [common.set_debug('msg loaded: ', msg.name, msg.cnt) for msg in common.messages]  # debug info

    def generate_from_file(self):
        """Read all files and call the parser"""

        for file in self.files:  # for each file
            common.set_info("Reading '{:s}'".format(file.name))
            common.current_file = file
            common.file_index_base = 0
            chunk_size = min(file.growth, 5000000)  # read growth bytes (maximized)
            cache = ''
            with open(file.path, encoding='utf_8', newline='', errors='ignore') as file.hnd:
                file.hnd.seek(file.size - file.growth)

                while True:
                    chunk = file.hnd.read(chunk_size)
                    if not cache and not chunk:
                        break

                    QApplication.processEvents()
                    if common.cancel:
                        yield -1    # cancelled!!!

                    txt = cache + chunk                                             # text to parse is left-over cache plus chunk
                    pos = yield from self.generate_from_text(txt, 0, len(txt))      # yield messages and return end position of last message
                    cache = txt[pos:] if pos else ''                                # left-over cache is text after last message (or emtpy if none found)

                    common.file_index_base += len(txt) - len(cache)                      # left-over cache + chunk - next cache
                    common.total_index_base += len(txt) - len(cache)                      # left-over cache + chunk - next cache
                    self.bytes_processed = common.total_index_base + 1

                file.close()    # done with file

    def generate_from_text(self, txt, from_pos, to_pos, level=0):
        level += 1  # function entry

        for msg_wanted in self.msglist_wanted[level - 1:]:  # for each message type

            for msg_data in msg_wanted.get_iter(txt, from_pos, to_pos):  # get all msg_data (iterating) for msg_wanted

                msg_beg, msg_end = msg_data['range']
                self.bytes_processed = max(self.bytes_processed, common.total_index_base + msg_end + 1)
                # print(msg_wanted.name, msg_beg, msg_end, repr(txt[msg_beg:msg_end][:200]))

                if msg_beg > from_pos:
                    yield from self.generate_from_text(txt, from_pos, msg_beg, level)  # recursively search before the found message

                # assign msg_data to message
                pointer = {msg.trigger: msg for msg in common.messages}
                uid = msg_data['id']
                try:    # message already exists
                    msg = pointer[uid]
                except:     # create new message
                    name = unique_name(msg_data['name'], common.messages)
                    msg = msgClass(name)        # create new message dynamical
                    msg.trigger = uid
                    msg.wanted = False
                    msg.do_compile()    # compile message after dynamic creation
                    common.messages.append(msg)
                    common.set_debug('new message', msg.name)

                if msg.enabled:
                    msg.value = msg_data['value']
                    msg.range = msg_data['range']
                    msg.parts = msg_data['parts']
                    msg.type = msg_wanted.type

                    yield from msg.calculate_message()  # yield the found message
                from_pos = msg_end                      # continue search after the found message

                QApplication.processEvents()
                if common.cancel:
                    yield -1  # cancelled!!!

        level -= 1  # function exit
        return from_pos     # return end position of last found message (or 0 if none found)


# === END of module===================

if __name__ == '__main__':
    pass

    # import pickle, time, common
    # from modFunc import load_config_from
    #
    # db = DatabaseClassSqlite("Default.db")
    # ldr = loaderClass(db)
    # load_config_from('configs\Default.cfg')
    # [msg.do_compile() for msg in common.messages]  # recompile all messages
    #
    # tim = time.time()
    #
    # ldr.do_load('all')
    # db.commit()
    #
    # print(time.time() - tim)


# OLD CODE

# xp[-1] = re.sub(r'(.*):?(\d*)', lambda x: x.group(1)+':'+str(int('0'+x.group(2))+1), xp[-1])

# (ns:)(tag) (attr)>(text) | /(ns:)(tag)
# re_xml = re.compile(r'<([\w-.]+:)?([\w-.]+)\s*([^<>]+)??>([^<>]*)|<\/[^<>]+>')  # (ns:)(tag) (attr)>(text) | /ns:tag
# re_xml = re.compile(r'<([\w-.]+:)??([\w-.]+)\s*([^<>]+)??>([^<>]*)|<\/(?1)?(?2)>')  # (ns:)(tag) (attr)>(text) | /(ns:)(tag)
# re_xml = re.compile(r'<([\/])?(\w+:)?+(\w+)\s*([^<>]*[^\/])??(\/)?>') # (/)(ns)(tag)(attr)(/)
# re_line = re.compile(r'([^\n]*)\n') # line

#        def peekable(iterator): # helper function to get item and next item (peek)
#            item = next(iterator)
#            for peek in iterator: yield item, peek; item = peek
#            yield item, None

# shutil.copyfile("{0}.db".format(os.path.basename(os.path.splitext(common.current_config_file)[0])),
#                 "{0}.db".format(os.path.basename(os.path.splitext(selected_file)[0])))
