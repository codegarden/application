# -------------------------------------------------------------------------------
# Name:        modClasses
# Purpose:     class definitions (message, field, model,...)
#
# Author:      nlrgo
#
# Created:     12-10-2013
# Copyright:   (c) nlrgo 2013
# Licence:     <your licence>
# -------------------------------------------------------------------------------
import sys
import regex as re
import html

from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import QObject, Qt, QEvent, QAbstractTableModel, pyqtSignal, QModelIndex, QRectF, QVariant, QTimer
from PyQt5.QtGui import QFont, QKeyEvent, QPen, QColor, QTextFormat, QTextCursor, QTextDocument
from PyQt5.QtWidgets import *
from qcplot.qcustomplot import QCustomPlot, QCPGraph, QCPBars, QCP, QCPBarsGroup, QCPAxisRect, QCPAxis, QCPItemLine, QCPItemText, QCPItemPosition, QCPLineEnding, QCPPlotTitle

from modClasses import locationClass
from modFunc import *  # possibly used by msgClass.calculate()


class ModelClass(QAbstractTableModel):

    def __init__(self, parent, dbase):
        QAbstractTableModel.__init__(self, parent)
        self.parent = parent    # result instance
        self.db = dbase

        self.table = ''
        self.where_clause = ''
        self.order_clause = ''
        self.rid = []
        self.rowCnt = 0
        self.columnCnt = 0
        self.cache = {}
        self.col_list = []
        self.filter_text = ''       # WHERE 'expression' (excl text WHERE)
        self.filter_gui_text = ''   # entered expression in gui
        self.sorted_section = {}    # ORDER BY column {name, ord} (excl text ORDER BY)
        self.cell_conversion = {}

        self.xcol = ''              # column name as graph x-axis
        self.ycols = []             # column names as y-axes
        self.top_row = -1
        self.bottom_row = -1
        self.r_beg = 0
        self.r_end = 30

    def set_sort(self, section):    # redirect
        """set self.sorted_section to {column name, sort order} or {} if no sorting"""

        col_name = self.col_list[section]   # name of clicked column
        if self.sorted_section and col_name == self.sorted_section['name']:  # column already sorted
            order = [1, -1, 0][self.sorted_section['ord']]    # rotate sort order
        else:   # set column to sort ascending (0)
            order = 0
            self.sorted_section = {'name':col_name, 'ord':order}
        self.sorted_section = {'name':col_name, 'ord':order} if order>-1 else {}

        self.set_table(self.table)

    def set_table(self, table):
        """set model to (sorted/filtered) table """
        common.set_debug('{}, called by: {}'.format(sys._getframe(0).f_code.co_name, sys._getframe(1).f_code.co_name))

        def py_rowid_func(vix):  # custom sql function (must be local)

            if self.rid:
                if vix >= 0 and vix < len(self.rid):
                    return self.rid[vix]
                else:
                    return False
            else:
                return vix + 1

        self.table = table
        self.db.conn.create_function('rowid_func', 1, py_rowid_func)

        # set column names and column count
        self.col_list = self.db.column_names(self.table)
        self.columnCnt = len(self.col_list)

        # set where clause (if any)
        self.where_clause = 'WHERE {}'.format(self.filter_text) if self.filter_text else ''

        # set order clause (if any)
        if self.sorted_section and self.sorted_section['name'] in self.col_list:
            section_name = self.sorted_section['name']
            section_order = ['ASC','DESC',''][self.sorted_section['ord']]
            self.order_clause = 'ORDER BY "{}" {}'.format(section_name, section_order)
        else:
            self.sorted_section = {}
            self.order_clause = ''

        # set mapping list
        self.rid.clear()
        if self.where_clause or self.order_clause:
            try:
                sql = 'select _rowid_ from "{}" {} {}'.format(self.table, self.where_clause, self.order_clause)
                cur = self.db.conn.execute(sql)
                [self.rid.append(id[0]) for id in cur.fetchall()]
                common.set_debug("set_table('{}'): {}".format(self.table, sql))
            except:
                self.rid.clear()
        else:
            db_row_cnt = self.db.row_count(self.table)
            [self.rid.append(ix+1) for ix in range(db_row_cnt)]
            common.set_debug("set_table('{}'): unfilterd".format(self.table))

        # set sort indicator asc/desc
        if self.sorted_section:
            section = self.col_list.index(self.sorted_section['name'])
            order = self.sorted_section['ord']
            self.parent.view.horizontalHeader().setSortIndicatorShown(True)
            self.parent.view.horizontalHeader().setSortIndicator(section, order)  # set sortindicator asc/desc
        else:  # reset sort indicator
            self.parent.view.horizontalHeader().setSortIndicatorShown(False)

        # set row count
        self.rowCnt = len(self.rid)

        # self.parent.table_changed()
        self.layoutChanged.emit()   # inform view and result

    def flags(self, index):
        return Qt.ItemIsEditable | Qt.ItemIsEnabled | Qt.ItemIsSelectable

    def rowCount(self, parent=None):
        return self.rowCnt

    def columnCount(self, parent=None):
        return self.columnCnt

    def headerData(self, section, orient, role):

        if role == Qt.DisplayRole:
            if orient == Qt.Horizontal:
                return self.col_list[section]   # hor: column name
            else:
                return self.rid[section]        # vert: row number
        else:
            return QVariant()

    def data(self, index, role=Qt.DisplayRole):

        rix = index.row()
        cix = index.column()

        if role == Qt.DisplayRole or role == Qt.EditRole:
            try:
                if role == Qt.DisplayRole:
                    cur = self.db.conn.execute('select substr("{}", 0, 256) from "{}" where _rowid_ = rowid_func({})'.format(self.col_list[cix], self.table, rix))
                    row = cur.fetchone()
                    val = str(row[0]).split('\n')[0]    # first line only
                    # val = str(row[0]).replace('\n', r'\n') if row else ''    # limited value
                else:
                    cur = self.db.conn.execute('select "{}" from "{}" where _rowid_ = rowid_func({})'.format(self.col_list[cix], self.table, rix))
                    row = cur.fetchone()
                    val = str(row[0])    # raw value
            except Exception as err:
                val = 'Error in model.data(): {}'.format(err)
            return val
        else:
            return QVariant()

    def setData(self, index, value, role=Qt.EditRole):
        rix = index.row()
        cix = index.column()

        if role == Qt.EditRole:
            try:
                sql = 'update "{}" set "{}" = "{}" where _rowid_ = rowid_func({})'.format(self.table, self.col_list[cix], value, rix)
                self.db.conn.execute(sql)  # update table
                self.db.commit()
                self.dataChanged.emit(index, index)  # inform the view
                self.layoutChanged.emit()
                return True
            except Exception as err:
                common.set_debug('Error in setData(): {}'.format(err))
                return False
        else:
            return False

    def get_block_sql(self, columns=['*'], first=0, last=None, where='1', limit=-1, reverse=False):
        """
        Description: table is queried in row order as defined in self.rid, using custom sql function: id = rowid_func(0,1,2,..)
        Pseudo SQL:  select. (visible columns) from (table) join (select. ix, id from table) on (table.rowid = id)
        """

        db_col_nams = self.db.column_names(self.table)              # read column names from table
        col_nams = [db_col_nams[col] if type(col)==int else col for col in columns]                      # db_col_nams[col] or col
        col_nams = [nam if (nam=='*' or " AS " in nam) else ('"{}"'.format(nam)) for nam in col_nams]  # quote items (except for "*" and "'' AS ... ")

        if last==None:
            last = len(self.rid)

        selector = '{}-_rowid_'.format(len(self.rid)) if reverse else '_rowid_-1'

        sql = """
            select {1}
            from "{0}" tab
            join (select {6} _ix_, rowid_func({6}) _id_ from "{0}") ptr
            on tab._rowid_ = ptr._id_
            where _ix_ >= {2} and _ix_ <= {3} and {4}
            limit {5}
            """.format(self.table, ','.join(col_nams), first, last, where, limit, selector)

        common.set_debug('{}:\n{}'.format(sys._getframe(0).f_code.co_name, sql))
        return sql

    def get_block(self, columns=['*'], first=0, last=None, where='1', limit=-1, reverse=False):

        sql = self.get_block_sql(columns, first, last, where, limit, reverse)
        cur = self.db.conn.execute(sql)
        return cur.fetchall()

    def find_next(self, search_text, cur_idx):

        for passage in [1,2]:  # 1: search in current row from current to last column, 2: search in next rows

            col_ixs = [c for c in range(self.columnCount()) if not self.parent.view.isColumnHidden(c)]     # column indexes
            col_nams = ['"{}"'.format(self.col_list[c]) for c in col_ixs]                                  # column names

            if passage == 1:
                check_row = cur_idx.row()           # current row
                check_col = cur_idx.column() + 1    # next column

            elif passage == 2:
                where = "{} like '%{}%'".format("||','||".join(col_nams), search_text)
                res = self.get_block(columns=['_ix_'], first=check_row + 1, where=where, limit=1)  # search from next row forward
                if not res:
                    return None # not found

                check_row = res[0]['_ix_']
                check_col = 0

            # search text in 'check_row' from 'check_col' to the last column
            for col in [c for c in col_ixs if c >= check_col]:
                idx = self.index(check_row, col)
                if search_text.lower() in str(idx.data(Qt.EditRole)).lower():
                    return idx  # text is found in idx.data

    def find_prev(self, search_text, cur_idx):

        for passage in [1,2]:  # 1: search in current row from current to first column, 2: search in previous rows

            col_ixs = ([c for c in reversed(range(self.columnCount())) if not self.parent.view.isColumnHidden(c)])     # column indexes in reverse order
            col_nams = ['"{}"'.format(self.col_list[c]) for c in col_ixs]                                              # column names

            if passage == 1:
                check_row = cur_idx.row()           # current row
                check_col = cur_idx.column() - 1    # previous column

            elif passage == 2:
                where = "{} like '%{}%'".format("||','||".join(col_nams), search_text)
                res = self.get_block(columns=['_ix_'], last=check_row - 1, where=where, limit=1, reverse=True)  # search from next row forward
                if not res:
                    return None # not found

                check_row = res[0]['_ix_']
                check_col = self.columnCount()

            # search text in 'check_row' from 'check_col' to the first column
            for col in [c for c in col_ixs if c <= check_col]:
                idx = self.index(check_row, col)
                if search_text.lower() in str(idx.data(Qt.EditRole)).lower():
                    return idx  # text is found in index idx

    def modify_table(self, columns, row_nr=0, cnt=0):
        """
        create temp table with 'cols' columns and 'row_nr' rows from old table
        possibly insert 'cnt' empty rows
        possibly insert remaining rows from old table
        drop old table
        rename temp table to old table
        """

        if not columns:
            return
        name_tmp = unique_name('temp', self.db.db_tables())
        default_values = ["''"] * len(self.db.column_names(self.table))

        if cnt < 0:     # delete
            to_row = row_nr - 1
            from_row = row_nr + abs(cnt)
        else:           # insert 0 or more rows
            to_row = row_nr
            from_row = row_nr + 1

        sql = self.get_block_sql(columns=columns, last=to_row)
        self.db.conn.execute('CREATE TABLE IF NOT EXISTS "{}" AS {}'.format(name_tmp, sql))
        for i in range(max(cnt, 0)):
            self.db.conn.execute('INSERT INTO {} VALUES ({})'.format(name_tmp, ','.join(default_values)))
        sql = self.get_block_sql(columns=columns, first=from_row)
        self.db.conn.execute('INSERT INTO {} {}'.format(name_tmp, sql))
        self.db.conn.execute('DROP TABLE IF EXISTS "{}"'.format(self.table))
        self.db.conn.execute("ALTER TABLE '{}' RENAME TO '{}'".format(name_tmp, self.table))


class ResultClass(QWidget):
    search_txt = ''
    highlights = {}
    v_context_menu = None   # vertical context menu
    h_context_menu = None   # horizontal context menu

    def __init__(self, table, dbo):
        super(ResultClass, self).__init__()

        self.states = {}
        self.timer = QTimer()
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.hnd_timeout)

        self.db = dbo
        self.table = table
        self.model = ModelClass(self, dbo)
        self.model.layoutChanged.connect(self.hnd_layoutChanged)

        self.view = QTableView()
        self.view.setModel(self.model)
        self.view.setObjectName('viewResults')
        self.view.setItemDelegate(ResultsDelegate(self))
        self.view.verticalScrollBar().valueChanged.connect(self.on_scroll)
        self.view.setSelectionMode(QAbstractItemView.ContiguousSelection)
        self.view.setCornerButtonEnabled(True)
        self.view.setWordWrap(False)
        self.view.setContextMenuPolicy(Qt.ActionsContextMenu)
        self.view.setFont(QFont('Courier New'))

        self.view.verticalHeader().setDefaultSectionSize(22)
        self.view.verticalHeader().setContextMenuPolicy(Qt.CustomContextMenu)
        self.view.verticalHeader().customContextMenuRequested.connect(self.ver_header_context)  # context requested handler
        self.view.verticalHeader().setResizeContentsPrecision(0)     # nof columns in resize calculation

        self.view.horizontalHeader().setSortIndicatorShown(True)
        self.view.horizontalHeader().setSectionsMovable(True)
        self.view.horizontalHeader().setResizeContentsPrecision(0)     # nof columns in resize calculation
        self.view.horizontalHeader().setStretchLastSection(True)
        self.view.horizontalHeader().setContextMenuPolicy(Qt.CustomContextMenu)
        self.view.horizontalHeader().customContextMenuRequested.connect(self.hor_header_context)  # context requested handler

    def on_scroll(self):
        self.timer.start(500)

    def hnd_timeout(self):
        self.model.layoutChanged.emit()

    def hnd_layoutChanged(self):
        """Set current index, bottom and top row"""

        view = self.view
        model = self.model

        model.top_row = view.rowAt(0)
        model.bottom_row = view.rowAt(view.height()) if view.rowAt(view.height()) != -1 else model.rowCnt

        if not model.rowCnt or not model.columnCnt:   # empty table -> invalidate current index
            view.setCurrentIndex(QModelIndex())
        else:                                         # truncate current index
            cur_col = view.currentIndex().column()
            cur_row = view.currentIndex().row()
            view.setCurrentIndex(model.index(min(cur_row, model.rowCnt - 1), min(cur_col, model.columnCnt - 1)))

    def selectedBlock(self):
        """retrieves the begin row index, end row index and column indexes of the currently selected block"""
        selmod = self.view.selectionModel()

        rowidx = self.view.currentIndex().row()
        colidx = self.view.currentIndex().column()
        if colidx == -1:
            return (-1, -1, [])

        # find begin and end row index by probing (fast)
        a = -1
        beg_rowidx = end_rowidx = rowidx
        b = self.model.rowCnt

        while beg_rowidx > a + 1:
            p = round((a + beg_rowidx) / 2)
            if selmod.isSelected(self.model.index(p, colidx)):
                beg_rowidx = p
            else:
                a = p

        while end_rowidx < b - 1:
            p = round((end_rowidx + b) / 2)
            if selmod.isSelected(self.model.index(p, colidx)):
                end_rowidx = p
            else:
                b = p

        # find columns indexes by checking all columns of current selected row
        col_idxs = []
        for c in range(self.model.columnCount()):
            logidx = self.view.horizontalHeader().logicalIndex(c)
            if selmod.isSelected(self.model.index(rowidx, logidx)) and not self.view.isColumnHidden(logidx):
                col_idxs.append(logidx)

        return(beg_rowidx, end_rowidx, col_idxs)

    def find_next(self, search_text):
        self.search_txt = search_text  # set result attribute for delegate
        common.cancel = False
        common.set_info("Searching next '{0:s}'".format(search_text))
        QApplication.processEvents()

        sel = self.view.currentIndex() if self.view.currentIndex() else self.model.index(0, 0)  # current index or first cell

        idx = self.model.find_next(search_text, sel)
        if idx:
            self.view.setCurrentIndex(idx)
            self.view.scrollTo(idx)
            self.view.setFocus()
            common.set_info("'{0:s}' found".format(search_text))
            return

        QApplication.processEvents()
        if common.cancel:
            common.set_info('Searching cancelled')
            return

        common.set_info("'{0:s}' not found".format(search_text))

    def find_prev(self, search_text):
        self.search_txt = search_text  # set result attribute for delegate
        common.cancel = False
        common.set_info("Searching prev '{0:s}'".format(search_text))
        QApplication.processEvents()

        sel = self.view.currentIndex() if self.view.currentIndex() else self.model.index(0, 0)  # current index or first cell

        idx = self.model.find_prev(search_text, sel)
        if idx:
            self.view.setCurrentIndex(idx)
            self.view.scrollTo(idx)
            self.view.setFocus()
            common.set_info("'{0:s}' found".format(search_text))
            return

        QApplication.processEvents()
        if common.cancel:
            common.set_info('Searching cancelled')
            return

        common.set_info("'{0:s}' not found".format(search_text))

    def hor_header_context(self, point):

        section =  self.view.horizontalHeader().logicalIndexAt(point)

        self.view.selectColumn(section)

        self.h_context_menu.popup(self.view.viewport().mapToGlobal(point))

    def ver_header_context(self, point):

        section = self.view.verticalHeader().logicalIndexAt(point)

        if not self.view.selectionModel().isRowSelected(section, QModelIndex()):
            self.view.selectRow(section)

        self.v_context_menu.popup(self.view.viewport().mapToGlobal(point))


class ResultsDelegate(QStyledItemDelegate):  # delegate for result table (highlight/search text)

    def __init__(self, parent=None):
        super(ResultsDelegate, self).__init__(parent)

    def paint(self, painter, option, index):

        search_txt = self.parent().search_txt   # text to search for
        hl_dict = self.parent().highlights      # highlight dictionary {text:color}
        if not (search_txt or hl_dict):         # default paint
            QStyledItemDelegate.paint(self, painter, option, index)
            return

        val = str(index.model().data(index, Qt.DisplayRole))    # get cell value

        found = (search_txt and search_txt in val)                  # search text found in cell value
        high = next((hl for hl in hl_dict if hl in val), False)     # highlight text found in cell value

        if found or hl_dict:    # custom paint
            painter.save()

            if found:   # search text
                val = html.escape(val)                      # escape html in cell text
                search_txt = html.escape(search_txt)        # escape html in search text
                pat = '(.*?)({})(.*)'.format(search_txt)    # create pattern

                m = re.search(pat, val)         # split cell text on search text
                val = m.group(1)
                val += '<span style = "color:red">{}</span>'.format(m.group(2))     # style searched text
                val += m.group(3)

            doc = QTextDocument()               # create document
            doc.setDefaultFont(option.font)     # use existing font
            doc.setDocumentMargin(3)            # set margins
            doc.setHtml(val)                    # assign (styled) cell text

            if high:    # highlight text
                clr = QtGui.QColor(hl_dict[high])   # get highlight color
                if option.state & 0x00008000:       # make darker is cell is selected
                    clr = clr.darker(140)
                painter.fillRect(option.rect, clr)  # paint background color

            painter.translate(option.rect.x(), option.rect.y())         # translate coordinates to cell values
            doc.drawContents(painter, QRectF(0, 0, option.rect.width(), option.rect.height()))  # paint document
            painter.restore()

        else:       # default paint
            QStyledItemDelegate.paint(self, painter, option, index)


class FieldsDelegate(QStyledItemDelegate):  # class for multiline fields

    def __init__(self, owner):
        super(FieldsDelegate, self).__init__(owner)


    def editorEvent(self, event, model, QStyleOptionViewItem, index):   # mouse clicks and start editing
        self.model = model
        self.index = index
        return False

    def createEditor(self, parent, option, index):  # create editor
        self.editor = QTextEdit(parent)
        self.editor.setTabChangesFocus(True)        # tab key -> next field
        self.editor.setWordWrapMode(0)              # No wrap
        self.editor.setHorizontalScrollBarPolicy(1) # always off
        self.editor.setVerticalScrollBarPolicy(1)   # always off
        self.editor.document().setDocumentMargin(0)
        return self.editor

    def updateEditorGeometry(self, editor, option, index):      # set editor geomery
        editor.setGeometry(option.rect)

    def setEditorData(self, editor, index):         # read editor data from the model
        if self.editor.objectName() != 'has_data':  # data not read yet
            self.editor.setObjectName('has_data')
            value = index.data(QtCore.Qt.DisplayRole)
            self.old_data = value
            editor.setPlainText(value)
            editor.selectAll()

    def setModelData(self, editor, model, index):   # write editor data to the model
        value = editor.toPlainText()
        model.setData(index, value, QtCore.Qt.DisplayRole)

    def eventFilter(self, editor, event):

        if event.type() == QEvent.KeyPress and event.key() == Qt.Key_Return:    # return key pressed

                if event.modifiers() == Qt.ControlModifier:         # ctrl-Return pressed
                    event = QKeyEvent(QEvent.KeyPress, Qt.Key_Return, Qt.NoModifier)
                    QTextEdit.keyPressEvent(self.editor, event)     # send Return to editor (add new line)

                else:                                               # Return pressed
                    cur_idx = self.parent().currentIndex()
                    self.parent().setCurrentItem(None)              # force commitData() and closeEditor()
                    self.parent().setCurrentCell(cur_idx.row(), cur_idx.column())

        if event.type() == QEvent.KeyPress:
            if event.key() == Qt.Key_Escape:
                value = self.old_data
            else:
                value = editor.toPlainText()
            self.model.setData(self.index, value, QtCore.Qt.DisplayRole)

        return QStyledItemDelegate.eventFilter(self, editor, event)


class PythonHighlighter(QtGui.QSyntaxHighlighter):
    Rules = []

    def __init__(self, parent=None):
        super(PythonHighlighter, self).__init__(parent)

        # SQL highlighting
        keywordFormat = QtGui.QTextCharFormat()
        keywordFormat.setForeground(QtGui.QColor("blue"))
        keywordFormat.setFontWeight(QtGui.QFont.Bold)

        keywords = ["ABORT", "ALL", "ALTER", "AND", "AS", "ASC", "BETWEEN", "BY", "CASE", "CAST", "COLUMN", "COMMIT",
                    "CREATE", "DEFAULT", "DELETE", "DESC", "DISTINCT", "DROP", "EXISTS", "EXPLAIN", \
                    "FOR", "FROM", "GROUP", "HAVING", "IF", "IMMEDIATE", "IN", "INDEX", "INNER", "INSERT", "INTO", "IS",
                    "ISNULL", "JOIN", "LEFT", "LIKE", "LIMIT", "NOT", "NOTNULL", "NULL", "OF", "OFFSET", "ON", "OR", \
                    "ORDER", "OUTER", "PLAN", "PRAGMA", "PRIMARY", "REGEXP", "REINDEX", "RENAME", "RIGHT", "ROLLBACK",
                    "ROW", "SELECT", "SET", "TABLE", "TEMP", "THEN", "TO", "TRIGGER", "UNION", "UNIQUE", "UPDATE", \
                    "USING", "VACUUM", "VALUES", "VIEW", "WHEN", "WHERE"]
        keywords += ["abs", "changes", "char", "coalesce", "global", "ifnull", "instr", "hex", "last_insert_rowid",
                     "length", "like", "lower", "ltrim", "max", "min", "nullif", "quote", "random", "replace", "round",
                     "rtrim", "soundex", "sqlite_version", \
                     "substr", "trim", "typeof", "unicode", "upper", "date", "time", "datetime", "julianday",
                     "strftime", "avg", "count", "group_concat", "max", "min", "sum", "total"]

        for pattern in keywords:
            re = QtCore.QRegExp(r"\b" + pattern + r"\b")
            self.Rules.append((re, keywordFormat))

        # COMMENT highlighting
        commentFormat = QtGui.QTextCharFormat()
        commentFormat.setForeground(QtGui.QColor("darkgray"))
        commentFormat.setFontItalic(True)

        self.Rules.append((QtCore.QRegExp(r"#.*"), commentFormat))
        self.Rules.append((QtCore.QRegExp(r"--.*"), commentFormat))

        # STRING highlighting
        stringFormat = QtGui.QTextCharFormat()
        stringFormat.setForeground(QtGui.QColor("deeppink"))

        stringRe = QtCore.QRegExp(r"""(?:'[^']*'|"[^"]*")""")
        stringRe.setMinimal(True)
        self.Rules.append((stringRe, stringFormat))

        stringRe = QtCore.QRegExp(r"""(:?"["]".*"["]"|'''.*''')""")
        stringRe.setMinimal(True)
        self.Rules.append((stringRe, stringFormat))

    def highlightBlock(self, text):

        for regex, format in self.Rules:
            regex.setCaseSensitivity(0)
            i = regex.indexIn(text)
            while i >= 0:
                length = regex.matchedLength()
                self.setFormat(i, length, format)
                i = regex.indexIn(text, i + length)


class MyQListWidget(QListWidget):
    """subclass to allow files to be dropped"""

    sig_dropped = QtCore.pyqtSignal()   # custom signal

    def __init__(self, parent):
        QListWidget.__init__(self, parent)
        self.setAcceptDrops(True)       # allow dropping

    def dragEnterEvent(self, e):
        if e.mimeData().hasUrls():
            e.acceptProposedAction()

    def dragMoveEvent(self, e):
        if e.mimeData().hasUrls():
            e.acceptProposedAction()

    def dropEvent(self, e):
        for url in e.mimeData().urls(): # append new locations
            new_loc = locationClass(url.toLocalFile())
            common.locations.append(new_loc)
        self.sig_dropped.emit()         # signal drop event finished -> display_locations()


class MyQTextEdit(QTextEdit):
    """subclass to allow files to be dropped"""

    def __init__(self, parent):
        QTextEdit.__init__(self, parent)
        self.clip = QApplication.clipboard()

    def keyPressEvent(self, event):

        if event == QtGui.QKeySequence.Copy:
            self.copy()
        elif event == QtGui.QKeySequence.Cut:
            self.cut()
        elif event.key() == Qt.Key_D and event.modifiers() == Qt.ControlModifier:
            self.duplicate()
        elif event.key() == Qt.Key_Slash and event.modifiers() == Qt.ControlModifier:
            self.comment()
        elif event.key() == Qt.Key_Return:
            self.return_pressed()
        elif event.key() == Qt.Key_Tab:
            self.tab_pressed(event)
        elif event.key() == Qt.Key_Backtab:
            self.tab_pressed(event, True)
        else:
            QTextEdit.keyPressEvent(self, event)

    def tab_pressed(self, event, unindent=False):

        if self.textCursor().hasSelection():
            cur = self.textCursor()
            beg = cur.selectionStart()      # begin of selection
            end = cur.selectionEnd()        # end of selection

            cur.setPosition(beg)
            cur.movePosition(QTextCursor.StartOfLine)       # to begin of first selected line
            cur.setPosition(end, QTextCursor.KeepAnchor)
            cur.movePosition(QTextCursor.EndOfLine, QTextCursor.KeepAnchor) # to end of last selected line
            self.setTextCursor(cur)

            sel = self.textCursor().selectedText()      # get selected text
            lines = sel.split('\u2029')                 # selectedText() returns \u2029 instead of \n
            for l, line in enumerate(lines):
                if unindent:
                    lines[l] = re.sub(r'^(\t| *)', '', line)
                else:
                    lines[l] = '\t' + line if line.strip() else line
            sel = '\n'.join(lines)
            self.insertPlainText(sel)                   # insert

            cur.setPosition(beg)
            cur.movePosition(QTextCursor.StartOfLine)       # to begin of first selected line
            line_beg = cur.selectionStart()
            cur.setPosition(line_beg + len(sel), QTextCursor.KeepAnchor)
            cur.movePosition(QTextCursor.EndOfLine, QTextCursor.KeepAnchor) # to end of new selection
            self.setTextCursor(cur)
        else:
            QTextEdit.keyPressEvent(self, event)

    def return_pressed(self):
        txt = self.toPlainText()
        pos = self.textCursor().position()  # character position cursor

        ins = re.findall(r'\n[ \t]*', txt[:pos])    # find identation of current line
        self.insertPlainText(ins[-1]) if ins else self.insertPlainText('\n')

    def copy(self):
        txt = self.toPlainText()

        if self.textCursor().hasSelection():
            QTextEdit.copy(self)
        else:
            self.select_current_line()  # select line containing the cursor
            sel = self.textCursor().selectedText()      # selected text
            self.clip.setText(sel)                      # to clipboard

    def cut(self):
        txt = self.toPlainText()

        if self.textCursor().hasSelection():
            QTextEdit.cut(self)
        else:
            self.select_current_line()  # select line containing the cursor
            sel = self.textCursor().selectedText()      # selected text
            self.clip.setText(sel)                      # to clipboard
            self.textCursor().removeSelectedText()      # remove

    def duplicate(self):
        txt = self.toPlainText()

        if self.textCursor().hasSelection():
            return
        else:
            self.select_current_line()  # select line containing the cursor
            sel = self.textCursor().selectedText()      # selected text
            self.insertPlainText(sel)                   # insert
            self.insertPlainText(sel)                   # insert

    def comment(self):
        self.select_current_line()  # select line containing the cursor
        sel = self.textCursor().selectedText()          # selected text

        comment_char = '#' if 'Python' in self.toPlainText() else '--'

        comment = re.findall(r'^\s*({}\s*)'.format(comment_char), sel)
        if comment:
            sel = sel.replace(comment[0], '', 1)        # remove comment
        elif sel.strip():
            sel = re.sub(r'^(\s*)', r'\1{} '.format(comment_char), sel) # insert comment

        self.insertPlainText(sel)                       # insert

    def select_current_line(self):
        """select line containing the cursor"""
        txt = self.toPlainText()    # all text

        pos = self.textCursor().position()  # character position cursor
        txt_bef, txt_aft = txt[:pos].split('\n'), txt[pos:].split('\n')   # split text at cursorposition
        l_bef, l_aft = txt_bef[:-1], txt_aft[1:]                                # keep complete lines before and after cursor pos
        line = txt_bef[-1] + txt_aft[0]                                   # line containing the cursor

        beg = len('\n'.join(l_bef))            # begin of line
        end = len('\n'.join(l_bef + [line]))   # end of line
        cur = self.textCursor()
        cur.setPosition(beg + 1)                    # start of selection
        cur.setPosition(min(end + 1, len(txt)), 1)  # end of selection (max end of text)
        self.setTextCursor(cur)                     # select line
        return beg, end                             # return beg, end position

    def text_block(self):
        """Return selected block"""

        SEP = '\x1D'  # group seperator
        extraSelections = []
        blk = ''

        txt = self.toPlainText()
        is_script = ('__script__' in txt)

        if self.textCursor().hasSelection():  # selection has priority
            blk = self.textCursor().selection().toPlainText()
        else:  # get block
            txt_len = len(txt)
            pos = self.textCursor().position()
            txt = txt[:pos] + SEP + txt[pos:]  # insert SEP

            pat_colon = ';[\s' + SEP + ']*'  # split on ';' (ignoring space or SEP)
            pat_empty = '|\n[\s' + SEP + ']*\n'  # split on empty line (ignoring space or SEP)
            pat = pat_colon if is_script else pat_colon + pat_empty
            txtl = re.split(pat, txt)  # split text

            for txt in txtl:
                if SEP in txt:  # block contains SEP
                    sep_pos = txt.find(SEP)
                    blk = txt.replace(SEP, '')  # strip SEP
                    beg = pos - sep_pos  # block begin pos
                    end = beg + len(blk)  # block end pos

                    selection = QTextEdit.ExtraSelection()
                    selection.format.setBackground(QColor(0xFAFFE0))
                    selection.format.setProperty(QTextFormat.FullWidthSelection, True)
                    selection.cursor = self.textCursor()
                    selection.cursor.setPosition(beg)  # set anchor and cursor position
                    selection.cursor.setPosition(min(end + 1, txt_len), 1)  # set only cursur position

                    extraSelections.append(selection)  # add selection

        self.setExtraSelections(extraSelections)

        return blk


# === END of module===================

if __name__ == "__main__":
    pass
