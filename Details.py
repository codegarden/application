# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Details.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Details(object):
    def setupUi(self, Details):
        Details.setObjectName("Details")
        Details.setEnabled(True)
        Details.resize(627, 576)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/single"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Details.setWindowIcon(icon)
        self.gridLayout_2 = QtWidgets.QGridLayout(Details)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.splitter = QtWidgets.QSplitter(Details)
        self.splitter.setOrientation(QtCore.Qt.Vertical)
        self.splitter.setObjectName("splitter")
        self.tblDetails = QtWidgets.QTableWidget(self.splitter)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(2)
        sizePolicy.setHeightForWidth(self.tblDetails.sizePolicy().hasHeightForWidth())
        self.tblDetails.setSizePolicy(sizePolicy)
        self.tblDetails.setWordWrap(False)
        self.tblDetails.setObjectName("tblDetails")
        self.tblDetails.setColumnCount(0)
        self.tblDetails.setRowCount(1)
        item = QtWidgets.QTableWidgetItem()
        self.tblDetails.setVerticalHeaderItem(0, item)
        self.tblDetails.horizontalHeader().setVisible(False)
        self.tblDetails.horizontalHeader().setStretchLastSection(True)
        self.tblDetails.verticalHeader().setCascadingSectionResizes(False)
        self.tblDetails.verticalHeader().setDefaultSectionSize(25)
        self.tblDetails.verticalHeader().setStretchLastSection(False)
        self.widget = QtWidgets.QWidget(self.splitter)
        self.widget.setObjectName("widget")
        self.gridLayout = QtWidgets.QGridLayout(self.widget)
        self.gridLayout.setObjectName("gridLayout")
        self.txtSource = QtWidgets.QPlainTextEdit(self.widget)
        self.txtSource.setBaseSize(QtCore.QSize(0, 0))
        font = QtGui.QFont()
        font.setFamily("Consolas")
        font.setPointSize(9)
        self.txtSource.setFont(font)
        self.txtSource.setLineWrapMode(QtWidgets.QPlainTextEdit.WidgetWidth)
        self.txtSource.setReadOnly(True)
        self.txtSource.setBackgroundVisible(False)
        self.txtSource.setObjectName("txtSource")
        self.gridLayout.addWidget(self.txtSource, 0, 0, 1, 4)
        self.btnHtml = QtWidgets.QPushButton(self.widget)
        self.btnHtml.setStatusTip("")
        self.btnHtml.setObjectName("btnHtml")
        self.gridLayout.addWidget(self.btnHtml, 1, 0, 1, 1)
        self.btnS2h = QtWidgets.QPushButton(self.widget)
        self.btnS2h.setObjectName("btnS2h")
        self.gridLayout.addWidget(self.btnS2h, 1, 1, 1, 1)
        self.btnH2s = QtWidgets.QPushButton(self.widget)
        self.btnH2s.setObjectName("btnH2s")
        self.gridLayout.addWidget(self.btnH2s, 1, 2, 1, 1)
        self.btnNone = QtWidgets.QPushButton(self.widget)
        self.btnNone.setObjectName("btnNone")
        self.gridLayout.addWidget(self.btnNone, 1, 3, 1, 1)
        self.gridLayout_2.addWidget(self.splitter, 0, 0, 1, 1)

        self.retranslateUi(Details)
        QtCore.QMetaObject.connectSlotsByName(Details)

    def retranslateUi(self, Details):
        _translate = QtCore.QCoreApplication.translate
        Details.setWindowTitle(_translate("Details", "Single Record"))
        item = self.tblDetails.verticalHeaderItem(0)
        item.setText(_translate("Details", "No record selected"))
        self.btnHtml.setToolTip(_translate("Details", "Convert to html"))
        self.btnHtml.setText(_translate("Details", "html"))
        self.btnS2h.setToolTip(_translate("Details", "Convert string to hex"))
        self.btnS2h.setText(_translate("Details", "string -> hex"))
        self.btnH2s.setToolTip(_translate("Details", "Convert hex to string"))
        self.btnH2s.setText(_translate("Details", "hex -> string"))
        self.btnNone.setToolTip(_translate("Details", "No conversion"))
        self.btnNone.setText(_translate("Details", "none"))

import resources_rc
