# -------------------------------------------------------------------------------
# Name:        Logwise
# Purpose:     provide user interface (GUI, Help, Details, Graph)
#
# Author:      nlrgo
#
# Created:     12-10-2013
# Copyright:   (c) nlrgo 2013
# Licence:     <your licence>
# -------------------------------------------------------------------------------

import sys
import glob
import copy
import threading
import shutil
import subprocess
import collections

from PyQt5.QtCore import pyqtSlot, QTimer, QSize, Qt, QItemSelectionModel, QUrl, QSizeF
from PyQt5.QtGui import QIcon, QMovie, QTextCursor, QColor, QTextFormat, QGuiApplication, QPixmap, QTextDocument, QKeySequence
from PyQt5.QtWidgets import QApplication, QAction, QMenu, QMainWindow, QDialog

from LW_BUILD_INFO import app_name, version, build
from modClasses import *
from modClassesGUI import *
from modFunc import sizeof_fmt, load_config_from, save_config_to, unique_name, list_get, read_reg, write_reg
from modLoader2 import loaderClass

from GUI import Ui_MainWindow
from Help import Ui_Dialog
from Details import Ui_Details
from Graph import Ui_widGraph

curobj = ['table', 'result', 'view', 'index', 'rowidx', 'colidx', 'model', 'colnam', 'value']
TABLE, RESULT, VIEW, INDEX, ROWIDX, COLIDX, MODEL, COLNAM, VALUE = curobj

class FrmClass(QMainWindow, Ui_MainWindow):

    def __init__(self, silent=False):
        QMainWindow.__init__(self)
        self.setupUi(self)
        import common

        self.block = False
        self.on_off = False
        self.led_off = QPixmap(":/led_off")
        self.led_orange = QPixmap(":/led_orange")
        self.led_red = QPixmap(":/led_red")
        self.dirty = ''
        self.loc_clip = self.msg_clip = self.fld_clip = self.scr_clip = self.res_clip = []
        self.frmDetails = frmDetailsClass(self)
        self.frmHelp = FrmHelpClass(self)
        self.frmGraph = frmGraphClass(self)
        self.txtDatabaseLocation.setText(common.default_dbase_folder)
        OutputClass(silent, self)
        self.cmd_request = QTimer()
        self.splitter_3.setSizes([300, 300])         # set messages/fields splitter sizes

        # export to common
        common.gui = self       # gui is instantiated
        common.msg_box = self.msg_box
        common.display_info = self.display_info
        common.display_error = self.display_error
        common.run_current = self.run_current
        common.current_table = lambda: self.current(TABLE)      # current selected result table
        common.current_column = lambda: self.current(COLNAM)     # current selected result field

        # set message and field table
        self.tblMessages.horizontalHeader().setVisible(True)
        self.tblMessages.horizontalHeader().setStretchLastSection(True)
        self.tblMessages.setItemDelegateForColumn(2, FieldsDelegate(self.tblMessages))
        self.tblFields.horizontalHeader().setVisible(True)
        self.tblFields.setItemDelegateForRow(2, FieldsDelegate(self.tblFields))

        # set highlighter
        self.highlighter = PythonHighlighter(self.txtScriptEdit.document())

        # add context actions
        self.lstLocations.addAction(self.actionEnable)
        self.lstLocations.addAction(self.actionDisable)
        self.lstLocations.addAction(self.actionNew)
        self.lstLocations.addAction(self.actionCopy)
        self.lstLocations.addAction(self.actionPaste)
        self.lstLocations.addAction(self.actionDelete)

        self.tblMessages.addAction(self.actionTestMessage)
        action = QAction('Set Message Type', self.tblMessages)
        menu = QMenu(self.tblMessages)
        menu.addAction(self.actionLineType)
        menu.addAction(self.actionMultilineType)
        menu.addAction(self.actionXMLType)
        action.setMenu(menu)
        self.tblMessages.addAction(action)
        self.tblMessages.addAction(self.actionEnable)
        self.tblMessages.addAction(self.actionDisable)
        self.tblMessages.addAction(self.actionNew)
        self.tblMessages.addAction(self.actionCopy)
        self.tblMessages.addAction(self.actionPaste)
        self.tblMessages.addAction(self.actionDelete)

        self.tblFields.addAction(self.actionHide)
        self.tblFields.addAction(self.actionUnhide)
        self.tblFields.addAction(self.actionNew)
        self.tblFields.addAction(self.actionCopy)
        self.tblFields.addAction(self.actionPaste)
        self.tblFields.addAction(self.actionDelete)

        self.lstScripts.addAction(self.actionRunScript)

        action = QAction('Set Script Type', self.lstScripts)
        menu = QMenu(self.lstScripts)
        menu.addAction(self.actionSQLType)
        menu.addAction(self.actionPythonType)
        action.setMenu(menu)
        self.lstScripts.addAction(action)

        self.lstScripts.addAction(self.actionNew)
        self.lstScripts.addAction(self.actionCopy)
        self.lstScripts.addAction(self.actionPaste)
        self.lstScripts.addAction(self.actionDelete)

        self.txtScriptEdit.addAction(self.actionRunStatement)
        self.txtScriptEdit.addAction(self.actionTableName)
        self.txtScriptEdit.addAction(self.actionColumnName)
        self.txtScriptEdit.addAction(self.actionPasteCellValue)

        self.lstResults.addAction(self.actionImport)
        self.lstResults.addAction(self.actionExport)
        self.lstResults.addAction(self.actionCopy)
        self.lstResults.addAction(self.actionPaste)
        self.lstResults.addAction(self.actionDelete)
        self.lstResults.addAction(self.actionCombineTables)
        self.lstResults.addAction(self.actionMergeTables)
        self.lstResults.addAction(self.actionIndexTable)

        self.lstHighlights.addAction(self.actionDelete)

        # connect signals
        self.radOutputMapped.clicked.connect(self.update_settings)
        self.radOutputCombined.clicked.connect(self.update_settings)
        self.radOutputMerged.clicked.connect(self.update_settings)
        self.chkUseHeader.clicked.connect(self.update_settings)
        self.txtDelimiter.textEdited.connect(self.update_settings)
        self.txtQuoteCharacter.textEdited.connect(self.update_settings)
        self.txtLineTerminator.textEdited.connect(self.update_settings)
        self.radFileName.clicked.connect(self.update_settings)
        self.radFileDate.clicked.connect(self.update_settings)
        self.chkDebug.clicked.connect(self.update_settings)
        self.ledFileFromDate.textEdited.connect(self.update_settings)
        self.chkShowFolder.clicked.connect(self.update_settings)
        self.chkShowFile.clicked.connect(self.update_settings)
        self.chkShowSize.clicked.connect(self.update_settings)
        self.chkShowOffset.clicked.connect(self.update_settings)
        self.chkShowMessage.clicked.connect(self.update_settings)
        self.chkShowPath.clicked.connect(self.update_settings)
        self.boxMaxFiles.valueChanged.connect(self.update_settings)
        self.ledExternalCmd.textEdited.connect(self.update_settings)

        self.btnNewLocation.clicked.connect(lambda: self.newPasteLocation('new'))
        self.btnNewMessage.clicked.connect(lambda: self.newPasteMessage('new'))
        self.btnNewField.clicked.connect(lambda: self.newPasteField('new'))
        self.btnNewScript.clicked.connect(lambda: self.newPasteScript('new'))
        self.btnNewHighlight.clicked.connect(lambda: self.search('new'))
        self.tblMessages.verticalHeader().sectionMoved.connect(self.message_moved)  # message row moved
        self.tblFields.horizontalHeader().sectionMoved.connect(self.field_moved)  # field column moved

        self.btnDeleteLocation.clicked.connect(lambda: self.copyDeleteLocation(do_delete=True))
        self.btnDeleteMessage.clicked.connect(lambda: self.copyDeleteMessage(do_delete=True))
        self.btnDeleteField.clicked.connect(lambda: self.copyDeleteField(do_delete=True))
        self.btnDeleteScript.clicked.connect(lambda: self.copyDeleteScript(do_delete=True))
        self.btnDeleteHighlight.clicked.connect(lambda: self.search('delete'))
        self.lstLocations.sig_dropped.connect(self.sig_dropped_handle)

        self.actionLineType.triggered.connect(lambda: self.set_message_type('line'))
        self.actionMultilineType.triggered.connect(lambda: self.set_message_type('multi'))
        self.actionXMLType.triggered.connect(lambda: self.set_message_type('xml'))
        self.actionSQLType.triggered.connect(lambda: self.set_script_type('query'))
        self.actionPythonType.triggered.connect(lambda: self.set_script_type('python'))
        self.actionStop.triggered.connect(self.do_stop)
        self.actionLoad.triggered.connect(self.do_load)
        self.actionTestMessage.triggered.connect(self.do_test)
        self.actionRepeat.triggered.connect(self.do_repeat)

        # status bar (info, totals)
        self.prev_info_text = ''
        self.lblTotals = QLabel()
        self.statusBar.addPermanentWidget(self.lblTotals)

        # timers
        self.edit_timer = QTimer()

        self.settings_timer = QTimer()
        self.settings_timer.timeout.connect(self.update_settings)

        self.timer = QTimer()
        self.timer.start(250)
        self.timer.timeout.connect(self.handle_timeout)

        self.interval = intervalClass(0.5)

    def handle_timeout(self):
        """Timeout handler"""

        if os.path.exists(common.INBOX_FILE):    # open files
            with open(common.INBOX_FILE) as f:
                for file in f.read().splitlines():
                    new_loc = locationClass(file)       # create new location
                    common.locations.append(new_loc)    # append to locations
            self.display_locations()
            self.showMaximized() if self.isMaximized() else self.showNormal()   # restore window
            self.activateWindow()                       # bring to front
            os.remove(common.INBOX_FILE)

        if self.ledFileFromDate.hasFocus(): self.statusBar.showMessage(
            'Filter value = ' + str(evalx(self.ledFileFromDate.text())))  # display current crit value

        # progress
        percent = int(100 * common.ldr.bytes_processed / (common.ldr.bytes_total)) if common.ldr.bytes_total > 0 else 0
        self.progressBar.setFormat(sizeof_fmt(common.ldr.bytes_processed) + ' processed')
        self.progressBar.setValue(percent)

        # totals
        self.lblTotals.setText(
            sizeof_fmt(common.ldr.bytes_total) + ', ' + str(common.ldr.messages_loaded) + ' messages')

        # busy
        if common.busy or self.btnRepeat.isChecked():
            self.on_off = not self.on_off
            self.labLED.setPixmap(self.led_red if self.on_off else self.led_orange)

            if next(self.interval):
                self.display_messages()
                self.refresh_results()
        else:
            self.labLED.setPixmap(self.led_off)

        # scroll to bottom
        if self.actionDisplayLastRow.isChecked():
            self.chkLastRow.setChecked(True)
            table, view = self.current(TABLE, VIEW)
            if table:
                view.scrollToBottom()
        else:
            self.chkLastRow.setChecked(False)

    # ===ACTIONS============================================================

    @pyqtSlot(bool)
    def do_stop(self):
        """Stop"""

        common.set_debug('{} (caller: {})'.format(sys._getframe(0).f_code.co_name, sys._getframe(1).f_code.co_name))
        common.set_info('Cancelled')

        if common.cancel:
            common.ldr.do_load(reset=True)    # cancel
        common.cancel = True  # cancel!
        common.busy = False
        self.actionRepeat.setChecked(False)
        self.refresh_results()
        QApplication.processEvents()

    @pyqtSlot()
    def do_load(self):
        """Run"""

        if common.busy:
            return
        common.busy = True  # load

        common.set_info('Load started...')
        tim = time.time()
        QApplication.processEvents()

        exec_script('#load_begin')  # run pre-script

        common.ldr.do_load(load=True)  # load from start to database

        exec_script('#load_end')  # run post-script

        self.display_messages()
        self.refresh_results()

        common.set_info('{} message(s) loaded in {:.1f} seconds'.format(common.ldr.messages_loaded, time.time() - tim))
        self.save_config()  # save configuration

        common.busy = False

    @pyqtSlot()
    def do_test(self):
        """Test one message"""

        tblM = self.tblMessages
        mcr = tblM.currentRow()
        if common.busy or mcr < 0:
            return

        common.busy = True  # test
        QApplication.processEvents()

        test_msg = common.messages[mcr]   # get message to test

        enabled_state = test_msg.enabled    # save current state
        test_msg.enabled = True             # enable message
        common.ldr.do_load(test=[test_msg])  # test one next message
        test_msg.enabled = enabled_state    # restore state

        self.display_messages()

        common.busy = False

    @pyqtSlot()
    def do_repeat(self):
        """Repeat"""

        if common.busy:
            return
        common.busy = True  # repeat
        common.set_info('Repeat started...')
        self.btnRepeat.setChecked(True)
        self.actionRepeat.setChecked(True)

        while self.actionRepeat.isChecked() and self.btnRepeat.isChecked():

            common.ldr.do_load(tail=True)  # load continued to database

            if common.cancel:
                self.actionRepeat.setChecked(False)

            start = time.time()
            while time.time() < (start + 0.5):
                QApplication.processEvents()
                time.sleep(0.01)

        self.btnRepeat.setChecked(False)
        self.actionRepeat.setChecked(False)
        common.set_info('Repeat stopped')

        self.display_messages()
        self.refresh_results()

        self.save_config()  # save configuration
        common.busy = False

    @pyqtSlot()
    def on_actionRunScript_triggered(self):
        """Run script"""

        self.run_current()  # run full script

    @pyqtSlot()
    def on_actionRunStatement_triggered(self):
        """Run statement"""

        self.run_current(True)  # run selected block

    def run_current(self, partial=False):
        """Execute the current selected script (partial: only marked/selected code)"""
        common.set_debug(sys._getframe(0).f_code.co_name)

        start_time = time.time()

        if common.busy:
            return

        common.busy = True  # run script
        common.cancel = False
        QApplication.processEvents()


        lstS = self.lstScripts
        item = lstS.currentItem()
        script = item.data(QtCore.Qt.UserRole)

        exec_script('#script_begin') if not script.name.startswith('#script_begin') else None     # run pre-script

        if partial:
            block = self.txtScriptEdit.text_block()    # run text block if partial
            exec_code(block, script.type, script.name)
        else:
            exec_script(script.name)

        exec_script('#script_end') if not script.name.startswith('#script_end') else None     # run post-script

        self.refresh_results()

        if script.type == 'query':     # only for SQL scripts
            self.set_current_result(script.name)  # select result (if any)

        common.set_info('Script executed in {0:.1f} seconds'.format(time.time() - start_time))
        common.busy = False

    @pyqtSlot(bool)
    def on_actionSingleRecord_triggered(self):
        """Toggle Single Record pane"""
        self.frmDetails.show()
        self.frmDetails.populate()
        self.frmDetails.activateWindow()    # bring to front

    @pyqtSlot(bool)
    def on_actionConfiguration_triggered(self):
        """Toggle Configuration pane"""
        if self.tabWidget.isVisible():
            self.tabWidget.hide()
        else:
            self.tabWidget.show()
        self.update_settings()

    @pyqtSlot(bool)
    def on_actionResultlist_triggered(self):
        """Toggle Result list pane"""
        if self.lstResults.isVisible():
            self.lstResults.hide()
        else:
            self.lstResults.show()
        self.update_settings()

    def mousePressEvent(self, event):
        lstR = self.lstResults

        pos = self.tabWidget.mapTo(self, self.tabWidget.pos())
        size = self.tabWidget.size()

        top = pos.y()
        left = pos.x()
        bottom = top + size.height()
        right = left + size.width()

        # hide/show message/field view
        if self.tabWidget.currentIndex() == 1 and event.y() < bottom:
            if not 0 in self.splitter_3.sizes():        # both views visible
                common.settings['splitter_state'] = self.splitter_3.saveState()
                if event.x() < left:
                    self.splitter_3.setSizes([0, self.splitter_3.sizes()[1]])    # hide message view

                elif event.x() > right:
                    self.splitter_3.setSizes([self.splitter_3.sizes()[0], 0])    # hide message view
            else:
                self.splitter_3.restoreState(common.settings['splitter_state'])
            self.splitter_3.refresh()

        if event.x() < left and event.y() > bottom:     # hide/show results list
            if self.lstResults.isVisible():
                self.lstResults.hide()
            else:
                self.lstResults.show()
            self.update_settings()

        if event.y() < top + 25:                 # hide/show configuration pane
            if self.tabWidget.isVisible():
                self.tabWidget.hide()
            else:
                self.tabWidget.show()
            self.update_settings()

    @pyqtSlot(bool)
    def on_actionTools_triggered(self):
        """Toggle Tools dock widget"""
        if self.dockTools.isVisible():
            self.dockTools.hide()
        else:
            self.dockTools.show()
        self.update_settings()

    @pyqtSlot(bool)
    def on_actionHelp_triggered(self):
        """Show Help page"""
        self.frmHelp.show()

    @pyqtSlot(bool)
    def on_actionAbout_triggered(self):
        """Show About Window"""

        about = "Author:\t{}\nEmail:\t{}\n\nVersion:\t{}\nBuild:\t{}\t\t".format('Ruud Gommers', 'writinghand@gmail.com', version, build)
        QMessageBox.about(QMessageBox(), app_name, about)

    @pyqtSlot(bool)
    def on_actionExit_triggered(self):
        self.close()

    @pyqtSlot(bool)
    def on_btnDatabaseLocation_clicked(self):
        dialog = QFileDialog(self)
        dialog.setDirectory(common.default_dbase_folder)    # default folder
        dialog.setFileMode(QFileDialog.Directory)           # select directory
        if dialog.exec():
            common.default_dbase_folder = dialog.selectedFiles()[0]
            common.db.conn.close()                          # close database
            common.db.connect(common.default_dbase_folder)  # connect same instance again with new file
            common.ldr = loaderClass(common.db)           # new loader instance

            write_reg('dbase_folder', common.default_dbase_folder)
            self.txtDatabaseLocation.setText(common.default_dbase_folder)

    # ===SETTINGS/CONFIGURATION================================================

    def update_settings(self):
        """Store settings"""

        self.settings_timer.stop()  # stop resize timer

        if self.block:
            return  # don't save during loading

        common.set_debug(sys._getframe(0).f_code.co_name)

        cfg = common.settings
        cfg['mapped'] = self.radOutputMapped.isChecked()
        cfg['combined'] = self.radOutputCombined.isChecked()
        cfg['merged'] = self.radOutputMerged.isChecked()
        cfg['header'] = self.chkUseHeader.isChecked()
        cfg['delimiter'] = self.txtDelimiter.text()
        cfg['quotecharacter'] = self.txtQuoteCharacter.text()
        cfg['lineterminator'] = self.txtLineTerminator.text()
        cfg['file_name'] = self.radFileName.isChecked()
        cfg['file_date'] = self.radFileDate.isChecked()
        cfg['geometry'] = self.saveGeometry()
        cfg['theme'] = self.cmbSetTheme.currentText()
        cfg['debug'] = self.chkDebug.isChecked()
        cfg['config_pane'] = self.tabWidget.isVisible()
        cfg['list_pane'] = self.lstResults.isVisible()
        cfg['tools_pane'] = self.dockTools.isVisible()
        cfg['from_file_date'] = self.ledFileFromDate.text()
        cfg['show_info'] = [self.chkShowFolder.isChecked(),self.chkShowFile.isChecked(),
                            self.chkShowSize.isChecked(),self.chkShowOffset.isChecked(),
                            self.chkShowMessage.isChecked(),self.chkShowPath.isChecked()]
        cfg['max_files'] = self.boxMaxFiles.value()
        cfg['external'] = self.ledExternalCmd.text()

        # highlight LOGDATA if in combined/merged mode
        if common.settings.get('mapped', False):
            self.set_result_color('LOGDATA', 'black')
        else:
            self.set_result_color('LOGDATA', 'red')

        self.save_config()      # save global settings
        self.display_fields()   # update info fields (NB: after recompile during save)

    def display_settings(self):
        """Update settings"""
        common.set_debug(sys._getframe(0).f_code.co_name)

        self.block = True
        self.cmbSetTheme.blockSignals(True)
        self.cmbSetTheme.clear()
        self.cmbSetTheme.addItem('Default')
        for qss in glob.glob('stylesheets/*.qss'):  # load combobox with available style sheets (.qss)
            base = os.path.basename(qss)
            self.cmbSetTheme.addItem(os.path.splitext(base)[0])
        self.cmbSetTheme.blockSignals(False)
        try:
            cfg = common.settings
            self.radOutputMapped.setChecked(bool(cfg.get('mapped', False)))
            self.radOutputCombined.setChecked(bool(cfg.get('combined', True)))
            self.radOutputMerged.setChecked(bool(cfg.get('merged', False)))
            self.chkUseHeader.setChecked(bool(cfg.get('header', False)))
            self.txtDelimiter.setText(str(cfg.get('delimiter', ',')))
            self.txtQuoteCharacter.setText(str(cfg.get('quotecharacter', '"')))
            self.txtLineTerminator.setText(str(cfg.get('lineterminator', '\\n')))   # '\n' gives unpredictable results!
            self.radFileName.setChecked(bool(cfg.get('file_name', True)))
            self.radFileDate.setChecked(bool(cfg.get('file_date', False)))
            if 'geometry' in cfg: self.restoreGeometry(cfg['geometry'])
            self.cmbSetTheme.setCurrentText(cfg.get('theme', 'Default'))
            self.chkDebug.setChecked(bool(cfg.get('debug', False)))
            self.tabWidget.setVisible(cfg.get('config_pane', True))
            self.lstResults.setVisible(cfg.get('list_pane', True))
            self.dockTools.setVisible(cfg.get('tools_pane', True))
            self.ledFileFromDate.setText(cfg.get('from_file_date', ''))
            show_info = cfg.get('show_info', [False]*6)
            self.chkShowFolder.setChecked(show_info[0])
            self.chkShowFile.setChecked(show_info[1])
            self.chkShowSize.setChecked(show_info[2])
            self.chkShowOffset.setChecked(show_info[3])
            self.chkShowMessage.setChecked(show_info[4])
            self.chkShowPath.setChecked(show_info[5])
            self.boxMaxFiles.setValue(cfg.get('max_files', 0))
            self.ledExternalCmd.setText(cfg.get('external') or r'C:\Program Files (x86)\Notepad++\notepad++.exe')

        except Exception as err:
            common.set_error('Missing setting: ' + str(err))

        self.block = False

    def resizeEvent(self, event):
        self.settings_timer.start(1000)

    def moveEvent(self, event):
        self.settings_timer.start(1000)

    @pyqtSlot(bool)
    def on_btnSetTheme_clicked(self):
        """Set theme"""

        for qss in glob.glob('*.qss'):
            base = os.path.basename(qss)
            self.cmbSetTheme.addItem(os.path.splitext(base)[0])

    @pyqtSlot(int)
    def on_cmbSetTheme_currentIndexChanged(self):
        """Set theme"""
        common.set_debug(sys._getframe(0).f_code.co_name)

        if self.cmbSetTheme.currentText() == 'Default':
            self.setStyleSheet('')
        else:
            with open('stylesheets/' + self.cmbSetTheme.currentText() + '.qss') as qss:
                self.setStyleSheet(qss.read())
        self.update_settings()

    @pyqtSlot(bool)
    def on_actionClearConfig_triggered(self):
        """Clear config"""

        clear = QMessageBox.question(self, app_name, 'Clear all configuration?')
        if clear == QMessageBox.No:
            return

        common.locations = []   # clear locations
        self.display_locations()

        common.messages = [msg for msg in common.messages if msg.name.startswith('#')]    # clear messages (if not starts with #)
        self.display_messages()

        common.scripts = [scr for scr in common.scripts if scr.name.startswith('#')]    # clear scripts (if not starts with #)
        self.display_scripts()

        tables = [tbl for tbl in common.db.db_tables() if not tbl.startswith('#')]      # clear tables (if not starts with #)
        for table in tables: # drop tables
            common.db.conn.execute('DROP TABLE IF EXISTS "{}"'.format(table))
        self.refresh_results()

        common.set_info('Config cleared')
        self.save_config()

    @pyqtSlot(bool)
    def on_actionOpenConfig_triggered(self):
        """Open config"""

        dialog = QFileDialog(self)
        config_folder = read_reg('config_folder', os.path.abspath('configs'))
        dialog.setDirectory(config_folder)              # default folder
        dialog.setFileMode(QFileDialog.ExistingFile)    # select existing file
        dialog.setNameFilters(["Config files (*.cfg)", "All files (*)"])
        if dialog.exec():
            common.current_config_file = dialog.selectedFiles()[0]
            self.load_config(common.current_config_file)  # open
            write_reg('config_folder', os.path.split(common.current_config_file)[0])

    @pyqtSlot(bool)
    def on_actionMergeConfig_triggered(self):
        """Merge config"""

        dialog = QFileDialog(self)
        config_folder = read_reg('config_folder', os.path.abspath('configs'))
        dialog.setDirectory(config_folder)              # default folder
        dialog.setFileMode(QFileDialog.ExistingFile)    # select existing file
        dialog.setNameFilters(["Config files (*.cfg)", "All files (*)"])
        if dialog.exec():
            selected_file = dialog.selectedFiles()[0]
            self.load_config(selected_file, full=False)  # merge

    @pyqtSlot(bool)
    def on_actionSaveConfig_triggered(self):
        """Save config"""

        self.save_config(force=True)  # save

    @pyqtSlot(bool)
    def on_actionSaveAsConfig_triggered(self):
        """Save As config"""

        dialog = QFileDialog(self)
        config_folder = read_reg('config_folder', os.path.abspath('configs'))
        dialog.setDirectory(config_folder)              # default folder
        dialog.setAcceptMode(QFileDialog.AcceptSave)
        dialog.setDefaultSuffix('cfg')
        dialog.setNameFilters(["Config files (*.cfg)", "All files (*)"])
        if dialog.exec():
            common.current_config_file = dialog.selectedFiles()[0]
            self.save_config(force=True)  # save as
            write_reg('config_folder', os.path.split(common.current_config_file)[0])

    @pyqtSlot(bool)
    def on_actionNew_triggered(self):
        """New"""

        if self.lstLocations.hasFocus():
            self.newPasteLocation('new')
        elif self.tblMessages.hasFocus():
            self.newPasteMessage('new')
        elif self.tblFields.hasFocus():
            self.newPasteField('new')
        elif self.lstScripts.hasFocus():
            self.newPasteScript('new')
        elif self.lstResults.hasFocus():
            self.newPasteResults('new')
        elif self.ledSearchText.hasFocus():
            self.search('new')

    @pyqtSlot(bool)
    def on_actionCut_triggered(self):
        """Cut"""

        if self.lstLocations.hasFocus():
            self.copyDeleteLocation(True, True)
        elif self.tblMessages.hasFocus():
            self.copyDeleteMessage(True, True)
        elif self.tblFields.hasFocus():
            self.copyDeleteField(True, True)
        elif self.lstScripts.hasFocus():
            self.copyDeleteScript(True, True)
        elif self.lstResults.hasFocus():
            self.copyDeleteResult(True, True)
        elif self.splitter.widget(1).hasFocus():
            self.copyCells()

    @pyqtSlot(bool)
    def on_actionCopy_triggered(self):
        """Copy"""

        if self.lstLocations.hasFocus():
            self.copyDeleteLocation(do_copy=True)
        elif self.tblMessages.hasFocus():
            self.copyDeleteMessage(do_copy=True)
        elif self.tblFields.hasFocus():
            self.copyDeleteField(do_copy=True)
        elif self.lstScripts.hasFocus():
            self.copyDeleteScript(do_copy=True)
        elif self.lstResults.hasFocus():
            self.copyDeleteResult(do_copy=True)
        elif self.splitter.widget(1).hasFocus():
            self.copyCells()

    @pyqtSlot(bool)
    def on_actionPaste_triggered(self):
        """Paste"""

        if self.lstLocations.hasFocus():
            self.newPasteLocation(self.loc_clip)
        elif self.tblMessages.hasFocus():
            self.newPasteMessage(self.msg_clip)
        elif self.tblFields.hasFocus():
            self.newPasteField(self.fld_clip)
        elif self.lstScripts.hasFocus():
            self.newPasteScript(self.scr_clip)
        elif self.lstResults.hasFocus():
            self.newPasteResults(self.res_clip)

    @pyqtSlot(bool)
    def on_actionDelete_triggered(self):
        """Delete"""

        if self.lstLocations.hasFocus():
            self.copyDeleteLocation(do_delete=True)
        elif self.tblMessages.hasFocus():
            self.copyDeleteMessage(do_delete=True)
        elif self.tblFields.hasFocus():
            self.copyDeleteField(do_delete=True)
        elif self.lstScripts.hasFocus():
            self.copyDeleteScript(do_delete=True)
        elif self.lstResults.hasFocus():
            self.copyDeleteResult(do_delete=True)
        elif self.lstHighlights.hasFocus():
            self.search('delete')

    @pyqtSlot(bool)
    def on_actionEnable_triggered(self):  # enable

        lstL = self.lstLocations
        tblM = self.tblMessages
        lstL.blockSignals(True)
        tblM.blockSignals(True)

        if lstL.hasFocus():
            items = lstL.selectedItems()
            [item.setCheckState(QtCore.Qt.Checked) for item in items]
            self.update_locations()
        elif tblM.hasFocus():
            items = tblM.selectedItems()
            [tblM.item(item.row(), 0).setCheckState(QtCore.Qt.Checked) for item in items]
            self.update_messages()

        lstL.blockSignals(False)
        tblM.blockSignals(False)

    @pyqtSlot(bool)
    def on_actionDisable_triggered(self):  # disable

        lstL = self.lstLocations
        tblM = self.tblMessages
        lstL.blockSignals(True)
        tblM.blockSignals(True)

        if lstL.hasFocus():
            items = lstL.selectedItems()
            [item.setCheckState(QtCore.Qt.Unchecked) for item in items]
            self.update_locations()
        elif tblM.hasFocus():
            items = tblM.selectedItems()
            [tblM.item(item.row(), 0).setCheckState(QtCore.Qt.Unchecked) for item in items]
            self.update_messages()

        lstL.blockSignals(False)
        tblM.blockSignals(False)

    @pyqtSlot(bool)
    def on_actionHide_triggered(self):
        """Hide selected"""

        if self.tblFields.hasFocus():
            self.hide_fields()
        elif QApplication.focusWidget().objectName()=='viewResults':
            self.hide_columns()

    @pyqtSlot(bool)
    def on_actionUnhide_triggered(self):
        """Unhide all"""

        if self.tblFields.hasFocus():
            self.unhide_fields()
        elif QApplication.focusWidget().objectName() == 'viewResults':
            self.unhide_columns()

    # ==LOCATIONS================================================

    def newPasteLocation(self, clip='new'):
        """New or Paste location"""

        lstL = self.lstLocations

        if clip=='new':   # new
            dialog = QFileDialog(self)
            if lstL.currentRow() == -1:
                dialog.setDirectory(r'\\')  # root folder
            else:
                dialog.setDirectory(os.path.dirname(lstL.currentItem().text()))  # folder containing current file
            dialog.setFileMode(QFileDialog.ExistingFiles)
            dialog.setNameFilters(["All files (*)", "Log files (*.log *.xml)", "Text files (*.txt *.dat *.log)"])
            if dialog.exec():
                for r in range(lstL.count()):  # disable all existing locations
                    lstL.item(r).setCheckState(QtCore.Qt.Unchecked)

                for file in dialog.selectedFiles(): # append all selected locations
                    new_loc = locationClass(file)
                    common.locations.append(new_loc)

        else:    # paste
            for loc in reversed(clip):
                common.locations.append(copy.deepcopy(loc))

        self.display_locations()
        self.save_config()  # save global locations

    def copyDeleteLocation(self, do_delete=False, do_copy=False):
        """Copy or Delete location"""

        lstL = self.lstLocations
        if do_copy:
            self.loc_clip = []

        locations = [lstL.item(r).data(QtCore.Qt.UserRole) for r in range(lstL.count())]
        sel_rownrs = [i.row() for i in lstL.selectedIndexes()]
        for r in reversed(sel_rownrs):
            if do_copy:
                self.loc_clip.append(locations[r])   # copy

            if do_delete:
                lstL.takeItem(r)  # take item from the list
                lstL.setCurrentRow(min(r, lstL.count() - 1))

        self.update_locations()
        self.display_locations()

    def display_locations(self):
        """Display all locations"""
        common.set_debug(sys._getframe(0).f_code.co_name)

        lstL = self.lstLocations
        sel_rownrs = [i.row() for i in lstL.selectedIndexes()]    # save selected rows nrs
        lcr = max(lstL.currentRow(), 0)  # lower limit 0

        lstL.blockSignals(True)
        lstL.clear()

        for loc in common.locations:
            item = QListWidgetItem(loc.name)  # loc name
            item.setData(QtCore.Qt.UserRole, loc)  # message object
            item.setFlags(item.flags() | Qt.ItemIsEditable)
            state = QtCore.Qt.Checked if loc.enabled else QtCore.Qt.Unchecked
            item.setCheckState(state)  # loc state
            lstL.addItem(item)

        [lstL.item(r).setSelected(True) for r in range(lstL.count()) if r in sel_rownrs]
        lstL.setCurrentRow(lcr)
        lstL.blockSignals(False)

    def update_locations(self):
        """Store all locations in global\configuration"""
        common.set_debug(sys._getframe(0).f_code.co_name)

        lstL = self.lstLocations

        # store locs
        for r in range(lstL.count()):  # for each location row
            loc = lstL.item(r).data(QtCore.Qt.UserRole)
            loc.name = lstL.item(r).text()      # store location name
            state = lstL.item(r).checkState()  # store location state
            loc.enabled = True if state == QtCore.Qt.Checked else False

        common.locations = [lstL.item(r).data(QtCore.Qt.UserRole) for r in range(lstL.count())]
        self.save_config()  # save global locations

    def sig_dropped_handle(self):   # files dropped on lstLocations
        self.display_locations()    # show locations in list
        self.update_locations()     # save locations

    def on_lstLocations_itemChanged(self, item):
        common.set_debug(sys._getframe(0).f_code.co_name)
        self.update_locations()

    def on_txtInFile_textEdited(self, text):
        common.set_debug(sys._getframe(0).f_code.co_name)
        common.ldr.fileselect = text.strip()

    @pyqtSlot(bool)
    def on_actionListFiles_triggered(self):
        """List enabled files and attributes"""

        common.set_info('List files...')
        QApplication.processEvents()

        if common.busy:
            return
        common.busy = True  # list files

        common.db.conn.execute("CREATE TABLE IF NOT EXISTS 'FILES' ('file.name', 'file.size', 'file.time', 'file.path')")
        common.db.conn.execute("DELETE FROM FILES")

        common.ldr.update_files()

        for file in common.ldr.files:
            vals = [file.name, str(file.size),str(file.time), file.path]
            sql = "INSERT INTO 'FILES' ('file.name','file.size','file.time','file.path') VALUES " + "('" + ("','".join(vals)) + "')"
            common.db.conn.execute(sql)
        common.db.commit()

        self.refresh_results()

        lstR = self.lstResults  # set focus to result 'FILES'
        self.set_current_result('FILES')  # select result (if any)

        common.busy = False

    # ==MESSAGES==================================================

    def newPasteMessage(self, clip='new'):
        """New or Paste message"""

        tblM = self.tblMessages
        mcr = max(0, tblM.currentRow())  # lower limit 0

        if clip=='new':  # new
            mcr += 1
            msg_new = msgClass()
            msg_new.name = unique_name(msg_new.name, common.messages)

            msg_new.fields.append(fldClass())
            common.messages.insert(mcr, msg_new)

        else:  # paste
            for msg in reversed(clip):
                mcr += 1
                msg_paste = copy.deepcopy(msg)
                msg_paste.cnt = 0
                msg_paste.enabled = False
                msg_paste.name = unique_name(msg_paste.name, common.messages)
                common.messages.insert(mcr, msg_paste)

        tblM.setRowCount(len(common.messages))      # set row count to increased number of messages
        tblM.clearSelection()
        tblM.setCurrentCell(mcr, 0)                 # and select message after the last inserted message

        self.display_messages()  # display changed messages
        self.save_config()  # save configuration

    def newPasteField(self, clip='new'):
        """New or Paste field"""

        tblM = self.tblMessages
        tblF = self.tblFields
        mcr = tblM.currentRow()
        fcc = tblF.currentColumn()
        if mcr < 0:
            return

        msg = common.messages[mcr]

        if clip=='new':  # new
            fcc += 1
            fld_new = fldClass()
            fld_new.name = unique_name(fld_new.name, msg.fields)
            msg.fields.insert(fcc, fld_new)

        else:  # paste
            for fld in reversed(clip):
                fcc += 1
                fld_paste = copy.deepcopy(fld)
                fld_paste.name = unique_name(fld_paste.name, msg.fields)
                msg.fields.insert(fcc, fld_paste)  # paste field

        tblF.setColumnCount(len(msg.fields))    # set column count to increased number of fields
        tblF.setCurrentCell(0, fcc)             # and select field after the last inserted field

        self.display_fields()
        self.save_config()  # save configuration

    def copyDeleteMessage(self, do_delete=False, do_copy=False):
        """Copy or Delete message"""

        tblM = self.tblMessages
        tblM.blockSignals(True)
        mcr = tblM.currentRow()
        if mcr < 0:
            return

        sel_rowixs = [i.row() for i in tblM.selectedIndexes() if i.column() == 0]

        if do_copy:
            self.msg_clip = []

        for r in reversed(sel_rowixs):
            if do_copy:  # copy
                new_msg = copy.deepcopy(common.messages[r])
                self.msg_clip.append(new_msg)

            if do_delete:
                if not (common.messages[r].name.startswith('#') and len(sel_rowixs)>1):
                    tblM.removeRow(r)
                    common.messages.pop(r)
                    tblM.setCurrentCell(min(r, tblM.rowCount()-1), 0)  # upper limit last row

        self.display_messages()
        self.save_config()  # save configuration
        tblM.blockSignals(False)

    def copyDeleteField(self, do_delete=False, do_copy=False):
        """Copy or Delete field"""

        tblM = self.tblMessages
        tblF = self.tblFields
        mcr = tblM.currentRow()
        if mcr < 0:
            return

        msg = common.messages[mcr]  # current message
        sel_colixs = [i.column() for i in tblF.selectedIndexes() if i.row() == 0]
        if do_copy:
            self.fld_clip = []

        for c in reversed(sel_colixs):  # delete selected fields
            if do_copy:   # copy
                new_fld = copy.deepcopy(msg.fields[c])
                self.fld_clip.append(new_fld)
            if do_delete:
                tblF.removeColumn(c)
                msg.fields.pop(c)
                tblF.setCurrentCell(0, min(c, tblF.columnCount() - 1))  # upper limit last column

        self.save_config()  # save configuration

    def display_messages(self):
        """Display messages"""
        common.set_debug('{} (caller: {})'.format(sys._getframe(0).f_code.co_name, sys._getframe(1).f_code.co_name))

        tblM = self.tblMessages
##        mcr = max(tblM.currentRow(), 0)  # lower limit 0

        tblM.blockSignals(True)
        tblM.horizontalHeader().setVisible(True)
        tblM.verticalHeader().setVisible(True)
        tblM.verticalHeader().setSectionsMovable(True)
        tblM.setRowCount(len(common.messages))  # nof messages
        tblM.setColumnCount(4)  # nof columns
        tblM.setVerticalHeaderLabels(['   '] * tblM.rowCount())

        for r, msg in enumerate(common.messages):  # for each message

            item = QTableWidgetItem(msg.name)  # message name
            item.setFont(QFont('', -1, QFont.Bold if msg.wanted else QFont.Normal))     # set bold/normal
            item.setCheckState(Qt.Checked if msg.enabled else Qt.Unchecked)             # set checked/unchecked
            tblM.setIconSize(QSize(24, 16))
            if msg.type == 'line': item.setIcon(QIcon(':line'))
            if msg.type == 'multi': item.setIcon(QIcon(':multi'))
            if msg.type == 'xml': item.setIcon(QIcon(':xml'))
            tblM.setItem(r, 0, item)  # add to tablewidget

            item = QTableWidgetItem(str(msg.trigger))  # message trigger
            item.setToolTip(str(msg._trigger))
            tblM.setItem(r, 1, item)

            item = QTableWidgetItem(str(msg.edit))  # message edit
            tblM.setItem(r, 2, item)

            item = QTableWidgetItem(str(msg.cnt))  # message count
            item.setFlags(Qt.NoItemFlags)
            tblM.setItem(r, 3, item)

        if not common.messages or tblM.rowCount()==0:   # clear fields and return
            self.tblFields.setColumnCount(0)
            return

        tblM.horizontalHeader().resizeSections(QHeaderView.ResizeToContents)
        tblM.verticalHeader().resizeSections(QHeaderView.ResizeToContents)
        tblM.horizontalHeader().setStretchLastSection(True)
        tblM.blockSignals(False)

        self.display_fields()  # display fields of the current message

    def display_fields(self):
        """Display fields"""
        common.set_debug('{} (caller: {})'.format(sys._getframe(0).f_code.co_name, sys._getframe(1).f_code.co_name))

        tblF = self.tblFields
        try:
            msg = common.messages[self.tblMessages.currentRow()]
        except:
            return

        fcc = max(tblF.currentColumn(), 0)  # lower limit 0
        fields = [fld for fld in msg.fields if not fld.hidden]  # visisble fields

        tblF.blockSignals(True)
        tblF.verticalHeader().setVisible(True)
        tblF.horizontalHeader().setSectionsMovable(True)
        tblF.setRowCount(4)  # nof rows
        tblF.setColumnCount(len(fields))  # nof visible fields
        tblF.setHorizontalHeaderLabels([' '] * tblF.columnCount())
        tblF.setVerticalHeaderLabels(['Field','Id','Edit','Value'])

        for c, fld in enumerate(fields):  # for each visible field

            item = QTableWidgetItem(fld.name)  # set field name
            item.setFont(QFont('', -1, QFont.Bold))
            tblF.setItem(0, c, item)

            item = QTableWidgetItem(fld.id)  # set field id
            item.setTextAlignment(0x20)
            tblF.setItem(1, c, item)

            item = QTableWidgetItem(fld.edit)  # set field edit
            item.setTextAlignment(0x20)
            tblF.setItem(2, c, item)

            item = QTableWidgetItem(str(fld.value[:240]))  # set field value (limited)
            item.setForeground(QtGui.QColor('grey'))
            tblF.setItem(3, c, item)

        # tblF.horizontalHeader().resizeSections(QHeaderView.ResizeToContents)  # (moved to message_clicked)
        tblF.verticalHeader().resizeSections(QHeaderView.ResizeToContents)      # resize fields height

        tblF.clearSelection()
        tblF.setCurrentCell(0, fcc)
        tblF.blockSignals(False)

    def update_messages(self):
        """Update changes"""

        tblM = self.tblMessages
        tblF = self.tblFields

        # update item data
        for r in range(tblM.rowCount()):  # for each message row
            msg = common.messages[r]

            msg.name = tblM.item(r, 0).text()  # store message name
            state = tblM.item(r, 0).checkState()  # store message state
            msg.enabled = True if state == QtCore.Qt.Checked else False
            item = tblM.item(r, 1)  # store message trigger
            msg.trigger = item.text()
            item = tblM.item(r, 2)  # store message edit
            msg.edit = item.text()

            if r == tblM.currentRow():  # store fields of current message

                for c in range(tblF.columnCount()):  # for each message field
                    fld = msg.fields[c]

                    fld.name = tblF.item(0, c).text()  # store field name
                    item = tblF.item(1, c)  # store field id
                    fld.id = item.text()
                    item = tblF.item(2, c)  # store field steps
                    fld.edit = item.text()
                    item = tblF.item(3, c)  # store field value
                    fld.value = item.text()

        self.save_config()  # save configuration

    def hide_fields(self):
        """hide the selected fields"""

        try:
            msg = common.messages[self.tblMessages.currentRow()]
        except:
            return

        fields = [fld for fld in msg.fields if not fld.hidden]  # visisble fields
        sel_colixs = [i.column() for i in self.tblFields.selectedIndexes() if i.row() == 0]

        for c in sel_colixs:
            fields[c].hidden = True

        self.display_fields()

    def unhide_fields(self):
        """unhide all fields"""

        try:
            msg = common.messages[self.tblMessages.currentRow()]
        except:
            return

        for fld in msg.fields:
            fld.hidden = False

        self.display_fields()

    def field_moved(self, idx, oldVisIdx, newVisIdx):
        mcr = self.tblMessages.currentRow()
        if mcr < 0:
            return

        fields = common.messages[mcr].fields

        for c, fld in enumerate(fields[:]):  # order fields by visual order
            fields[self.tblFields.visualColumn(c)] = fld

        self.tblFields.setColumnCount(0)  # remove fields from table
        self.display_fields()  # and display them again
        self.tblFields.setCurrentCell(0, newVisIdx)

        self.save_config()

    def message_moved(self, idx, oldVisIdx, newVisIdx):

        for r, msg in enumerate(common.messages[:]):    # order messages by visual order
            common.messages[self.tblMessages.visualRow(r)] = msg

        self.tblMessages.setRowCount(0)     # remove messages from table
        self.display_messages()             # and display them again
        self.tblMessages.setCurrentCell(newVisIdx,0)

        self.save_config()

    def on_tblMessages_currentCellChanged(self, row, col):
        """Display the fields of the current message"""
        common.set_debug(sys._getframe(0).f_code.co_name, row)

        if self.tblMessages.item(row, 0):
            name = self.tblMessages.item(row, 0).text()
            self.set_current_result(name)  # select result (if any)
            self.display_fields()

    def on_tblMessages_cellClicked(self, row, col):
        if self.tblMessages.item(row, 0):
            name = self.tblMessages.item(row, 0).text()
            self.set_current_result(name)  # select result (if any)
            self.display_fields()
            self.tblFields.horizontalHeader().resizeSections(QHeaderView.ResizeToContents)  # resize fields width

    def on_tblMessages_cellChanged(self, row, col):
        """Message changed"""

        self.update_messages()
        self.tblMessages.horizontalHeader().resizeSections(QHeaderView.ResizeToContents)
        self.tblMessages.verticalHeader().resizeSections(QHeaderView.ResizeToContents)
        self.tblMessages.horizontalHeader().setStretchLastSection(True)

    def on_tblFields_cellChanged(self, row, col):
        """Field changed"""

        self.update_messages()
        self.tblFields.horizontalHeader().resizeSections(QHeaderView.ResizeToContents)
        self.tblFields.verticalHeader().resizeSections(QHeaderView.ResizeToContents)

    def set_message_type(self, type):
        mcr = self.tblMessages.currentRow()
        if mcr > -1:
            msg = common.messages[mcr]
            msg.name = unique_name(type, common.messages)
            msg.type = type
            msg.wanted = True
            self.display_messages()
            self.update_messages()
            self.save_config()

    def set_script_type(self, type):
        scr = self.lstScripts.currentRow()
        if scr > -1:
            script = common.scripts[scr]
            script.type = type
            self.display_scripts()
            self.update_scripts()

    # ==SCRIPTS====================================================

    def newPasteScript(self, clip='new'):
        """New or Paste script"""

        name = ''
        if clip=='new':   # new
            script_new = queryClass()
            script_new.name = name = unique_name(script_new.name, common.scripts)
            common.scripts.append(script_new)

        else:    # paste
            for script in reversed(clip):
                script_paste = copy.deepcopy(script)
                script_paste.name = name = unique_name(script_paste.name, common.scripts)
                common.scripts.append(script_paste)

        self.display_scripts()  # display changed scripts
        self.save_config()  # save configuration

        # set script (if any)
        lstS = self.lstScripts
        lstS.clearSelection()
        [lstS.setCurrentRow(i) for i in range(lstS.count()) if lstS.item(i).text() == name]

    def copyDeleteScript(self, do_delete=False, do_copy=False):
        """Copy or Delete script"""

        lstS = self.lstScripts
        if do_copy:
            self.scr_clip = []

        scripts = [lstS.item(r).data(QtCore.Qt.UserRole) for r in range(lstS.count())]
        sel_rowixs = [i.row() for i in lstS.selectedIndexes()]
        for r in reversed(sel_rowixs):
            if do_copy:
                self.scr_clip.append(scripts[r])  # copy

            if do_delete:
                if not (scripts[r].name.startswith('#') and len(sel_rowixs)>1):
                    lstS.takeItem(r)  # take item from the list
                    lstS.setCurrentRow(min(r, lstS.count() - 1))

        self.update_scripts()
        self.display_scripts()  # display changed scripts

    def display_scripts(self):
        """Display all scripts"""
        common.set_debug(sys._getframe(0).f_code.co_name)

        lstS = self.lstScripts

        sel_rownrs = [i.row() for i in lstS.selectedIndexes()]    # save selected rows nrs
        scr = max(lstS.currentRow(), 0)  # lower limit 0

        lstS.blockSignals(True)
        lstS.clear()

        for r, script in enumerate(common.scripts):
            self.set_header(script)
            item = QListWidgetItem(script.name)  # script name
            item.setData(QtCore.Qt.UserRole, script)  # script object
            item.setFlags(item.flags() | Qt.ItemIsEditable)
            if script.type == 'query': item.setIcon(QIcon(':sql'))
            if script.type == 'python': item.setIcon(QIcon(':python'))
            lstS.addItem(item)

        if not common.scripts or lstS.count == 0:   # clear code and return
            self.txtScriptEdit.clear()
            return

        [lstS.item(r).setSelected(True) for r in range(lstS.count()) if r in sel_rownrs]
        lstS.setCurrentRow(scr)
        lstS.blockSignals(False)

        self.display_code()

    def update_scripts(self):
        """Store all scripts in global\configuration"""
        common.set_debug(sys._getframe(0).f_code.co_name)

        lstS = self.lstScripts
        common.scripts = [lstS.item(r).data(QtCore.Qt.UserRole) for r in range(lstS.count())]
        self.save_config()  # save global queries

    def display_code(self):
        """Display script text of current script"""
        common.set_debug(sys._getframe(0).f_code.co_name)

        scr = max(self.lstScripts.currentRow(), 0)
        self.txtScriptEdit.blockSignals(True)

        item = self.lstScripts.item(scr)
        script = item.data(QtCore.Qt.UserRole)  # get script object
        self.txtScriptEdit.setPlainText(script.sql)  # display script text

        self.txtScriptEdit.blockSignals(False)

        self.txtScriptEdit.text_block()  # mark and get current text block
        self.set_current_result(script.name)  # select result (if any)

    def on_txtScriptEdit_cursorPositionChanged(self):
        """Cursor position changed"""

        self.txtScriptEdit.text_block()  # mark and get current text block

    def on_txtScriptEdit_textChanged(self):
        """Script text changed"""
        common.set_debug(sys._getframe(0).f_code.co_name)

        if not self.edit_timer.receivers(self.edit_timer.timeout):  # timer not connected yet
            self.edit_timer.timeout.connect(self.update_code)
        self.edit_timer.start(500)

    def update_code(self):
        """Update script text of current script"""
        self.edit_timer.stop()

        self.txtScriptEdit.text_block()  # mark and get current text block
        item = self.lstScripts.currentItem()
        if item:
            script = item.data(QtCore.Qt.UserRole)  # get script object
            script.sql = self.txtScriptEdit.toPlainText()
            self.set_header(script)
            self.update_scripts()

    def set_header(self, script):
        """Set the script text header according to the script type"""

        sql_header = '-- SQL script'
        python_header = '# Python script'

        sql = script.sql
        if script.type == 'query' and not sql.startswith(sql_header):
            if '#__script__' in sql:    # remove obselete header
                sql = '\n'.join([l for l in sql.split('\n') if '#__script__' not in l])
                script.type = 'python'  # and set type
            else:
                sql = '{}, {}: \n{}'.format(sql_header, datetime.date.today().isoformat(), sql)
                self.txtScriptEdit.setPlainText(sql)
        if script.type == 'python' and not sql.startswith(python_header):
            sql = '{}, {}: \n{}'.format(python_header, datetime.date.today().isoformat(), sql)
            self.txtScriptEdit.setPlainText(sql)

        script.sql = sql

    def on_lstScripts_currentRowChanged(self, row):
        """Display script text of current script"""
        common.set_debug(sys._getframe(0).f_code.co_name, row)

        if self.lstScripts.item(row):
            script = self.lstScripts.item(row).data(QtCore.Qt.UserRole)  # get script object
            self.set_current_result(script.name)  # select result (if any)
            self.display_code()

    def on_lstScripts_itemClicked(self, item):
        """Script clicked"""
        self.set_current_result(item.text())  # select result (if any)

    def on_lstScripts_itemChanged(self, item):
        """Script name changed"""
        common.set_debug(sys._getframe(0).f_code.co_name, item.text())

        script = item.data(QtCore.Qt.UserRole)
        script.name = item.text()

        self.update_scripts()

    def on_lstScripts_itemActivated(self, item):
        """Script return pressed"""
        common.set_debug(sys._getframe(0).f_code.co_name, item.text())

        self.run_current()  # run full script

    # ==RESULTS====================================================

    def newPasteResults(self, clip='new'):
        """New or Paste result"""

        name = ''
        if clip == 'new':  # new
            name = unique_name('new', [res.table for res in common.results])
            common.db.conn.execute('CREATE TABLE IF NOT EXISTS "{}" (Col1 DEFAULT "", Col2 DEFAULT "")'.format(name))
            common.db.conn.execute("INSERT INTO {} (Col1, Col2) VALUES ('', '')".format(name))

        else:  # paste
            for res in clip:
                if res.table in common.db.db_tables():
                    name = unique_name(res.table, [res.table for res in common.results])
                    common.db.conn.execute(
                        'CREATE TABLE IF NOT EXISTS "{}" AS SELECT * FROM "{}"'.format(name, res.table))

        self.refresh_results()  # refesh updates results (add new)
        self.set_current_result(name)

    def copyDeleteResult(self, do_delete=False, do_copy=False):
        """Copy or Delete the selected tables"""

        lstR = self.lstResults
        lstR.blockSignals(True)
        if do_copy:
            self.res_clip = []

        results = [lstR.item(r).data(QtCore.Qt.UserRole) for r in range(lstR.count())]
        sel_rowixs = [i.row() for i in lstR.selectedIndexes()]
        for r in reversed(sel_rowixs):
            if do_copy:
                self.res_clip.append(results[r])  # copy

            if do_delete:
                if not (results[r].table.startswith('#') and len(sel_rowixs)>1):
                    common.db.conn.execute('DROP TABLE IF EXISTS "{}"'.format(results[r].table))
                    lstR.setCurrentRow(min(r, lstR.count() - 1))

        lstR.blockSignals(False)
        self.refresh_results()

    def refresh_results(self):
        """Handles tableviews for each table in the database"""
        common.set_debug('{} (caller: {})'.format(sys._getframe(0).f_code.co_name, sys._getframe(1).f_code.co_name))

        lstR = self.lstResults
        lstR.blockSignals(True)  # prevent itemChanged and currentRowChanged

        rcr = max(lstR.currentRow(), 0)  # lower limit 0
        sel_rownrs = [i.row() for i in lstR.selectedIndexes()]    # save selected rows nrs

        results = frozenset([res.table for res in common.results])
        tables = frozenset(common.db.db_tables())

        # append new results
        for table in tables.difference(results):      # tables without result > append result
            common.set_debug("new table '{}'".format(table))

            # configure result and add to common.results
            result = ResultClass(table, common.db)

            result.v_context_menu = QMenu(self)
            result.v_context_menu.addAction(self.actionInsertRow)
            result.v_context_menu.addAction(self.actionDeleteRow)

            result.h_context_menu = QMenu(self)
            result.h_context_menu.addAction(self.actionRenameColumn)
            result.h_context_menu.addAction(self.actionUpdateColumn)
            result.h_context_menu.addAction(self.actionGroupBy)
            result.h_context_menu.addAction(self.actionGraphSetY)
            result.h_context_menu.addAction(self.actionGraphSetX)
            result.h_context_menu.addAction(self.actionInsertColumn)
            result.h_context_menu.addAction(self.actionDeleteColumn)
            result.h_context_menu.addAction(self.actionHide)
            result.h_context_menu.addAction(self.actionIndexColumn)

            result.view.addAction(self.actionSingleRecord)
            result.view.addAction(self.actionGraphShow)
            result.view.addAction(self.actionCopy)
            result.view.addAction(self.actionSelectionToTable)
            result.view.addAction(self.actionUnhide)
            if 'file.path' in common.db.column_names(table):
                result.view.addAction(self.actionViewExternal)

            # connect slots to each result view signal
            result.model.layoutChanged.connect(self.layout_changed)
            result.view.clicked.connect(self.layout_changed)
            result.view.selectionModel().currentChanged.connect(self.layout_changed)
            result.view.horizontalHeader().sectionClicked.connect(result.model.set_sort)  # set sort handler
            result.view.horizontalHeader().setStyleSheet(
                "QHeaderView::up-arrow { image: url(:/asc);}\nQHeaderView::down-arrow { image: url(:/desc);}")

            common.results.append(result)

        # remove old results
        for table in results.difference(tables):      # result without table > remove result
            [common.results.remove(res) for res in common.results if res.table==table]

        # add all results to list
        lstR.clear()
        for table in common.results:
            item = QListWidgetItem(table.table)  # table name
            item.setData(QtCore.Qt.UserRole, table)  # store result
            item.setFlags(item.flags() | Qt.ItemIsEditable)
            lstR.addItem(item)

        if not common.db.db_tables():   # clear view and return
            self.splitter.widget(1).setModel(None)
            return

        # highlight LOGDATA if in combined/merged mode
        if common.settings.get('mapped', False):
            self.set_result_color('LOGDATA', 'black')
        else:
            self.set_result_color('LOGDATA', 'red')

        lstR.setCurrentRow(rcr)
        [lstR.item(r).setSelected(True) for r in range(lstR.count()) if r in sel_rownrs]
        lstR.blockSignals(False)

        self.display_view()

    def layout_changed(self):
        """set_table, top/bottom row, set_data, view clicked, view current changed"""
        common.set_debug('{} (caller: {})'.format(sys._getframe(0).f_code.co_name, sys._getframe(1).f_code.co_name))

        self.frmDetails.populate()
        self.frmGraph.reload()

    def on_lstResults_currentRowChanged(self, row):
        """Displays the view of the selected result (table)"""
        common.set_debug(sys._getframe(0).f_code.co_name, row)

        self.display_view()

    def on_lstResults_itemClicked(self, item):
        """Display result information"""
        common.set_debug(sys._getframe(0).f_code.co_name)

        view = self.current(VIEW)
        view.resizeColumnsToContents()                      # resize columns width
        view.horizontalHeader().setStretchLastSection(True)

        self.display_statistics()

    def display_view(self):
        """Display the view of the current result"""

        table, result, model, view = self.current(TABLE, RESULT, MODEL, VIEW)
        if not table:
            return

        # display view
        model.set_table(table)      # force reread table fields and rows
        self.frmGraph.set_graph()   # set graph (if visible)

        self.splitter.insertWidget(1, view)     # insert current view (at pos 1)
        self.splitter.widget(2).hide()          # hide old view (moved to pos 2)
        view.show()                             # and show it
        self.splitter.refresh()                 # refresh prevents flickering

        # update GUI with result specific settings
        self.ledDisplayFilter.setText(model.filter_gui_text)

        # set highlight texts
        lstH = self.lstHighlights
        result.highlights = {lstH.item(r).text(): lstH.item(r).background() for r in range(lstH.count())}

        self.display_statistics()

    def display_statistics(self):
        """update visible information about the table"""

        table, model = self.current(TABLE, MODEL)
        if not table:
            return

        table_count = common.db.row_count(table)
        view_count = '{} of '.format(model.rowCnt) if model.rowCnt < table_count else ''
        where_clause = 'WHERE {} '.format(model.filter_gui_text) if model.filter_gui_text else ''
        order_clause = 'ORDER BY {}'.format(model.sorted_section['name']) if model.sorted_section else ''
        self.statusBar.showMessage("{}{} FROM '{}' {}{}".format(view_count, table_count, table, where_clause, order_clause))

    def on_lstResults_itemChanged(self, item):
        common.set_debug('{} (caller: {})'.format(sys._getframe(0).f_code.co_name, sys._getframe(1).f_code.co_name))
        """Table name changed"""

        org_name = item.data(QtCore.Qt.UserRole).table          # original name
        org_res = list_get(common.results, 'table', org_name)   # original result

        new_name = item.text()
        if new_name != org_name:    # name has changed
            if new_name.lower() in [tbl.lower() for tbl in common.db.db_tables()]:
                self.lstResults.blockSignals(True)
                item.setText(org_name)  # restore original table name without signalling
                self.lstResults.blockSignals(False)
                common.set_error("Table '{}' already exists".format(new_name))
            else:
                sql = "ALTER TABLE '{}' RENAME TO '{}'".format(org_name, new_name)
                common.db.conn.execute(sql)  # rename table
                common.results.remove(org_res)
                self.lstResults.setFocus()
                self.refresh_results()  # refresh updates result names (remove original, add new)

    def set_current_result(self, name):
        """set current row if the result contains 'name'"""
        lstR = self.lstResults

        lstR.clearSelection()  # set result (if any)
        for i in range(lstR.count()):
            if lstR.item(i).text() == name:
                lstR.setCurrentRow(i)

    def set_result_color(self, name, color):
        """set table 'name' to color 'color'"""
        lstR = self.lstResults

        for i in range(lstR.count()):
            if name in lstR.item(i).text(): lstR.item(i).setForeground(QtGui.QColor(color))

    @pyqtSlot(bool)
    def on_actionCombineTables_triggered(self):
        """combine selected tables to single table"""

        sel_tables = [item.text() for item in self.lstResults.selectedItems()]

        union_cols = collections.OrderedDict()  # create dict with all unique col names
        for table in sel_tables:
            [union_cols.update({col: 0}) for col in common.db.column_names(table)]

        selects = []
        for table in sel_tables:
            table_cols = common.db.column_names(table)
            cols = ['"{}"'.format(col) if col in table_cols else "'' AS '{}'".format(col) for col in union_cols]
            selects.append('SELECT {} FROM "{}"'.format(','.join(cols), table))
        name_union = unique_name('combined', [res.table for res in common.results])
        sql = 'CREATE TABLE IF NOT EXISTS "{}" AS {}'.format(name_union, '\nUNION ALL\n'.join(selects))
        common.db.conn.execute(sql)
        common.set_info('Combined {} tables'.format(len(sel_tables)))

        self.refresh_results()  # refesh updates results

    @pyqtSlot(bool)
    def on_actionMergeTables_triggered(self):
        """merge selected tables to single table"""

        sel_tables = [item.text() for item in self.lstResults.selectedItems()]

        max_cols = 0  # maximum numbers of columns in any table
        for table in sel_tables:
            max_cols = max(max_cols, len(common.db.column_names(table)))

        selects = []
        for table in sel_tables:
            table_cols = common.db.column_names(table)
            cols = ['"{}" AS Col{}'.format(table_cols[c], c) if c < len(table_cols) else "'' AS Col{}".format(c) for c
                    in range(max_cols)]
            selects.append('SELECT {} FROM "{}"'.format(','.join(cols), table))
        name_union = unique_name('merged', [res.table for res in common.results])
        sql = 'CREATE TABLE IF NOT EXISTS "{}" AS {}'.format(name_union, '\nUNION ALL\n'.join(selects))
        common.db.conn.execute(sql)
        common.set_info('Merged {} tables'.format(len(sel_tables)))

        self.refresh_results()  # refesh updates results

    @pyqtSlot(bool)
    def on_actionGraphSetX_triggered(self):
        """define column as graph x-axis"""
        model = self.current(MODEL)

        col_name = self.current(COLNAM)
        model.xcol = '' if col_name in model.xcol else col_name

        self.frmGraph.set_graph()

    @pyqtSlot(bool)
    def on_actionGraphSetY_triggered(self):
        """define column as graph y-axis"""
        model = self.current(MODEL)

        col_name = self.current(COLNAM)
        if col_name in model.ycols:
            model.ycols.remove(col_name)
        elif len(model.ycols) < 4:
            model.ycols.append(col_name)
        self.frmGraph.set_graph()

    @pyqtSlot(bool)
    def on_actionGraphShow_triggered(self):

        self.frmGraph.show()
        self.frmGraph.set_graph()

    @pyqtSlot(bool)
    def on_actionGroupBy_triggered(self):
        """group by selected column"""

        table, view, model = self.current(TABLE,VIEW,MODEL)

        section = view.currentIndex().column() if view.currentIndex().isValid() else 0
        grp_col = model.col_list[section]

        sql = 'select count(*) AS NOF_{0}, * from "{1}" group by "{0}"'.format(grp_col, table)
        table = '{}[{}]'.format(table, grp_col)
        common.db.query(sql, table)
        self.refresh_results()
        self.set_current_result(table)

    @pyqtSlot(bool)
    def hide_columns(self):
        """hide the selected columns"""
        print('hide')
        view, model = self.current(VIEW, MODEL)

        first = -1
        row = view.currentIndex().row()
        for col in range(model.columnCount()):
            if view.selectionModel().isSelected(model.index(row, col)):
                view.setColumnHidden(col, True)
                first = col

        view.setCurrentIndex(model.index(row, first+1))

    @pyqtSlot(bool)
    def unhide_columns(self):
        """unhide all columns"""

        view, model = self.current(VIEW,MODEL)

        for col in range(model.columnCount()):
            view.setColumnHidden(col, False)

    def copyCells(self):
        """Copy the selected cells"""

        result, model = self.current(RESULT, MODEL)
        clip = QApplication.clipboard()

        first, last, col_idxs = result.selectedBlock()
        common.set_info('Copying {} rows, {} columns...'.format(last-first+1, len(col_idxs)))

        rows = model.get_block(col_idxs, first, last)

        head = ['\t'.join(rows[0].keys())]
        lines = ['\t'.join('"{}"'.format(r) for r in row) for row in rows]
        clip.setText('\n'.join(head + lines))

        common.set_info('Copied {} rows, {} columns'.format(last-first+1, len(col_idxs)))

    @pyqtSlot(bool)
    def on_actionSelectionToTable_triggered(self):
        """Copy selection to table"""

        result, model = self.current(RESULT, MODEL)

        first, last, col_idxs = result.selectedBlock()
        common.set_info('Copying {} rows, {} columns...'.format(last - first + 1, len(col_idxs)))

        sql = model.get_block_sql(col_idxs, first, last)
        common.db.conn.execute('drop table if exists "_SELECTION_"')
        common.db.conn.execute('create table "_SELECTION_" as {}'.format(sql))

        if '_SELECTION_' in common.db.db_tables():
            name_new = unique_name('selection', [res.table for res in common.results])
            sql = "ALTER TABLE '_SELECTION_' RENAME TO '{}'".format(name_new)
            common.db.conn.execute(sql)  # rename table
            self.refresh_results()
            common.set_info("Copied {} rows, {} columns to '{}''".format(last - first + 1, len(col_idxs), name_new))
        else:
            common.set_info('Could not copy selection to table' )

    @pyqtSlot(bool)
    def on_actionViewExternal_triggered(self):
        """View file content in external program"""

        table, view, model = self.current(TABLE, VIEW, MODEL)

        col_names = common.db.column_names(table)
        if 'file.path' in col_names:    # column 'file.path' exists
            col_path = col_names.index('file.path')

            sel_rowixs = [i.row() for i in view.selectedIndexes() if i.column() == view.currentIndex().column()]  # get row numbers of selection
            cmd = [self.ledExternalCmd.text().strip()]                                      # command to run
            paths = set([model.data(model.index(r, col_path), role=Qt.EditRole) for r in sel_rowixs])            # get all unique paths (set uniquifies)
            cmd.extend(paths)
            try:
                subprocess.Popen(cmd)   # run external program with paths as argument
            except:
                common.set_error("Could not open external program '{}'".format(cmd))
        else:
            common.set_error("No info field 'file.path' (See tab Settings, 'Include info fields')")

    @pyqtSlot(bool)
    def on_actionImport_triggered(self):
        """Import"""

        if common.busy:
            return
        common.busy = True  # import

        dialog = QFileDialog(self)
        dialog.setFileMode(QFileDialog.ExistingFile)
        dialog.setNameFilters(["CSV files (*.csv)", "Text files (*.txt *.dat *.log)", "All files (*)"])

        if dialog.exec():
            csv_filename = dialog.selectedFiles()[0]
            cfg = common.settings
            delim = str(cfg['delimiter'])
            quote = str(cfg['quotecharacter'])
            term = str(cfg['lineterminator'])
            head = bool(cfg['header'])
            common.db.importCSV(csv_filename, '', delim, quote, term, head)

            self.update_settings()
            self.refresh_results()

        common.busy = False

    @pyqtSlot(bool)
    def on_actionExport_triggered(self):
        """Export"""

        if common.busy:
            return
        common.busy = True  # export

        lstR = self.lstResults
        table = lstR.currentItem().text()

        dialog = QFileDialog(self)
        dialog.setAcceptMode(QFileDialog.AcceptSave)
        dialog.setDefaultSuffix('cfg')
        dialog.setNameFilters(["CSV files (*.csv)", "Text files (*.txt *.dat *.log)", "All files (*)"])

        if dialog.exec():
            csv_filename = dialog.selectedFiles()[0]
            cfg = common.settings
            delim = str(cfg['delimiter'])
            quote = str(cfg['quotecharacter'])
            term = str(cfg['lineterminator'])
            head = bool(cfg['header'])
            common.db.exportCSV(table, csv_filename, delim, quote, term, head)

            self.update_settings()

        common.busy = False

    @pyqtSlot(bool)
    def on_actionDatabase_Objects_triggered(self):
        """Show Database Objects"""

        result_name = 'Database Objects'
        sql = 'DROP TABLE IF EXISTS "{}"'.format(result_name)
        common.db.query(sql, result_name, True)     # drop result table if exists
        rows = common.db.select('sqlite_master')    # select rows
        common.db.insert(result_name, rows)         # insert into result table

        self.refresh_results()
        self.set_current_result(result_name)

    @pyqtSlot(bool)
    def on_actionIndexColumn_triggered(self):
        """Index Field"""

        if common.busy:
            return
        common.busy = True

        table, col_name = self.current(TABLE,COLNAM)

        common.db.create_field_index(table, col_name)
        self.refresh_results()

        common.set_info("Index created on field '{}'".format(col_name))
        common.busy = False

    @pyqtSlot(bool)
    def on_actionIndexTable_triggered(self):
        """Index Table"""

        if common.busy:
            return
        common.busy = True

        table = self.current(TABLE)
        common.db.create_table_indexes(table)
        self.refresh_results()

        common.set_info("Indexes created on table '{}'".format(table))
        common.busy = False

    @pyqtSlot(bool)
    def on_actionIndexDatabase_triggered(self):
        """Index Database"""

        if common.busy:
            return
        common.busy = True

        tables = common.db.db_tables()
        for table in tables:
            common.db.create_table_indexes(table)
            common.set_info("Indexes created on table '{}'".format(table))

        common.set_info("Indexes created on all tables")
        common.busy = False

    @pyqtSlot(bool)
    def on_actionRenameColumn_triggered(self):
        """rename column"""

        result, view, model, colnam = self.current(RESULT, VIEW, MODEL, COLNAM)
        header = view.horizontalHeader()
        first, last, col_idxs = result.selectedBlock()
        if  len(col_idxs) != 1:
            return

        all_cols = [header.logicalIndex(c) for c in range(model.columnCount())]      # all columns (logical)
        col = col_idxs[0]      # index of column name to change
        res = QInputDialog.getText(self, app_name, 'Change column name <b>{}</b> to...'.format(colnam), QLineEdit.Normal, model.col_list[col], Qt.WindowTitleHint | Qt.WindowCloseButtonHint)
        if res[1]:
            all_cols = [model.col_list[c] for c in all_cols]                # all column names
            all_cols[col] = '"{}" AS \'{}\''.format(all_cols[col], res[0])  # change column name

            model.modify_table(all_cols)
            self.refresh_results()

    @pyqtSlot(bool)
    def on_actionDeleteColumn_triggered(self):
        """delete columns(s)"""

        view, model = self.current(VIEW, MODEL)
        header = view.horizontalHeader()

        all_cols = [header.logicalIndex(c) for c in range(model.columnCount())]      # all columns (logical)
        sel_cols = [idx.column() for idx in view.selectionModel().selectedColumns()] # selected columns (logical)
        [header.moveSection(header.visualIndex(i), i) for i in all_cols]             # restore column order

        if sel_cols:
            del_idx = all_cols.index(sel_cols[0])
            for col in sel_cols:                                        # remove column name from list
                all_cols.remove(col)

            model.modify_table(all_cols)
            self.refresh_results()
            view.selectColumn(del_idx)

    @pyqtSlot(bool)
    def on_actionInsertColumn_triggered(self):
        """insert column(s)"""

        view, model = self.current(VIEW, MODEL)
        header = view.horizontalHeader()

        all_cols = [header.logicalIndex(c) for c in range(model.columnCount())]      # all columns (logical)
        sel_cols = [idx.column() for idx in view.selectionModel().selectedColumns()] # selected columns (logical)
        [header.moveSection(header.visualIndex(i), i) for i in all_cols]             # restore column order

        if sel_cols:
            names = model.col_list + ['Col']                # list of column names (to create unique name)
            ins_idx = all_cols.index(sel_cols[0])
            for col in sel_cols:
                name_col = unique_name('Col', names)        # get unique column name
                names.append(name_col)                      # and update the list

                all_cols.insert(ins_idx, "'' AS {}".format(name_col))   # insert column name as alias
                ins_idx += 1

            model.modify_table(all_cols)
            self.refresh_results()
            view.selectColumn(ins_idx)

    @pyqtSlot(bool)
    def on_actionDeleteRow_triggered(self):
        """delete row(s)"""

        result, view, model = self.current(RESULT, VIEW, MODEL)
        first, last, col_idxs = result.selectedBlock()

        all_cols = [c for c in range(model.columnCount())]     # all columns (logical)

        if first > -1:
            model.modify_table(all_cols, first, -(last - first + 1))
            self.refresh_results()
            view.selectRow(first)

    @pyqtSlot(bool)
    def on_actionInsertRow_triggered(self):
        """insert row(s)"""

        result, view, model = self.current(RESULT, VIEW, MODEL)
        first, last, col_idxs = result.selectedBlock()

        all_cols = [c for c in range(model.columnCount())]     # all columns (logical)

        if first > -1:
            model.modify_table(all_cols, first - 1, last - first + 1)
            self.refresh_results()
            view.selectRow(first)

    @pyqtSlot(bool)
    def on_actionUpdateColumn_triggered(self):
        """update column value"""

        table, colnam = self.current(TABLE, COLNAM)
        if  colnam:
            self.d = dialogClass(self, table, colnam)

    # ==TOOLS====================================================

    @pyqtSlot(bool)
    def on_btnScreenshot_clicked(self):
        screen = QGuiApplication.primaryScreen()
        pm = screen.grabWindow(self.splitter.widget(1).winId())
        QApplication.clipboard().setPixmap(pm)
        common.set_info('Screenshot (table) copied to clipboard')

    def on_ledDisplayFilter_returnPressed(self):
        """set display filter (WHERE)"""

        model = self.current(MODEL)
        col_nams = ['"{}"'.format(c) for c in common.db.column_names(model.table)]

        txt = self.ledDisplayFilter.text()
        model.filter_gui_text = txt

        where = []
        parts = re.split('(?i)( AND | OR )', txt)
        for part in parts:
            matches = list(re.finditer(r"""(\w+)|(["'])(.*?)\2""", part))    # find literals (words or quoted strings)
            literals = [m.group(1) or m.group(3) for m in matches]
            expand = len(literals) == 1 and not re.fullmatch('(?i)( AND | OR )', part)
            where.append("{} like '%{}%'".format("||','||".join(col_nams), literals[0]) if expand else part)

        model.filter_text = ''.join(where)
        self.refresh_results()

    def on_ledDisplayFilter_textEdited(self):
        """clear display filter"""

        if not self.ledDisplayFilter.text().strip():
            model = self.current(MODEL)

            model.filter_gui_text = ''
            model.filter_text = ''
            self.refresh_results()

    def on_ledSearchText_textChanged(self, text):
        """Set result search text"""

        res = self.current(RESULT)  # current result
        res.search_txt = text       # set search text
        res.view.reset()            # update view

    @pyqtSlot(bool)
    def on_actionFindPrevious_triggered(self):
        """Search for text"""
        self.search('prev')

    @pyqtSlot(bool)
    def on_actionFindNext_triggered(self):
        """Search for text"""
        self.search('next')

    def on_ledSearchText_returnPressed(self):
        """Add text to highlightst"""

        self.search('next')

    def on_lstHighlights_itemChanged(self, item):
        """Update highlights"""
        self.search('store')

    def on_lstHighlights_itemClicked(self, item):
        """Update search text"""
        self.ledSearchText.setText(item.text())

    def search(self, fun):

        common.set_debug(sys._getframe(0).f_code.co_name, fun)

        colorList = [0x7FFFD4, 0xDEB887, 0x8FBC8F, 0x00BFFF, 0xFFD700, 0xFFB6C1, 0x66CDAA, 0x00FA9A, 0xFFA500, 0xCD853F,
                     0xBC8F8F, 0x87CEEB, 0xFF6347, 0xDAA520, 0xADFF2F, 0xF0E68C, 0xF08080, 0x90EE90, 0xF4A460, 0x9ACD32,
                     0xBDB76B, 0xFF8C00, 0xB0C4DE, 0x32CD32]
        text = self.ledSearchText.text()

        if fun in 'next|prev|new':
            if not text:
                if self.ledSearchText.hasFocus():
                    common.set_error('No text entered')
                else:
                    self.ledSearchText.setFocus()
                return

        lstR = self.lstResults
        item = lstR.currentItem()
        if not item:
            common.set_error('No result table selected')
            return

        res = list_get(common.results, 'table', item.text())
        res.exit = False
        lstH = self.lstHighlights

        if fun == 'next':
            res.find_next(text)
        elif fun == 'prev':
            res.find_prev(text)
        elif fun == 'new' and lstH.count() < len(colorList):
            item = QListWidgetItem(text)
            item.setFlags(item.flags() | Qt.ItemIsEditable)
            lstH.addItem(item)
        elif fun == 'delete':
            for item in lstH.selectedItems():
                lstH.takeItem(lstH.row(item))
                del item

        if fun in 'new|delete|store':
            res.highlights = {lstH.item(r).text(): lstH.item(r).background() for r in range(lstH.count())}
            # res.view.scroll(1, 0);
            # res.view.scroll(-1, 0)  # force repaint
            res.view.reset()

            for r in range(lstH.count()):
                color = colorList[r]
                lstH.item(r).setBackground(QtGui.QColor(color))

    # ==MISC====================================================

    @pyqtSlot(bool)
    def on_actionTableName_triggered(self):
        """Paste table name"""

        table = self.current(TABLE)
        QApplication.clipboard().setText(table)
        try:
            QApplication.focusWidget().paste()
        except:
            common.set_info('Cannot paste table name')

    @pyqtSlot(bool)
    def on_actionColumnName_triggered(self):
        """Paste column name"""

        col_name = self.current(COLNAM)
        QApplication.clipboard().setText(col_name)
        try:
            QApplication.focusWidget().paste()
        except:
            common.set_info('Cannot paste column name')

    @pyqtSlot(bool)
    def on_actionPasteCellValue_triggered(self):
        """Paste cell value"""

        val = self.current(VALUE)
        QApplication.clipboard().setText(val)
        try:
            print(QApplication.focusWidget())
            QApplication.focusWidget().paste()
        except:
            common.set_info('Cannot paste cell value')

    def msg_box(self, text):
        """Show information 'text' in message box"""
        QMessageBox.information(self, app_name, str(text) + ' ' * 10)

    def display_info(self, text):
        """Display info 'text' in statusbar"""
        self.statusBar.showMessage(text)

    def display_error(self, text):
        """Show warning 'text' message box"""
        text = text.replace('\n', '<BR>')
        QMessageBox.warning(QMessageBox(), app_name, text + ' ' * 10)

    def current(self, *specs):
        common.set_debug('{} (caller: {})'.format(sys._getframe(0).f_code.co_name, sys._getframe(1).f_code.co_name))

        values = []

        lstR = self.lstResults
        table = lstR.currentItem().text() if lstR.currentItem() else ''  # table
        result = list_get(common.results, 'table', table)   # result
        view = result.view if result else None  # view
        index = view.currentIndex() if view else None  # index
        rowidx = index.row() if index and index.isValid() else -1  # rowidx
        colidx = index.column() if index and index.isValid() else -1  # colidx
        model = result.model if result else None  # model
        colnam = model.headerData(colidx, Qt.Horizontal, Qt.DisplayRole) if colidx > -1 else ''  # colnam (colidx, horizontal, displayrole)
        value = model.data(index) if model else ''  # value

        for spec in specs:
            if spec == 'table':
                values.append(table)
            elif spec == 'result':
                values.append(result)
            elif spec == 'view':
                values.append(view)
            elif spec == 'index':
                values.append(index)
            elif spec == 'rowidx':
                values.append(rowidx)
            elif spec == 'colidx':
                values.append(colidx)
            elif spec == 'model':
                values.append(model)
            elif spec == 'colnam':
                values.append(colnam)
            elif spec == 'value':
                values.append(value)

        return values if len(values) != 1 else values[0]

    def save_config(self, force=False):
        common.set_debug('{} (caller: {})'.format(sys._getframe(0).f_code.co_name, sys._getframe(1).f_code.co_name))

        if force or common.current_config_file == 'configs/Default.cfg':
            save_config_to(common.current_config_file)
            self.dirty = ''
        else:
            save_config_to('configs/Default.cfg')   # always save default config
            self.dirty = '*'

        conf_name = os.path.basename(os.path.splitext(common.current_config_file)[0])
        self.setWindowTitle('{}{} - {}'.format(self.dirty, app_name, conf_name))
        if force:
            common.set_info("Config saved: '{}'".format(common.current_config_file))

        [msg.do_compile() for msg in common.messages]  # recompile messages after saving

    def load_config(self, config_file, full=True):

        load_config_from(config_file, full)

        if full:
            self.dirty = ''
            conf_name = os.path.basename(os.path.splitext(config_file)[0])
            self.setWindowTitle('{}{} - {}'.format(self.dirty, app_name, conf_name))

            self.display_settings()
            self.display_locations()

        else:
            self.dirty = '*'
            if not self.windowTitle().startswith('*'):
                self.setWindowTitle('*' + str(self.windowTitle()))

        self.show()

        self.display_messages()
        self.display_scripts()
        self.refresh_results()

    def closeEvent(self, event):
        common.set_info('Application closing')
        common.cancel = True  # cancel!

        if self.dirty:
            buttons = QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel
            txt = "Save '{}'?".format(common.current_config_file)
            answer = QMessageBox.question(self, app_name, txt, buttons)
            if answer == QMessageBox.Yes:
                self.save_config(common.current_config_file)  # save
            elif answer == QMessageBox.Cancel:
                event.ignore()
                return

        if os.path.exists(common.LOCK_FILE): # remove lock file
            os.remove(common.LOCK_FILE)

        self.frmDetails.deleteLater()  # destroy on exit
        self.frmHelp.deleteLater()  # destroy on exit
        self.frmGraph.deleteLater()  # destroy on exit
        sys.exit()

class FrmHelpClass(QDialog, Ui_Dialog):
    def __init__(self, parent):
        QDialog.__init__(self, parent)
        self.setupUi(self)

        help_file = r'manual.html'
        if os.path.isfile(help_file):  # help file exist
            with open(help_file, "r", encoding='utf-8') as f:   # RTFM
                self.textBrowser.setHtml(f.read())
                self.textBrowser.zoomIn(2)

class frmDetailsClass(QDialog, Ui_Details):
    def __init__(self, parent):
        QDialog.__init__(self, parent, Qt.WindowTitleHint | Qt.WindowCloseButtonHint)
        self.setupUi(self)
        self.parent = parent
        self.conv_funs = {'html':to_html, 's2h':s2h, 'h2s':h2s, 'none':str}

    def populate(self):
        common.set_debug('{} (caller: {})'.format(sys._getframe(0).f_code.co_name, sys._getframe(1).f_code.co_name))

        table, view, model = self.parent.current(TABLE, VIEW, MODEL)
        if not view or not view.currentIndex().isValid() or not self.isVisible():
            return          # no need /not possible to populate details

        self.tblDetails.setRowCount(model.columnCount())        # set nof rows
        self.tblDetails.setColumnCount(1)                       # set nof columns

        row = view.currentIndex().row()         # current row
        self.setWindowTitle("[{}] - Row {} ({})".format(table, str(row + 1), model.rowCnt))

        for col in range(model.columnCount()):                                  # for each view column
            col_name = model.headerData(col, Qt.Horizontal, Qt.DisplayRole)             # get column name
            item = QTableWidgetItem(col_name)
            self.tblDetails.setVerticalHeaderItem(col, item)                            # set name

            item = QTableWidgetItem(model.data(model.index(row, col), Qt.DisplayRole))
            self.tblDetails.setItem(col, 0, item)                                       # set value

        col = view.currentIndex().column()      # current column
        self.tblDetails.setCurrentCell(col, 0)  # set current cell

        self.refresh_cell()

    @pyqtSlot()
    def on_btnHtml_clicked(self):
        self.refresh_cell('html')

    @pyqtSlot()
    def on_btnS2h_clicked(self):
        self.refresh_cell('s2h')

    @pyqtSlot()
    def on_btnH2s_clicked(self):
        self.refresh_cell('h2s')

    @pyqtSlot()
    def on_btnNone_clicked(self):
        self.refresh_cell('none')

    def on_tblDetails_currentCellChanged(self): # current cell changed -> refresh cell content
        self.refresh_cell()

    def refresh_cell(self, conv=''):
        model, row = self.parent.current(MODEL, ROWIDX)

        col = max(0, self.tblDetails.currentRow())                      # column (current row of details view)

        if conv:                            # set conversion for current column
            model.cell_conversion[col] = self.conv_funs[conv]

        txt = model.data(model.index(row, col), role=Qt.EditRole)       # EditRole gives not-truncated value

        if col in model.cell_conversion:    # conversion defined for current column
            txt = model.cell_conversion[col](txt)
        self.txtSource.setPlainText(txt)

class frmGraphClass(QMainWindow, Ui_widGraph):
    screen = QGuiApplication.primaryScreen()

    zoom = 0
    model = None    # model assigned to graph

    def __init__(self, parent):
        global plots    # qcp requirement!
        QMainWindow.__init__(self, parent)
        self.setupUi(self)

        self.parent = parent
        plots = []
        self.group = QCPBarsGroup(self.qcp)
        self.series_colors = (QColor('blue'), QColor('red'), QColor('green'), QColor('purper'))
        self.range_colors = (QColor('red'), QColor('blue'), QColor('black'))
        self.data = []

        self.qcp.setInteraction(QCP.iRangeDrag)
        self.qcp.axisRect().setRangeDrag(Qt.Vertical)
        self.qcp.setNoAntialiasingOnDrag(True)
        self.qcp.xAxis.setTickLabelRotation(50)
        self.qcp.legend.setVisible(True)

        #connect signals
        self.qcp.mouseWheel.connect(self.hnd_mouse)
        self.qcp.mouseMove.connect(self.show_tooltip)
        for obj in self.findChildren((QPushButton, QCheckBox, QRadioButton, QTextEdit)):
            if type(obj) == QPushButton: obj.clicked.connect(self.update_state)
            if type(obj) == QCheckBox: obj.clicked.connect(self.update_state)
            if type(obj) == QRadioButton: obj.clicked.connect(self.update_state)
            if type(obj) == QTextEdit: obj.textChanged.connect(self.update_state)

        # add line item
        self.line = line = QCPItemLine(self.qcp)
        self.qcp.addItem(line)
        line.setPen(QPen(Qt.DashLine))
        line.start.setTypeX(QCPItemPosition.ptPlotCoords)
        line.start.setTypeY(QCPItemPosition.ptAbsolute)
        line.end.setTypeX(QCPItemPosition.ptPlotCoords)
        line.end.setTypeY(QCPItemPosition.ptAbsolute)
        line.setClipToAxisRect(False)
        self.installEventFilter(self)   # capture left/right key as alternative for mousewheel

    def eventFilter(self, target, event):
        if event.type() == QEvent.KeyRelease:
            if event.key() == Qt.Key_Left:
                self.reload(-1)
            elif event.key() == Qt.Key_Right:
                self.reload(1)

        return False    # don't consume event

    def hnd_mouse(self, event):
        direction = 1 if event.angleDelta().y() > 0 else -1    # direction plus or minus
        # origin = (event.x()- self.qcp.axisRect().left()) / self.qcp.axisRect().width()  # relative x position
        self.reload(direction)                                 # shift/zoom direction

    def update_state(self):
        obj = self.sender()
        name = obj.objectName()

        if type(obj) == QTextEdit:
            self.result.states[name] = obj.toPlainText()
            obj.setFontWeight(QFont.Bold)
            # title.setFontPointSize(10)
            obj.resize(obj.document().size().width(), obj.document().size().height())

        if name == 'chkStacked' and obj.isChecked():
            self.chkGrouped.setChecked(False)
        elif name == 'chkGrouped' and obj.isChecked():
            self.chkStacked.setChecked(False)

        for chk in self.findChildren((QPushButton, QCheckBox, QRadioButton)):
            self.result.states[chk.objectName()] = chk.isChecked()

        if type(obj) in (QPushButton, QCheckBox, QRadioButton):
            self.set_graph()

    @pyqtSlot(bool)
    def on_btnAll_clicked(self):
        self.model.r_beg = 0
        self.model.r_end = self.model.rowCnt
        self.reload()

    @pyqtSlot(bool)
    def on_btnView_clicked(self):

        self.model.r_beg = self.model.top_row
        self.model.r_end = self.model.bottom_row
        self.reload()

    @pyqtSlot(bool)
    def on_btnScreenshot_clicked(self):

        screen = QGuiApplication.primaryScreen()
        pm = screen.grabWindow(self.qcp.winId())
        QApplication.clipboard().setPixmap(pm)
        common.set_info('Screenshot (graph) copied to clipboard')

    def show_tooltip(self, event):
        pass
        # x_coord = self.qcp.xAxis.pixelToCoord(event.pos().x())
        # y_coord = self.qcp.yAxis.pixelToCoord(event.pos().y())
        # self.qcp.setToolTip(str(y_coord))
        # print('tooltip', x_coord, '-', y_coord)

    def resizeEvent(self, event=None):
        title = self.txtTitle
        title.move(self.qcp.width()/2-title.width()/2, 28)

    def set_graph(self):
        global plots    # customplot needs plottables to be global

        if not self.isVisible():
            return

        table, result, model = self.parent.current(TABLE, RESULT, MODEL)
        if not table:
            self.setWindowTitle('[No table]')
            return

        # set instance attributes
        self.result = result
        self.model = model

        # init states
        if not result.states:
            for obj in self.findChildren((QPushButton, QCheckBox, QRadioButton)):
                result.states[obj.objectName()] = False
            result.states['radBarGraphs'] = True
            result.states['chkAutoScaleY'] = True

        # restore states
        for obj in self.findChildren((QPushButton, QCheckBox, QRadioButton)):
            obj.setChecked(result.states[obj.objectName()])

        self.txtTitle.setText(result.states.get('txtTitle', '<title>'))
        self.txtTitle.setVisible(self.chkTitle.isChecked())
        self.txtTitle.setFontWeight(QFont.Bold)
        self.txtTitle.setAlignment(Qt.AlignHCenter)
        if not result.states['txtTitle']:
            self.txtTitle.setText(table)
        self.resizeEvent()

        # auto find date column
        if not model.xcol:
            for c in range(model.columnCount()):
                is_date = []
                for r in range(min(model.rowCnt, 10)):
                    idx = model.index(r, c)                 # create model index
                    typ = type(evalx(model.data(idx)))      # get type of column value
                    is_date.append(typ == datetime.datetime)
                if all(is_date):    # first values of column are all dates
                    model.xcol = model.col_list[c]
                    break

        self.qcp.xAxis.setLabel(model.xcol if model.xcol else '<Base Column>')     # set x-axis label to column name

        # remove all plottables (graphs and bars)
        for p in plots[:]:
            self.qcp.removePlottable(p)
            plots.remove(p)
        prev = None

        # init plottables
        series = model.ycols if model.ycols else ['<Data Column>']
        for ycol, clr in zip(series, self.series_colors):

            if self.radLineGraphs.isChecked():    # graph
                plot = self.qcp.addGraph(self.qcp.xAxis, self.qcp.yAxis)
            else:                               # bars
                plot = QCPBars(self.qcp.xAxis, self.qcp.yAxis)
                self.qcp.addPlottable(plot)
                plot.setWidthType(QCPBars.wtAxisRectRatio)   # width is relative to axis
                plot.setWidth(.01)                           # allow 100 bars
                if self.chkGrouped.isChecked():
                    plot.setBarsGroup(self.group)               # grouped
                if self.chkStacked.isChecked() and prev:
                    plot.moveAbove(prev)                        # stacked

            plot.setName(ycol)      # set legend to column name
            plot.setPen(clr)
            plots.append(plot)
            prev = plot             # save previous plot (used when stacking)

        self.reload()   # get data

    def reload(self, direction=0, center=0.5):
        global plots    # customplot needs plottables to be global

        if not self.isVisible():
            return

        common.set_debug('reload(), direction= ', direction)

        MAX_ROWS = 80
        MAX_TICKS = 30

        model = self.model
        if not model or model.rowCnt == 0:
            self.setWindowTitle('[Empty]')
            return

        # helper function clip(min, val, max) returns val between min and max (inclusive)
        clip = lambda min, val, max: round(sorted((min, val, max))[1])  #

        # helper function xrange yields count evenly distributed integers from start to stop
        def xrange(start, stop, count):
            range = stop - start
            step = 1 if range < count else range / count
            while round(start) < stop:
                yield round(start)
                start += step

        # set col_list (xcol, ycol1, ycol2,..)
        if model.xcol in model.col_list:
            col_list = [model.col_list.index(model.xcol)]
        else:
            col_list = [-1]

        for ycol in model.ycols:
            col_list.append(model.col_list.index(ycol))
        if not model.ycols:
            col_list.append(-1)

        # calculate new row range
        r_beg = model.r_beg
        r_end = model.r_end
        if direction == 0:             # sync x-axis with table view
            r_mid = (model.top_row + model.bottom_row) / 2
            r_range = r_end - r_beg
            r_beg = r_mid - r_range / 2
            r_end = r_mid + r_range / 2
        else:                       # shift or zoom x-axis
            r_range = r_end - r_beg

            factor = 0.2 if QApplication.keyboardModifiers() & Qt.ShiftModifier else 0.04   # shift: fast zoom/shift
            if QApplication.keyboardModifiers() & Qt.ControlModifier:   # zoom
                r_zoom = r_range/2 + direction * max(1, factor * r_range / 2)
                r_shift = 0
            else:   # shift
                r_zoom = r_range / 2
                r_shift = direction * max(1, factor * r_range)

            r_mid = (r_beg + r_end) / 2               # calculate row as center for zooming
            r_beg = r_mid + r_shift - max(2, r_zoom)  # calculate begin row after shift/zoom
            r_end = r_mid + r_shift + max(2, r_zoom)  # calculate end row after shift/zoom

        # calculate clipped row range
        row_cnt = model.rowCnt
        upper = clip(0, row_cnt - r_end + r_beg, row_cnt)       # max allowed beg row (total - range), clipped between 0 and total
        lower = clip(0, r_end - r_beg, row_cnt)                 # min allowed end row (range), clipped between 0 and total
        model.r_beg = r_beg = clip(0, (r_beg), upper)           # new beg row, clipped between 0 and upper
        model.r_end = r_end = clip(lower, (r_end), row_cnt)     # new end row, clipped between lower and total

        # get data (rownr, type, xcol, ycol1, ycol2,..) for r_beg to r_end
        drows = []
        for r in xrange(r_beg, r_end, MAX_ROWS):
            drow = []

            # xcol value
            if col_list[0] > -1:
                idx = model.index(r, col_list[0])   # create model index
                val = evalx(model.data(idx))        # get xcol value
            else:
                val = r + 1

            vtype = type(val)                   # get type
            if self.chkXNumeric.isChecked() and (vtype == type(None) or vtype == str):   # skip non numeric xcol values (in numeric mode)
                continue

            drow.append(r)                      # 0: row nr
            drow.append(vtype)                  # 1: type
            drow.append(val)                    # 2: field value

            # ycol values
            for c in col_list[1:]:
                if c > -1:
                    idx = model.index(r, c)     # create model index
                    val = model.data(idx)  # get ycol value
                    try:
                        val = float(val)    # get numeric ycol value
                    except:
                        val = sum(ord(c) for c in val) % 255    # calculate hash
                else:
                    val = 0
                drow.append(val)                # 3: ycol value (for each col)

            drows.append(drow)      # row complete, store in drows

        if not drows:
            drows = [[1,str,1,1]]
        data = list(map(list, zip(*drows)))     # transpose rows to columns

        # print('row', data[0])
        # print('type', data[1])
        # print('xcol', data[2])
        # print('ycol1', data[3])

        self.setWindowTitle('[{}] Rows {} to {} ({:.1f}%)'.format(model.table, r_beg + 1, r_end, 100*((r_end-r_beg)/model.rowCnt)))

        # set xdata (graph x-axis data)
        if  self.chkXNumeric.isChecked():
            xdata = [val.timestamp() if type(val) == datetime.datetime else val for val in data[2] ]     # numeric values
        else:
            xdata = data[0]     # row numbers

        # set graph x range
        range_min = min(xdata)
        range_max = max(xdata)
        self.qcp.xAxis.setRange(range_min, range_max)

        # set tick labels
        ticks = [(xdata[r], data[2][r]) for r in xrange(0, len(xdata), MAX_TICKS)]      # get evenly distributes (x, field) pairs

        dates = [t[1].timestamp() for t in ticks if type(t[1]) == datetime.datetime]
        delta = (max(dates) - min(dates)) if len(dates) > 1 else 0
        DATE, TIME, SEC, FRAC = '%Y-%b-%d', '   %H:%M', ':%S', ':%S.%f'
        formats = [(24*3600, DATE), (3600, DATE + TIME), (60, DATE + TIME + SEC), (1, DATE + TIME + FRAC), (-1, DATE + TIME + FRAC)]
        dat_frmt = next(x[1] for x in formats if delta > x[0])

        ticks = [(t[0], t[1].strftime(dat_frmt)) if type(t[1]) == datetime.datetime else (t[0], str(t[1])) for t in ticks[:]]     # format field if date, else convert to string

        self.qcp.xAxis.setAutoTicks(False)       # no auto ticks
        self.qcp.xAxis.setAutoTickLabels(False)  # no auto tick labels
        self.qcp.xAxis.setTickVector([t[0] for t in ticks])     # row numbers or numeric x values
        self.qcp.xAxis.setTickVectorLabels([t[1] for t in ticks])    # field values as labels

        # set x label color
        r_size = r_end - r_beg
        switch = [r_size > MAX_ROWS, r_size > MAX_TICKS, True].index(True)
        self.qcp.xAxis.setLabelColor(self.range_colors[switch])

        # set current row marker
        idx = self.result.view.currentIndex()
        row = clip(model.top_row, idx.row(), model.bottom_row)    # clip between top_row and bottom_row
        # row = clip(0, len(xdata) * (row - r_beg) / (r_end-r_beg), len(xdata)-1) # clip between 0 and xdata length
        row = round(len(xdata) * (row - r_beg) / (max(1, r_end-r_beg)))
        if row >= 0 and row < len(xdata):
            cur_x = xdata[row]
            self.line.start.setCoords(cur_x, 10)    # draw line
            self.line.end.setCoords(cur_x, self.qcp.axisRect().bottom())

        # set ydata
        series = model.ycols if model.ycols else ['def y']
        for plot, ycol, ydat in zip(plots, series, data[3:]):
            plot.setData(xdata, ydat)

        if self.chkAutoScaleY.isChecked():
            self.qcp.yAxis.rescale(True)

        self.qcp.replot()
        self.chkAutoScaleY.move(self.qcp.axisRect().left(), 28)

class dialogClass(QDialog):
    def __init__(self, parent=None, table='', colnam=''):
        super(dialogClass, self).__init__(parent, Qt.WindowTitleHint | Qt.WindowCloseButtonHint)
        self.parent = parent
        self.table = table
        self.colnam = colnam
        self.mode = ''          # mode = 'Change', 'Lookup', 'Shift', 'UTC', ...

        vbox = QVBoxLayout()
        self.labDescr = QLabel()
        vbox.addWidget(self.labDescr)

        form = QFormLayout()

        self.lab1 = QLabel()  # row 1: label + combo box
        self.cmb1 = QComboBox()
        form.addRow(self.lab1, self.cmb1)

        self.lab2 = QLabel()  # row 2: label + line edit
        self.led2 = QLineEdit()
        self.lab3 = QLabel()  # row 3: label + line edit
        self.led2.setClearButtonEnabled(True)
        form.addRow(self.lab2, self.led2)

        self.led3 = QLineEdit()
        self.led3.setClearButtonEnabled(True)
        form.addRow(self.lab3, self.led3)

        self.lab4 = QLabel()  # row 4: label + combo box
        self.cmb4 = QComboBox()
        form.addRow(self.lab4, self.cmb4)

        hbox = QHBoxLayout()
        self.btnB = QPushButton("Undo")
        self.btnC = QPushButton("OK")
        hbox.addWidget(self.btnB)
        hbox.addWidget(self.btnC)

        self.btnA = QPushButton("Apply")
        form.addRow(self.btnA, hbox)  # row 5: 3x button

        vbox.addLayout(form)
        self.setLayout(vbox)
        self.setWindowTitle(app_name)

        self.labDescr.setText("For column <b>{}</b> of table <b>{}</b>".format(colnam, table))
        self.lab1.setText('Update mode')
        self.cmb1.insertItems(0, ['Change', 'Lookup', 'Shift', 'UTC', 'Sequence', 'Right Aligned'])

        self.cmb1.currentIndexChanged.connect(self.index_changed)
        self.btnA.clicked.connect(self.apply)
        self.btnB.clicked.connect(self.undo)
        self.btnC.clicked.connect(self.ok)

        self.index_changed()
        self.show()

    def closeEvent(self, event):
        common.db.rollback()
        self.parent.refresh_results()

    def index_changed(self):
        self.mode = self.cmb1.currentText()
        [wid.hide() for wid in (self.lab2, self.led2, self.lab3, self.led3, self.lab4, self.cmb4)]  # hide all

        if self.mode == 'Lookup':
            [wid.show() for wid in (self.lab4, self.cmb4)]  # cmb4
            self.lab4.setText('Lookup in table...')
            self.cmb4.clear()
            self.cmb4.insertItems(0, common.db.db_tables())
        if self.mode == 'Change':
            [wid.show() for wid in (self.lab2, self.led2)]  # led2
            self.lab2.setText('Change to...')
            self.led2.setText('"{}"'.format(self.colnam))
        if self.mode == 'UTC':
            pass
        if self.mode == 'Sequence':
            pass
        if self.mode == 'Right Aligned':
            pass
        if self.mode == 'Shift':
            [wid.show() for wid in (self.lab2, self.led2)]  # led2
            self.lab2.setText('Shift by...')


    def apply(self):
        if self.mode == 'Lookup':
            lup_expr = 'lup("{}", "{}")'.format(self.colnam, self.cmb4.currentText())  # lup(col, lup_table)
        if self.mode == 'Change':
            lup_expr = self.led2.text()                 # expression
        if self.mode == 'UTC':
            lup_expr = 'utc("{}")'.format(self.colnam)  # utc(col)
        if self.mode == 'Sequence':
            lup_expr = '"_rowid_"'                      # _rowid_
        if self.mode == 'Right Aligned':
            cur = common.db.conn.execute('SELECT max(length("{}")) FROM "{}"'.format(self.colnam, self.table))
            row = cur.fetchone()
            lup_expr = '"{{:>{}}}".format("{}")'.format(row[0] if row else 0, self.colnam)  # "{:>max}".format(col)
        if self.mode == 'Shift':
            lup_expr = 'evalx("{}") + evalx(\'{}\')'.format(self.colnam,
                                                            self.led2.text())  # evalx(col) + evalx(val1)

        common.db.update(self.table, self.colnam, lup_expr)
        self.parent.refresh_results()

    def undo(self):
        common.db.rollback()
        self.parent.refresh_results()

    def ok(self):
        common.db.commit()
        self.accept()

    # class frmGraphClass(QMainWindow, Ui_widGraph):
    #     screen = QGuiApplication.primaryScreen()
    #
    #     zoom = 0
    #     model = None  # model assigned to graph
    #
    #     def __init__(self, parent):
    #         global plots  # qcp requirement!
    #         QMainWindow.__init__(self, parent)
    #         self.setupUi(self)
    #
    #         self.parent = parent
    #         plots = []
    #         self.group = QCPBarsGroup(self.qcp)
    #         self.series_colors = (QColor('blue'), QColor('red'), QColor('green'), QColor('purper'))
    #         self.range_colors = (QColor('red'), QColor('blue'), QColor('black'))
    #         self.data = []
    #
    #         self.qcp.setInteraction(QCP.iRangeDrag)
    #         self.qcp.axisRect().setRangeDrag(Qt.Vertical)
    #         self.qcp.setNoAntialiasingOnDrag(True)
    #         self.qcp.xAxis.setTickLabelRotation(50)
    #         self.qcp.legend.setVisible(True)
    #
    #         # connect signals
    #         self.qcp.mouseWheel.connect(self.hnd_mouse)
    #         self.qcp.mouseDoubleClick.connect(self.hnd_dbl_click)
    #         self.qcp.mouseMove.connect(self.show_tooltip)
    #         for obj in self.findChildren((QPushButton, QCheckBox, QRadioButton, QTextEdit)):
    #             if type(obj) == QPushButton: obj.clicked.connect(self.update_state)
    #             if type(obj) == QCheckBox: obj.clicked.connect(self.update_state)
    #             if type(obj) == QRadioButton: obj.clicked.connect(self.update_state)
    #             if type(obj) == QTextEdit: obj.textChanged.connect(self.update_state)
    #
    #         # add line item
    #         self.line = line = QCPItemLine(self.qcp)
    #         self.qcp.addItem(line)
    #         line.setPen(QPen(Qt.DashLine))
    #         line.start.setTypeX(QCPItemPosition.ptPlotCoords)
    #         line.start.setTypeY(QCPItemPosition.ptAbsolute)
    #         line.end.setTypeX(QCPItemPosition.ptPlotCoords)
    #         line.end.setTypeY(QCPItemPosition.ptAbsolute)
    #         line.setClipToAxisRect(False)
    #
    #     def update_state(self):
    #         obj = self.sender()
    #         name = obj.objectName()
    #
    #         if type(obj) == QTextEdit:
    #             self.result.states[name] = obj.toPlainText()
    #             obj.setFontWeight(QFont.Bold)
    #             # title.setFontPointSize(10)
    #             obj.resize(obj.document().size().width(), obj.document().size().height())
    #
    #         if name == 'chkStacked' and obj.isChecked():
    #             self.chkGrouped.setChecked(False)
    #         elif name == 'chkGrouped' and obj.isChecked():
    #             self.chkStacked.setChecked(False)
    #
    #         for chk in self.findChildren((QPushButton, QCheckBox, QRadioButton)):
    #             self.result.states[chk.objectName()] = chk.isChecked()
    #
    #         if type(obj) in (QPushButton, QCheckBox, QRadioButton):
    #             self.set_graph()
    #
    #     @pyqtSlot(bool)
    #     def on_btnAll_clicked(self):
    #         self.model.r_beg = 0
    #         self.model.r_end = self.model.rowCnt
    #         self.reload()
    #
    #     @pyqtSlot(bool)
    #     def on_btnView_clicked(self):
    #
    #         self.model.r_beg = self.model.top_row
    #         self.model.r_end = self.model.bottom_row
    #         self.reload()
    #
    #     @pyqtSlot(bool)
    #     def on_btnScreenshot_clicked(self):
    #
    #         screen = QGuiApplication.primaryScreen()
    #         pm = screen.grabWindow(self.qcp.winId())
    #         QApplication.clipboard().setPixmap(pm)
    #         common.set_info('Screenshot (graph) copied to clipboard')
    #
    #     def hnd_mouse(self, event):
    #         factor = 0.2 if event.angleDelta().y() > 0 else -0.2  # factor is +20% or -20%
    #         origin = (event.x() - self.qcp.axisRect().left()) / self.qcp.axisRect().width()  # relative x position
    #         origin = max(0, min(1, origin))  # clip between 0 and 1
    #
    #         if QApplication.keyboardModifiers() == Qt.ControlModifier:
    #             self.reload(zoom=factor, center=origin)  # zoom x-axis
    #         else:
    #             self.reload(shift=factor)  # shift x-axis
    #
    #     def hnd_dbl_click(self):
    #         model = self.model
    #         model.r_beg = model.top_row
    #         model.r_end = model.bottom_row
    #         self.reload()
    #
    #     def show_tooltip(self, event):
    #         pass
    #         # x_coord = self.qcp.xAxis.pixelToCoord(event.pos().x())
    #         # y_coord = self.qcp.yAxis.pixelToCoord(event.pos().y())
    #         # self.qcp.setToolTip(str(y_coord))
    #         # print('tooltip', x_coord, '-', y_coord)
    #
    #     def resizeEvent(self, event=None):
    #         title = self.txtTitle
    #         title.move(self.qcp.width() / 2 - title.width() / 2, 28)
    #
    #     def set_graph(self):
    #         global plots  # customplot needs plottables to be global
    #
    #         if not self.isVisible():
    #             return
    #
    #         table, result, model = self.parent.current(TABLE, RESULT, MODEL)
    #         if not table:
    #             self.setWindowTitle('[No table]')
    #             return
    #
    #         # set instance attributes
    #         self.result = result
    #         self.model = model
    #
    #         # init states
    #         if not result.states:
    #             for obj in self.findChildren((QPushButton, QCheckBox, QRadioButton)):
    #                 result.states[obj.objectName()] = False
    #             result.states['radBarGraphs'] = True
    #             result.states['chkAutoScaleY'] = True
    #
    #         # restore states
    #         for obj in self.findChildren((QPushButton, QCheckBox, QRadioButton)):
    #             obj.setChecked(result.states[obj.objectName()])
    #
    #         self.txtTitle.setText(result.states.get('txtTitle', '<title>'))
    #         self.txtTitle.setVisible(self.chkTitle.isChecked())
    #         self.txtTitle.setFontWeight(QFont.Bold)
    #         self.txtTitle.setAlignment(Qt.AlignHCenter)
    #         if not result.states['txtTitle']:
    #             self.txtTitle.setText(table)
    #         self.resizeEvent()
    #
    #         # auto find date column
    #         if not model.xcol:
    #             for c in range(model.columnCount()):
    #                 is_date = []
    #                 for r in range(min(model.rowCnt, 10)):
    #                     idx = model.index(r, c)  # create model index
    #                     typ = type(evalx(model.data(idx)))  # get type of column value
    #                     is_date.append(typ == datetime.datetime)
    #                 if all(is_date):  # first values of column are all dates
    #                     model.xcol = model.col_list[c]
    #                     break
    #
    #         self.qcp.xAxis.setLabel(model.xcol if model.xcol else '<Base Column>')  # set x-axis label to column name
    #
    #         # remove all plottables (graphs and bars)
    #         for p in plots[:]:
    #             self.qcp.removePlottable(p)
    #             plots.remove(p)
    #         prev = None
    #
    #         # init plottables
    #         series = model.ycols if model.ycols else ['<Data Column>']
    #         for ycol, clr in zip(series, self.series_colors):
    #
    #             if self.radLineGraphs.isChecked():  # graph
    #                 plot = self.qcp.addGraph(self.qcp.xAxis, self.qcp.yAxis)
    #             else:  # bars
    #                 plot = QCPBars(self.qcp.xAxis, self.qcp.yAxis)
    #                 self.qcp.addPlottable(plot)
    #                 plot.setWidthType(QCPBars.wtAxisRectRatio)  # width is relative to axis
    #                 plot.setWidth(.01)  # allow 100 bars
    #                 if self.chkGrouped.isChecked():
    #                     plot.setBarsGroup(self.group)  # grouped
    #                 if self.chkStacked.isChecked() and prev:
    #                     plot.moveAbove(prev)  # stacked
    #
    #             plot.setName(ycol)  # set legend to column name
    #             plot.setPen(clr)
    #             plots.append(plot)
    #             prev = plot  # save previous plot (used when stacking)
    #
    #         self.reload()  # get data
    #
    #     def reload(self, zoom=0, shift=0, center=0.5):
    #         global plots  # customplot needs plottables to be global
    #
    #         if not self.isVisible():
    #             return
    #
    #         common.set_debug('reload(), zoom={:.1f}, center={:.3f}'.format(zoom, center))
    #
    #         MAX_ROWS = 250
    #         MAX_TICKS = 30
    #
    #         model = self.model
    #         if not model or model.rowCnt == 0:
    #             self.setWindowTitle('[Empty]')
    #             return
    #
    #         # helper functions
    #         clip = lambda min, val, max: round(
    #             sorted((min, val, max))[1])  # clip(min, val, max) returns val between min and max (inclusive)
    #
    #         def xrange(start, stop, count):  # yields count evenly distributed integers from start to stop
    #             range = stop - start
    #             step = 1 if range < count else range / count
    #             while round(start) < stop:
    #                 yield round(start)
    #                 start += step
    #
    #         # set col_list (xcol, ycol1, ycol2,..)
    #         if model.xcol in model.col_list:
    #             col_list = [model.col_list.index(model.xcol)]
    #         else:
    #             col_list = [-1]
    #
    #         for ycol in model.ycols:
    #             col_list.append(model.col_list.index(ycol))
    #         if not model.ycols:
    #             col_list.append(-1)
    #
    #         # calculate new row range
    #         r_beg = model.r_beg
    #         r_end = model.r_end
    #         if zoom == 0 and shift == 0:  # sync x-axis with table view
    #             r_mid = (model.top_row + model.bottom_row) / 2
    #             r_range = r_end - r_beg
    #             r_beg = r_mid - r_range / 2
    #             r_end = r_mid + r_range / 2
    #         else:  # shift or zoom x-axis
    #             r_range = r_end - r_beg
    #             r_mid = r_beg + center * r_range  # calculate row as center for zooming
    #             r_beg = r_mid + (shift * r_range) - (1 - zoom) * (r_mid - r_beg)  # calculate begin row after shift/zoom
    #             r_end = r_mid + (shift * r_range) + (1 - zoom) * (r_end - r_mid)  # calculate end row after shift/zoom
    #
    #         # calculate clipped row range
    #         row_cnt = model.rowCnt
    #         upper = clip(0, row_cnt - r_end + r_beg,
    #                      row_cnt)  # max allowed beg row (total - range), clipped between 0 and total
    #         lower = clip(0, r_end - r_beg, row_cnt)  # min allowed end row (range), clipped between 0 and total
    #         model.r_beg = r_beg = clip(0, (r_beg), upper)  # new beg row, clipped between 0 and upper
    #         model.r_end = r_end = clip(lower, (r_end), row_cnt)  # new end row, clipped between lower and total
    #
    #         # get data (rownr, type, xcol, ycol1, ycol2,..) for r_beg to r_end
    #         drows = []
    #         for r in xrange(r_beg, r_end, MAX_ROWS):
    #             drow = []
    #
    #             # xcol value
    #             if col_list[0] > -1:
    #                 idx = model.index(r, col_list[0])  # create model index
    #                 val = evalx(model.data(idx))  # get xcol value
    #             else:
    #                 val = r + 1
    #
    #             vtype = type(val)  # get type
    #             if self.chkXNumeric.isChecked() and (
    #                     vtype == type(None) or vtype == str):  # skip non numeric xcol values (in numeric mode)
    #                 continue
    #
    #             drow.append(r)  # 0: row nr
    #             drow.append(vtype)  # 1: type
    #             drow.append(val)  # 2: field value
    #
    #             # ycol values
    #             for c in col_list[1:]:
    #                 if c > -1:
    #                     idx = model.index(r, c)  # create model index
    #                     val = model.data(idx)  # get ycol value
    #                     try:
    #                         val = float(val)  # get numeric ycol value
    #                     except:
    #                         val = sum(ord(c) for c in val) % 255  # calculate hash
    #                 else:
    #                     val = 0
    #                 drow.append(val)  # 3: ycol value (for each col)
    #
    #             drows.append(drow)  # row complete, store in drows
    #
    #         if not drows:
    #             drows = [[1, str, 1, 1]]
    #         data = list(map(list, zip(*drows)))  # transpose rows to columns
    #
    #         # print('row', data[0])
    #         # print('type', data[1])
    #         # print('xcol', data[2])
    #         # print('ycol1', data[3])
    #
    #         self.setWindowTitle('[{}] Rows {} to {} ({:.1f}%)'.format(model.table, r_beg + 1, r_end,
    #                                                                   100 * ((r_end - r_beg) / model.rowCnt)))
    #
    #         # set xdata (graph x-axis data)
    #         if self.chkXNumeric.isChecked():
    #             xdata = [val.timestamp() if type(val) == datetime.datetime else val for val in
    #                      data[2]]  # numeric values
    #         else:
    #             xdata = data[0]  # row numbers
    #
    #         # set graph x range
    #         range_min = min(xdata)
    #         range_max = max(xdata)
    #         self.qcp.xAxis.setRange(range_min, range_max)
    #
    #         # set tick labels
    #         ticks = [(xdata[r], data[2][r]) for r in
    #                  xrange(0, len(xdata), MAX_TICKS)]  # get evenly distributes (x, field) pairs
    #
    #         dates = [t[1].timestamp() for t in ticks if type(t[1]) == datetime.datetime]
    #         delta = (max(dates) - min(dates)) if len(dates) > 1 else 0
    #         DATE, TIME, SEC, FRAC = '%Y-%b-%d', '   %H:%M', ':%S', ':%S.%f'
    #         formats = [(24 * 3600, DATE), (3600, DATE + TIME), (60, DATE + TIME + SEC), (1, DATE + TIME + FRAC),
    #                    (-1, DATE + TIME + FRAC)]
    #         dat_frmt = next(x[1] for x in formats if delta > x[0])
    #
    #         ticks = [(t[0], t[1].strftime(dat_frmt)) if type(t[1]) == datetime.datetime else (t[0], str(t[1])) for t in
    #                  ticks[:]]  # format field if date, else convert to string
    #
    #         self.qcp.xAxis.setAutoTicks(False)  # no auto ticks
    #         self.qcp.xAxis.setAutoTickLabels(False)  # no auto tick labels
    #         self.qcp.xAxis.setTickVector([t[0] for t in ticks])  # row numbers or numeric x values
    #         self.qcp.xAxis.setTickVectorLabels([t[1] for t in ticks])  # field values as labels
    #
    #         # set x label color
    #         r_size = r_end - r_beg
    #         switch = [r_size > MAX_ROWS, r_size > MAX_TICKS, True].index(True)
    #         self.qcp.xAxis.setLabelColor(self.range_colors[switch])
    #
    #         # set current row marker
    #         idx = self.result.view.currentIndex()
    #         row = clip(model.top_row, idx.row(), model.bottom_row)  # clip between top_row and bottom_row
    #         # row = clip(0, len(xdata) * (row - r_beg) / (r_end-r_beg), len(xdata)-1) # clip between 0 and xdata length
    #         row = round(len(xdata) * (row - r_beg) / (r_end - r_beg))
    #         if row >= 0 and row < len(xdata):
    #             cur_x = xdata[row]
    #             self.line.start.setCoords(cur_x, 10)  # draw line
    #             self.line.end.setCoords(cur_x, self.qcp.axisRect().bottom())
    #
    #         # set ydata
    #         series = model.ycols if model.ycols else ['def y']
    #         for plot, ycol, ydat in zip(plots, series, data[3:]):
    #             plot.setData(xdata, ydat)
    #
    #         if self.chkAutoScaleY.isChecked():
    #             self.qcp.yAxis.rescale(True)
    #
    #         self.qcp.replot()
    #         self.chkAutoScaleY.move(self.qcp.axisRect().left(), 28)
    #

# === END of module===================

if __name__ == "__main__":
    pass
