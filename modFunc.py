# -------------------------------------------------------------------------------
# Name:        modFunc
# Purpose:     user functions
#
# Author:      nlrgo
#
# Created:     29-12-2015
# Copyright:   (c) nlrgo 2015
# Licence:     <your licence>
# -------------------------------------------------------------------------------

from winreg import QueryValueEx, SetValueEx, OpenKey, CreateKey, HKEY_CURRENT_USER, REG_SZ, KEY_ALL_ACCESS, CloseKey
# from PyQt5.QtWidgets import QApplication

import binascii
import datetime
import html
import os
import pickle
import regex as re
import time

import common

lup_data = {}   # global variable for use in lookup function lup {lupnam: (lupdict: tim), ...}

# == FIELD FUNCTIONS ============================================================================================


def attr(key, attributes):
    """Get attribute value for attribute key"""

    pattern = key + "\s*=\s*(?:['\"](.*?)['\"]|(\S*))"
    at = re.findall(pattern, attributes)

    return at[0][0] + at[0][1] if at else ''


def get(attr, key):
    """get attribute for key, needs to be checked/optimized"""

    matches = re.finditer('(\w+)\s*=\s*["\']?(\w*)["\']?', attr)

    pairs = {m.group(1): m.group(2) for m in matches}
    return pairs[key]


def cdate(val, fmt):
    """Convert 'val' in format 'fmt' to standard date string (YYYY-MM-DD HH:MM:SS.ffffff)"""

    val = re.sub('([+-]\d\d):(\d\d)', r'\1\2', val)  # remove possible ':' from time zone
    datim = datetime.datetime.strptime(val, fmt)  # create datetime object
    datim = datetime.datetime.utcfromtimestamp(datim.timestamp())  # convert to UTC time
    return datim.strftime("%Y-%m-%d %H:%M:%S.%f")  # return as standard formatted string


# compiles patterns for function utc
re_date = re.compile(r'^(\d{2,4})[^\d\n]*(\d{2,2})[^\d\n]*(\d{2,2})[^\d\n+-]*(\d{0,2})[^\d\n+-]*(\d{0,2})[^\d\n+-]*(\d{0,2})[^\d\n+-]*(\d{0,6})([+-]\d{2,2}[^\d\n]*\d{2,2}?)?')


def utc(date_str, offset=0):
    """Convert common date expression to datetime object"""

    val = ''
    match = re_date.findall(date_str)
    if match:  # date_str can be parsed
        match = match[0]
        Y, M, D, h, m, s, f, tz = match[0], match[1], match[2], \
                                  match[3] or '00', match[4] or '00', match[5] or '00', \
                                  match[6] or '000', \
                                  match[7] or '+0000'
        val = Y + '-' + M + '-' + D + 'T' + h + ':' + m + ':' + s + '.' + f + tz.replace(':', '')

    try:  # try if date_str is a valid date expression
        datim = datetime.datetime.strptime(val, '%Y-%m-%dT%H:%M:%S.%f%z')  # create datetime object from date expression
        datim = datetime.datetime.utcfromtimestamp(datim.timestamp())  # convert to UTC time

    except:  # try if date_str is time since epoch
        try:
            datim = datetime.datetime.fromtimestamp(float(date_str) / 1000)  # create datetime object from msecs
        except:
            datim = datetime.datetime.fromordinal(1)  # create datetime object = begin-of-epoch

    if offset:  # add/substract offset in hours
        offset = datetime.timedelta(seconds=offset * 3600)
        return datim + offset
    else:
        return datim


# compiles patterns for function evalx (24 aug 2016: added time zone)
re_date_pat = re.compile(r'(\d{4,4})[-/]?(\d{2,2})[-/]?(\d{2,2})')
re_time_pat = re.compile(r'(?:^|[\sT])(\d{2,2}):(\d{2,2})(:(\d{2,2}))?(\.\d{0,12})?Z?')
re_func_pat = re.compile(r'(now_year|now_month|now_day|now_hour|now)')
re_zone_pat = re.compile(r'([+-])(\d{2,2}):?(\d{2,2})?')
re_offs_pat = re.compile(r'(\d+)\s?(d|h|m|s)')


def evalx(expr=''):
    """Evaluate expression after expanding date formats"""

    if not expr.strip():
        return

    nu = datetime.datetime.now()    # current date/time

    expr = re_date_pat.sub(r'datetime.datetime(int("\1"),int("\2"),int("\3"))', expr)  # parse date format
    expr = re_time_pat.sub(r' + datetime.timedelta(hours=int("\1"),minutes=int("\2"),seconds=float("0\4\5"))', expr)     # parse time format
    expr = re_func_pat.sub(r'\1()', expr)      # parse date functions
    expr = re_offs_pat.sub(r' + \2(int("\1"))', expr)     # parse time offset
    expr = re_zone_pat.sub(r' -\1 datetime.timedelta(hours=int("\2"),minutes=int("0\3"))', expr)     # parse timezone

    def now():
        return nu

    def now_year():
        return datetime.datetime(nu.year, 1, 1)

    def now_month():
        return datetime.datetime(nu.year, nu.month, 1)

    def now_day():
        return datetime.datetime(nu.year, nu.month, nu.day)

    def now_hour():
        return datetime.datetime(nu.year, nu.month, nu.day, nu.hour)

    def d(nof):
        return datetime.timedelta(days=nof)

    def h(nof):
        return datetime.timedelta(hours=nof)

    def m(nof):
        return datetime.timedelta(minutes=nof)

    def s(nof):
        return datetime.timedelta(seconds=nof)

    try:
        return eval(expr)
    except:
        return str(expr)


def h2s(val):
    """hex to string"""
    try:
        even_val = '0' + val if len(val) % 2 else val  # ensure even length
        ba = binascii.unhexlify(even_val)
        return ba.decode('utf-8', 'replace')
    except:
        return "err h2s('{}')".format(val)


def s2h(val):
    """string to hex"""
    try:
        ba = bytearray(val, 'utf-8')
        return binascii.hexlify(ba).decode('utf-8')
    except:
        return "err s2h('{}')".format(val)


def h2i(val):
    """hex to integer"""
    try:
        number = int(val, 16)
        return format(number, 'd')
    except:
        return "err h2i('{}')".format(val)


def h2b(val):
    """hex to binary"""
    try:
        number = int(val, 16)
        return format(number, 'b')
    except:
        return "err h2b('{}')".format(val)


def i2b(val):
    """integer to binary"""
    try:
        number = int(val, 10)
        return format(number, 'b')
    except:
        return "err i2b('{}')".format(val)


def b2h(val):
    """binary to hex"""
    try:
        number = int(val, 2)
        return format(number, 'x')
    except:
        return "err b2h('{}')".format(val)


def to_html(val):
    """Convert entities to html"""

    return html.unescape(val)


def lup(key, table, header=False):
    """Lookup 'key' in first column of 'table' and return the value of the second column"""

    global lup_data

    lupnam = '_lup_{}_'.format(table)               # name of lookup dictionary
    lupdict, tim = lup_data.get(lupnam, (None, 0))  # get (lookup dictionary, time)

    if time.time() > tim + 2:       # lookup dictionary not used for > 2 sec

        lupdict = {}                        # create lookup dictionary

        rows = common.select(table)         # read lookup table
        for row in rows:
            row = list(row.items())         # row to list of tuples [(col1, val1), (col2, val2), ...]
            lupdict[row[0][1]] = row[1][1]   # lookup[val1] = val2

        common.set_debug("Created lookup dict for table '{}'".format(table))

    lup_data[lupnam] = (lupdict, time.time())  # store (lookup dictionary, time)

    try:
        key = str(key)
        trans = lupdict[key]  # lookup key in dictionary
        if header:
            return '{}:{}'.format(key, trans)  # key:value
        else:
            return trans
    except:
        return "{}[{}] key error".format(table, key)       # table[key] not found


def mask(val, mask_pattern):
    """Return masked val (e.g. mask('FF', '11111110') returns 254)"""

    num = int(val, 16)                  # convert hex string to integer
    return num & int(mask_pattern, 2)   # return masked integer


def state(key, table, mask_pattern='', header=False):
    """Lookup each bit of 'key' in 'table' and return the corresponding values (comma separated)
    example     key     'AB'
                value   1010 1011
                mask    0000 1111
                bit     7654 3210
                lookup bit 3, 1 and 0 in 'table'
    """

    num = int(key, 16)                  # convert hex string to integer
    if mask_pattern:
        num &= int(mask_pattern, 2)     # mask integer

    bit = 0
    trans = []      # list of translations
    while num > 0:
        if num & 1:     # bit is set
            try:
                bit_trans = lup(str(bit), table, header)    # lookup bit
                trans.append(bit_trans)
            except:
                pass

        num >>= 1   # shift bits of integer value (divide by 2)
        bit += 1    # next bit

    return ', '.join(trans)         # return comma seperated translations


def unpack(val):
    """Convert val to html and read (xml) message, to be used for enveloped xml messages"""
    from common import generate_from_text

    htm = to_html(val)
    yield from generate_from_text(htm, 0, len(htm))


def glue(msg, sel, delim='.'):
    """Concatenate previous sel number of fields or ('field1','field2',...)"""

    if type(sel) == int:
        values = [fld.value for fld in msg.fields[msg.i - sel:msg.i]]
    else:
        values = [fld.value for fld in msg.fields if fld.name in sel]

    return delim.join(values)


# == MESSAGE FUNCTIONS ============================================================================================

def split(msg, delim):
    """Split message value on delimiter"""

    words = msg.value.split(delim)      # split message

    parts = []
    for i, word in enumerate(words):    # create parts
        part = {'name': 'field' + str(i), 'id': 'split[' + str(i) + ']', 'values': (word.strip(),)}
        parts.append(part)

    msg.parts = parts
    return msg.value


def combine(msg, names):
    """Combine the values of the fields in [names] and insert as new field"""

    parts = msg.parts

    comb = []
    for name in names:      # get parts and combine
        part = next((parts.pop(i) for i, f in enumerate(parts) if f['name'] == name), None)
        comb.append(part['values'][0] if part else '')

    parts.insert(0, {'name': names[0], 'id': '.'.join(names), 'values': ('.'.join(comb),)})


def lup_comb(msg, names, table):
    """Combine the values of the fields in [names], lookup the combined value in 'table' and insert as new field"""

    parts = msg.parts

    comb = []
    for name in names:      # get parts and combine
        part = next((parts.pop(i) for i, f in enumerate(parts) if f['name'] == name), None)
        comb.append(part['values'][0] if part else '')

    comb = '.'.join(comb)
    val = lup(comb, table)  # lookup combination in table

    parts.insert(0, {'name': names[0] + '&', 'id': '.'.join(names), 'values': (val,)})


def exclude(msg, names):
    """Exclude the fields in [names]"""

    parts = msg.parts
    for name in names:      # remove part if name in names
        next((parts.pop(i) for i, f in enumerate(parts) if f['name'] == name), None)


def beumer(msg):
    """Search for 'Beumer messages' and yield sub messages if there are multiple in one line"""

    # import re
    sublen = 86

    message = msg.value

    match = re.search(' [RP]A', message)
    if match:
        beg = match.end()                   # start of content
        cnt = int(message[beg:beg + 2])     # number of sub messages
        head = message[:beg]                # get header

        if cnt > 1:
            for nr in range(cnt):
                subpos = beg + 2 + nr * sublen                                              # start of sub message
                sub = '{}x{:d}{}'.format(head, nr + 1, message[subpos:subpos + sublen])     # head + sub message
                msg.value = sub
                yield from msg.calculate_fields()                                           # yield sub message


# compiles pattern for function iata
re_iata = re.compile('^\.(.)/([^\r\n]*)', re.MULTILINE)     # parse IATA format


def iata(msg):
    """Parses IATA formatted messages"""

    parts = []
    matches = re_iata.finditer(msg.value)

    for m in matches:
        part = {'name': m.group(1), 'id': m.group(1), 'values': (m.group(2),)}
        parts.append(part)

    msg.parts = parts

    return msg.value


def subtree(msg, xpath=''):
    """Yields repeating sub trees at xpath in xml message, searches for xpath if not given"""

    base = []   # common tags
    subs = []   # sub trees
    stats = {}  # statistics

    if not xpath:  # auto find xpath of sub trees
        for part in msg.parts:  # count occurrences of xpaths
            xp = part['id']  # xpath is field id
            if xp in stats:
                stats[xp] += 1  # increment
            else:
                stats[xp] = 1   # init

        stats = [k for k, v in stats.items() if v > 1]   # find repeating xpaths
        xpath = sorted(stats)[0] if stats else ''      # select shortest xpath

    if xpath:  # xpath given or found automatically
        for part in msg.parts:
            xp = part['id']              # xpath is field id
            if xp.startswith(xpath):
                if xp == xpath:        # sub tree starts here
                    tree = []           # init sub tree
                    subs.append(tree)   # and append to trees
                tree.append(part)        # append fld to sub tree
            else:
                base.append(part)        # append fld to common tags

        nof_subs = str(len(subs))

        for i, tree in enumerate(subs):   # yield the sub trees (prepend base)
            info = str(i+1) + '/' + nof_subs + ' : ' + xpath                               # set information
            info_part = {'id': 'subtree info', 'name': 'Subtree Info', 'values': (info,)}   # and add to field
            parts = [info_part] + base + tree
            msg.parts = parts

            yield from msg.calculate_fields()
    else:
        yield from msg.calculate_fields()


def start_load():
    """Start loading messages according to current configuration"""

    common.set_info('Start load...')
    common.ldr.do_load(load=True)  # load to database
    common.set_info('{} messages read'.format(common.ldr.messages_loaded))


def start_repeat():
    """Start repeat loading messages according to current configuration"""

    common.set_info('Start repeat...')

    if common.gui:      # GUI available
        common.gui.btnRepeat.setChecked(True)
        common.gui.actionRepeat.setChecked(True)

        while common.gui.actionRepeat.isChecked() and common.gui.btnRepeat.isChecked():

            common.ldr.do_load(tail=True)  # load continued to database

            if common.cancel:
                common.gui.actionRepeat.setChecked(False)

            start = time.time()
            while time.time() < (start + 0.5):
                # QApplication.processEvents()
                time.sleep(0.01)

        common.gui.btnRepeat.setChecked(False)
        common.gui.actionRepeat.setChecked(False)

    else:               # Console

        while True:
            common.ldr.do_load(tail=True)  # load continued to database
            common.set_info('{} messages read'.format(common.ldr.messages_loaded))
            time.sleep(0.5)


def file_append(msg):
    """Append field values of message 'msg' to csv file"""

    with open(msg.name, 'a') as f:
        values = [fld.value for fld in msg.fields]
        f.write(','.join(values))
        f.write('\n')
    return msg.value


# ==PRIVATE FUNCTIONS========================================================================================

def sizeof_fmt(num, suffix='b'):
    """Helper function to display file sizes"""
    for unit in ['', 'k', 'M', 'G', 'T', 'P', 'E', 'Z']:
        if num == 0:
            return "None"
        elif abs(num) < 1024.0:
            return "%3.1f %s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)


def list_get(lst, attr, value):
    # return obj from lst (list) if obj.attr==value (return None if attribute or object not found)

    return next((obj for obj in lst if getattr(obj, attr, None) == value), None)


def unique_name(name, group):
    """Return name, possibly extended with a sequence number (1,2,...) to be unique (not in group)"""

    try:
        names = [g.name for g in group]
    except:
        names = [name for name in group]

    new = lambda name, i: name + str(i or '')  # assemble name + sequence number

    new_name = next(new(name, i) for i in range(len(names) + 1) if new(name, i) not in names)  # unique name

    return new_name


def read_reg(name, default):
    """Read name from registry"""

    sub_key = r'Software\Freave\Logwise'

    try:
        key = OpenKey(HKEY_CURRENT_USER, sub_key, 0, KEY_ALL_ACCESS)
        val = QueryValueEx(key, name)[0]
    except:
        val = default
        key = CreateKey(HKEY_CURRENT_USER, sub_key)
        SetValueEx(key, name, 0, REG_SZ, default)
    CloseKey(key)
    return val


def write_reg(name, value):
    """Write name to registry"""

    sub_key = r'Software\Freave\Logwise'

    try:
        key = OpenKey(HKEY_CURRENT_USER, sub_key, 0, KEY_ALL_ACCESS)
        SetValueEx(key, name, 0, REG_SZ, value)
    except:
        key = CreateKey(HKEY_CURRENT_USER, sub_key)
        SetValueEx(key, name, 0, REG_SZ, value)
    CloseKey(key)


def save_config_to(config_file):
    """Save the current configuration to file"""

    from modClasses import settingsClass

    if not os.path.isdir('configs'):  # folder doesn't exist
        os.makedirs('configs')

    with open(config_file, "wb") as output:

        settings = settingsClass()
        settings.values = common.settings

        pickle.dump(settings, output, -1)
        for obj in common.locations:
            pickle.dump(obj, output, -1)
        for obj in common.messages:
            pickle.dump(obj, output, -1)
        for obj in common.scripts:
            pickle.dump(obj, output, -1)


def load_config_from(config_file, full=True):
    """Load the configuration from file"""

    from modClasses import locationClass

    if not os.path.isfile(config_file):
        common.set_error("Configuration '{}' not found".format(config_file))
        return

    if full:
        common.settings = {}
        common.locations = []
        common.messages = []
        common.scripts = []

    with open(config_file, "rb") as inp:

        while inp:
            try:
                obj = pickle.load(inp)
                if full and obj.type == 'settings': common.settings = obj.values
                if full and obj.type == 'location': common.locations.append(obj)
                if obj.type in ['line', 'multi', 'xml']: common.messages.append(obj)
                if obj.type in ['query', 'python']: common.scripts.append(obj)
                common.set_debug('obj loaded: {}'.format(obj))
            except Exception as err:  # no more objects
                common.set_debug('Load error', err)
                break

        if common.cmd_files:    # files specified as command line arguments
            common.locations = []   # reset locations

            for file in common.cmd_files:   # add new locations
                new_loc = locationClass(file)
                common.locations.append(new_loc)

            common.cmd_files = []   # reset command line specification

    [msg.do_compile() for msg in common.messages]  # recompile messages after opening config
    common.set_info("Config opened: '{}'".format(config_file))

    # exec_script('#startup.*')  # run script at startup


if __name__ == '__main__':
    import modDB
    db = modDB.DatabaseClassSqlite(r'd:\Users\nlrgo\Desktop')

    # print(evalx("2016-04-19T12:15:22.1234567"))
    # print(evalx('Weight'))
    # print(mask('001', '0'))
    # print('state', state('02', 'new', '11111111', True))

    # print(evalx("2016-04-19T12:15+02:00"))
    print(utc("2016-04-19T12:15+02:00"))
    print(utc("2016-04-19T12:15:00"))
    print(utc("2016-04-19T12:15:00Z"))
