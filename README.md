# README #

Logwise - "Get wiser from your log files!"

### What is this repository for? ###

* Logwise is a Python/Qt project to parse, load and analyse log files
* Current version: 1.3


### How do I get set up? ###

* The application is tested with Python 3.4
* Configuration not needed
* Dependencies: Python 3.4, Qt5.5
* Database configuration: None (SQlite)
* Run sources with installed Python or create executable with e.g. *pyinstaller*
* Quick start: Select one or more xml files in the files tab and press F5 (Load)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* writinghand@gmail.com