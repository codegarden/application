Model
    self.xcol = ''  # column name as graph x-axis
    self.ycols = []  # column names as y-axes
    self.topRow = -1
    self.bottomRow = -1
    self.r_beg = 0
    self.r_end = 30

def set_graph   (on display_view, set_x, set_y, graph_show, update_state)

def reload  (on layout_changed, btn_all, btn_view, mouse_wheel, dbl_click, set_graph)
    col_list    # list if colulmn indexes to plot [x, y1, y2,..] or [-1,-1]

    r_beg, r_end    # plot range x-axis (begin row, end row)

    drow    # list of row values: [rownr, type, xcol, ycol1, ycol2,..] for r_beg to r_end
    drows   # list of rows
    data    # list of columns: rownrs, types, x-values, y1-values, y2-values, ...
    xdata   # x-axis values: data[2] if numeric else data[0]
    # set graph x range
    # set tick labels
    # set label color, row marker
    data[3], data[4], ...   # y-data



