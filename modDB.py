# -------------------------------------------------------------------------------
# Name:        modDB
# Purpose:     interface to Sqlite database
#
# Author:      nlrgo
#
# Created:     12-10-2013
# Copyright:   (c) nlrgo 2013
# Licence:     <your licence>
# -------------------------------------------------------------------------------

import sys, time, os, csv, regex as re
import sqlite3, collections

from modFunc import *  # possibly used in user scripts
import common

class DatabaseClassSqlite():
    def __init__(self, dbase_folder):
        self.connect(dbase_folder)

        # export to common
        for attr in dir(self):
            if not attr.startswith('__'):
                setattr(common, attr, getattr(self, attr))

    def connect(self, dbase_folder):
        self.conn = sqlite3.connect(os.path.join(dbase_folder, 'logwise.db'))
        self.conn.isolation_level = 'DEFERRED'
        self.conn.row_factory = sqlite3.Row

        # self.conn.set_progress_handler(self.progress_handler, 1000)

    # def progress_handler(self):
    #     QApplication.processEvents()        # allow interupt of sql operation is evil! Don't you dare!
    #     if common.cancel:  # abort query
    #         common.cancel = False
    #         return True
    #     else:           # continue
    #         return False

    def commit(self):
        """Commit current transaction"""
        self.conn.commit()

    def rollback(self):
        """Rollback any changes since last commit"""
        self.conn.rollback()

    def query(self, sql_script, table='', commit=False):
        """Execute query and return [rows] or, if 'table' specified, drop 'table', execute each statement in 'sql_script' and store results in 'table'"""

        match = re.search('{(.+?)}', sql_script)
        if match:   # substitute {table} for "table"."col1"||','||"table"."col2" ...
            tab = match.group(1)
            col_nams = ['"{}"."{}"'.format(tab, col) for col in self.column_names(tab)]
            record = "||','||".join(col_nams)
            sql_script = sql_script.replace(match.group(0), record)


        table = str(table.strip())
        rows = []

        if table:
            self.conn.execute('DROP TABLE IF EXISTS "{}"'.format(table))  # DROP table
            sql_list = [sql.strip() for sql in sql_script.split(';') if sql.strip()]  # split to list of SQL statements

            for sql in sql_list:
                createSql = 'CREATE TABLE "{}" AS {}'.format(table, sql)  # assemble create statement
                try:  # execute to table
                    self.conn.execute(createSql)

                except sqlite3.Error as err:  # execute DDL
                    try:
                        self.conn.execute(sql)
                    except sqlite3.Error as err:
                        raise err
        else:
            try:  # execute query
                cur = self.conn.execute(sql_script.strip())

            except sqlite3.Error as err:
                raise err

            [rows.append(collections.OrderedDict(row)) for row in cur.fetchall()]

        if commit:
            self.commit()   # query
        return rows

    def insert(self, table, rows, commit=False):
        """Insert [rows] to 'table'"""

        if type(rows) == dict:  # single row (dictionary)
            self.insert_dict(table, rows.keys(), rows.values())
        else:
            for row in rows:  # list of rows (dictionaries)
                self.insert_dict(table, row.keys(), row.values())

        if commit:
            self.commit()   # insert

    def select(self, table, columns='*', where='', groupby='', orderby=''):
        """Select 'columns' from 'table' 'where' 'groupby' 'orderby'"""

        table = str(table.strip())
        if type(columns) == str:
            columns = columns.split(',')  # string to list
        columns = [c.strip() if ('*' in c) else ('"{}"'.format(c.strip())) for c in columns]  # strip and quote items (except for *)
        columns = ','.join(columns)  # list to csv string

        sql = 'SELECT {} FROM "{}"'.format(columns, table)
        if where.strip(): sql += ' WHERE ' + where.strip()
        if groupby.strip(): sql += ' GROUP BY ' + groupby.strip()
        if orderby.strip(): sql += ' ORDER BY ' + orderby.strip()

        return self.query(sql)

    def update(self, table, column, expr, where='', commit=False):
        """Update 'column' of 'table' 'where'"""

        vars = set(re.findall('".*?"', expr))  # find unique column names
        vars = {c: 'py_var_{}'.format(v) for v, c in enumerate(vars)}  # create dict {column:pyvar, ...}
        for c, v in vars.items():  # substitute column names with variable
            expr = expr.replace(c, v)

        python_fun = 'def py_updatefunc({}):\n'.format(','.join(vars.values()))    # create py_fun definition
        python_fun += '\ttry:\n'
        python_fun += '\t\treturn str({})\n'.format(expr)                   # implementation
        python_fun += '\texcept Exception as err:\n'
        python_fun += '\t\treturn repr(err)\n'                              # exception
        common.set_debug(python_fun)

        try:
            exec(python_fun, globals())                                         # execute definition
            self.conn.create_function('_updatefunc', len(vars), py_updatefunc)                 # create sql function
        except Exception as err:
            common.set_error('update() ' + repr(err))
            return

        assignment = ('"{}" = _updatefunc({})'.format(column, ','.join(vars)))      # "column" = fun("column", "column",..)
        sql = 'UPDATE "{}" SET {}'.format(table, assignment)                # make sql statement
        if where.strip():
            sql += ' WHERE ' + where.strip()

        exec('from modClasses import *', globals())  # load common names to global
        exec('from modFunc import *', globals())  # load common names to global
        self.conn.execute(sql)

        if commit:
            self.commit()   # update

    def row_count(self, table):
        """Return number of rows in 'table'"""

        table = str(table.strip())
        try:
            cur = self.conn.execute('SELECT max(_rowid_) FROM "{}"'.format(table))
            row = cur.fetchone()
            return int(row[0])
        except:
            return 0

    def db_tables(self):
        """Return list of names of all tables"""
        try:
            cur = self.conn.execute('SELECT name FROM "sqlite_master" WHERE type = "table"')
            return [row[0] for row in cur.fetchall()]
        except sqlite3.Error as err:
            common.set_debug('db_tables()', err)
            return []

    def column_names(self, table):
        """Return list of names of all columns in 'table'"""

        table = str(table.strip())
        try:
            cur = self.conn.execute('SELECT * FROM "{}" LIMIT 1'.format(table))
            return [f[0] for f in cur.description]
        except:
            return []

    def create_field_index(self, table, col):
        """Create index for column 'col' of 'table'"""

        table = str(table.strip())
        sql = 'CREATE INDEX IF NOT EXISTS "I_{0:s}_{1:s}" ON "{0:s}" ("{1:s}") '.format(table, col)

        try:
            self.conn.execute(sql)
        except sqlite3.Error as err:
            common.set_error('create_field_index() ' + repr(err))
            return

        common.set_info("Index created on '{0:s} ({1:s})'".format(table, col))

    def create_table_indexes(self, table):
        """Create index for each column of 'table'"""

        table = str(table.strip())

        for col in self.column_names(table):
            self.create_field_index(table, col)

    def drop_table_indexes(self, tables=''):
        """Drop all indexes of all tables in [tables]"""

        if not tables:
            tables = self.db_tables()

        if type(tables) == str: tables = tables.split(',')  # string to list

        for table in tables:
            indexes = self.conn.execute('SELECT name FROM "sqlite_master" WHERE type = "index" AND tbl_name = "{}"'.format(table.strip()))
            for index in indexes:
                if index:
                    self.conn.execute('DROP INDEX IF EXISTS "{}"'.format(index['name']))

    def drop_tables(self, tables):
        """Drop all tables in [tables]"""

        if type(tables) == str: tables = tables.split(',')  # string to list

        for table in tables:
            sql = 'DROP TABLE IF EXISTS "{}"'.format(str(table.strip()))
            self.conn.execute(sql)

    def importCSV(self, file='import.csv', table='', delim=',', quote='"', term='\r\n', head=False):
        """Import csv 'file' to 'table'"""

        if not table:
            table = os.path.basename(file).split('.')[0]  # get filename before the first dot
            table = re.sub('^(?=\d)|[^\w ]', '_', table)  # change invalid characters to underscores

        # specify the dialect
        dial = csv.Dialect
        dial.delimiter = delim
        dial.doublequote = True
        dial.quoting = csv.QUOTE_MINIMAL  # NONE, MINIMAL, NONNUMERIC, ALL
        dial.quotechar = quote
        dial.lineterminator = term

        file = os.path.join(os.path.dirname(common.current_config_file), file)
        if not os.path.isfile(file):  # file doesn't exist
            common.set_error("File '{}' to import doesn't exist".format(file))
            return

        self.conn.execute('DROP TABLE IF EXISTS "{}"'.format(table))

        cnt = 0
        with open(file, newline='') as fi:

            csv_reader = csv.reader(fi, dialect=dial)  # read first line

            try:
                row = next(csv_reader)
            except:
                common.set_error("File is empty")
                return

            if head:  # first line are field names
                fieldnames = row
            else:  # set fieldnames to standard field names (Col1, Col2,...)
                fieldnames = ["Col" + str(i) for i in range(len(row))]
                self.insert_dict(table, fieldnames, row)
                cnt += 1

            for row in csv_reader:
                self.insert_dict(table, fieldnames, row)
                cnt += 1

        self.commit()   # import

        common.set_info("Imported {:d} line(s) from '{:s}' to table '{:s}'".format(cnt, os.path.basename(file), table))

    def exportCSV(self, table='export', file='', delim=',', quote='"', term='\r\n', head=False):
        """Export 'table' to csv 'file'"""

        if not file:
            file = table + '.csv'  # file name = table.csv

        # specify the dialect
        dial = csv.Dialect
        dial.delimiter = delim
        dial.doublequote = True
        dial.quoting = csv.QUOTE_MINIMAL  # NONE, MINIMAL, NONNUMERIC, ALL
        dial.quotechar = quote
        dial.lineterminator = bytes(term, "utf-8").decode("unicode_escape")  # unescape

        cur = self.conn.execute('SELECT * FROM "{}"'.format(table))
        row = cur.fetchone()  # fetch first row
        if not row:  # table is empty
            common.set_error("Table '{}' is empty".format(table))
            return

        fieldnames = row.keys()

        cnt = 1
        file = os.path.join(os.path.dirname(common.current_config_file), file)
        with open(file, "w", newline='') as fo:
            csv_writer = csv.writer(fo, dialect=dial)

            if head:  # write field names
                csv_writer.writerow(fieldnames)
                cnt += 1
            csv_writer.writerow(list(row))  # write first line of field values

            for row in cur.fetchall():  # write next lines of field values
                csv_writer.writerow(list(row))
                cnt += 1

        common.set_info("Exported {:d} line(s) from table '{:s}' to '{:s}'".format(cnt, table, os.path.basename(file)))

    def insert_msg(self, msg):
        """Insert field values of 'msg' to table (create if not exists)"""

        if common.settings.get('mapped', False):  # mapped: table per message
            table = msg.name.strip()
        else:           # combined/merged: single table for all messages
            table = 'LOGDATA'

        if not common.settings['merged']:  # keep fieldnames (fieldname1, fieldname2, ...)
            row = collections.OrderedDict(('"{}"'.format(fld.name), '"{}"'.format(fld.value)) for fld in msg.fields if fld.value)  # build dict from quoted name,value pairs
        else:                           # default fieldnames (col1, col2, ...)
            row = collections.OrderedDict((('col' + str(nr)), '"{}"'.format(fld.value)) for nr, fld in enumerate(msg.fields) if fld.value)  # build dict from quoted colx,value pairs

        sql = 'INSERT INTO "{}" ({}) VALUES ({})'.format(table, ','.join(row.keys()), ','.join(row.values()))

        try:  # insert
            # TODO ValueError: the query contains a null character
            self.conn.execute(sql)
            return  # succes, done!

        except sqlite3.Error as err:
            if 'no such table' in err.args[0]:  # table doen't exist
                try:  # create table
                    cols = ['{} DEFAULT ""'.format(col) for col in row.keys()]
                    self.conn.execute('CREATE TABLE "{}" ({})'.format(table, ','.join(cols)))
                    common.set_debug("table '{}' created".format(table))
                except sqlite3.Error as err:
                    common.set_debug('insert_msg().create_table: ', err)

            elif 'no column named' in err.args[0]:  # column doesn't exist
                for col in row.keys():
                    try:  # create columns
                        self.conn.execute('ALTER TABLE "{}" ADD COLUMN {} DEFAULT ""'.format(table, col))
                        common.set_debug('column "{}".{} added'.format(table, col))
                    except sqlite3.Error as err:
                        if not 'duplicate column name' in err.args[0]:
                            common.set_debug('insert_msg().add_column: ', err)

        try:  # now try again
            self.conn.execute(sql)
            return  # succes, done!
        except sqlite3.Error as err:
            common.set_debug("insert_msg(): {} in '{}'".format(err, sql))   # should never happen

    def insert_dict(self, table, cols, vals):
        """Insert [vals] to [cols] of 'table'"""

        table = str(table.strip())
        lencols = len(cols);
        lenvals = len(vals)
        if lencols > lenvals:  # add values ('')
            for v in range(lenvals, lencols):
                vals.append('')
        elif lenvals > lencols:  # add columns (Colx)
            for c in range(lencols, lenvals):
                cols.append('Col' + str(c + 1))

        # insert
        vals = [str(fld).replace('"', "'") for fld in vals]  # replace to single quotes
        vals = ['"{}"'.format(val) for val in vals]  # quote values
        cols = ['"{}"'.format(col) for col in cols]  # quote colummns
        sql = 'INSERT INTO "{}" ({}) VALUES ({})'.format(table, ','.join(cols), ','.join(vals))

        try:
            self.conn.execute(sql)
            return  # succes, done!

        except sqlite3.Error as err:
            if 'no such table' in err.args[0]:  # table doen't exist
                try:  # create table
                    cols = ['{} DEFAULT ""'.format(col) for col in cols]
                    self.conn.execute('CREATE TABLE "{}" ({})'.format(table, ','.join(cols)))
                    common.set_debug("table '{}' created".format(table))
                except sqlite3.Error as err:
                    common.set_debug(err)
                    raise RuntimeError('insert_dict()') from err

            elif 'no column named' in err.args[0]:  # column doesn't exist
                for col in cols:
                    try:  # create columns
                        self.conn.execute('ALTER TABLE "{}" ADD COLUMN {} DEFAULT ""'.format(table, col))
                    except sqlite3.Error as err:
                        if not 'duplicate column name' in err.args[0]:
                            raise RuntimeError('insert_dict()') from err

        try:  # now try again
            self.conn.execute(sql)
            return  # succes, done!
        except sqlite3.Error as err:
            common.set_debug('insert_dict():', err)  # should never happen

    def print_table(self, table='LOGDATA', cnt=10):

        try:
            cur = self.conn.execute("SELECT count(*) FROM '" + table + "'")
            count = cur.fetchone()[0]

            cur = self.conn.execute("SELECT * FROM '" + table + "'")
            print('==> TABLE', table, count, 'records')
            for nr, row in enumerate(cur.fetchmany(cnt)):
                if nr == 0: print(row.keys())
                print(nr, list(row))

        except sqlite3.Error as err:
            print('print_table()', err)


class LogClass:
    LEVELS = ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]

    def __init__(self, database, lev=2):
        self.db = database
        self.set_level(lev)
        print("Logger initiated")

    def set_level(self, lev):
        self.level = self.LEVELS.index(lev.upper())

    def debug(self, *args):
        if self.level <= 0: self.write(args)

    def info(self, *args):
        if self.level <= 1: self.write(args)

    def warning(self, *args):
        if self.level <= 2: self.write(args)

    def error(self, *args):
        if self.level <= 3: self.write(args)

    def critical(self, *args):
        if self.level <= 4: self.write(args)

    def write(self, args):

        msg = ""
        for m in args:
            msg += str(m) + " "

        ts = time.strftime("%H:%M:%S")
        source = sys._getframe(2).f_code.co_name
        severity = sys._getframe(1).f_code.co_name.upper()
        print("[" + source + "]", severity + ":", msg)

        if not self.db.exec(
                                                                                "INSERT INTO 'logging' VALUES ('" + ts + "','" + source + "','" + severity + "','" + msg + "');"):
            print("LOG INSERT: ", self.db.lastError().text())


# === END of module===================

if __name__ == "__main__":
    pass
