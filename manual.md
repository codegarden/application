# Logwise ![logo]

##Content {#toc}

1. [Introduction](#introduction)
1. [Features](#features)
1. [Layout](#layout)
1. [Quick start](#quick-start)
 - [XML messages](#xml-messages)
 - [Line messages](#line-messages)
 - [Multi-line messages](#multi-line-messages)
 - [Buttons](#buttons)
1. [File selection](#file-selection)
1. [Configuration](#configuration)
 - [Messages](#messages)
 - [Fields](#fields)
1. [Calculation](#calculation)
 - [Regular expressions](#regular-expressions)
 - [Variables](#variables)
 - [Functions](#functions)
 - [Data objects](#data-objects)
1. [Scripting](#scripting)
 - [Database functions](#database-functions)
 - [General functions](#general-functions)
1. [Tools](#tools)
1. [Results](#results)
1. [Console](#console)
1. [Date formats](#date-formats)


## Introduction [^toc](#toc) {#introduction} 


>> **Get wiser from your log data in three steps:**  
>> 1. select files  
>> 2. load messages  
>> 3. analyse data

Ever needed to browse through your log files to get something out of it but got lost in the ocean of raw data? Logwise unlocks the information hidden in that stream of data by converting it to a structured table format and providing tools to query and present the information in a meaningful way. Necessary configuration is kept as easy as possible.

- Simply select the files that you want to scan for messages and press the button to load them
- XML messages are loaded from your text files without any configuration. Single line messages are read after minimal and easy configuration
- The messages are stored to an embedded database that can be queried with standard SQL and presented in a graphical way


## Features [^toc](#toc) {#features}

### Select files {#select-files}
  - Browse for files, drop them on the files window or open from Windows Explorer
  - Use wild-cards to specify files or folders
  - Expand to see the effective selected files (and possibly open them in a text external editor)
  - Load files sorted on file name or modification date
  - Limit the number of files to the most recent ones
  - Select only files containing specific text

### Load messages
- Specify lines, matching a regular expression, or multi-line messages to load
- Enable loading of all existing XML messages (without configuration)
- Specify post processing of the message or, in case of XML, specify the sub trees to load
- Specify which fields to split the message to and post processing of each field
- Use Python expressions, regular expressions or custom functions for processing messages and fields
- Test processing of a single message at a time to check the configuration
- Load all files, monitor progression (number of loaded messages and percentage of total files size) and cancel on any moment
- Resume loading of messages (after cancelling) or process the files incremental (loading messages in real time when files grow)

### Analyse data
- View table content instantly (only loading visible rows)
- View single records with multi line view of selected field
- Graph view on table content as bars (stacked or grouped) or lines, automatically selecting a date column as horizontal axis
- Make a screen shot of the current visible table or graph
- Auto scroll to last row (for real time data)
- Sort, filter or group data in table view and hide or move columns
- Search text in table view or specify text phrases to highlight
- Copy cells to clipboard or export/import table 
- Use custom scripts to analyse data with standard SQL or Python code (extended with database manipulation functions)
- Scripts can be run by user, at program start-up, before or after loading or in regular intervals

### Miscellaneous
- Apply one of several appealing themes 
- Specify standard fields to include in the messages (file, folder, size, …)
- Specify import/export csv format
- Open, save and merge configurations
 

##Layout [^toc](#toc) {#layout}

The screen is divided in three main areas.

### Configuration 
 The upper part of the screen holds all the configuration information in four tabs:

- **Files**. The file location specify the folder or files to be loaded. Wild-cards may be used.
- **Messages**. This configuration specifies the message types and the fields of each message. On the left are the messages listed (one per row) and on the right are the fields listed (one per column) of the currently selected message on the left.
- **Scripts**. On the left are the available scripts listed. On the right is the content visible of the currently selected script on the left. Scripts are SQL statements or Python code. The currently visible script can be edited or executed.
- **Settings**. As the name suggests, this is the place to modify the settings.

### Result 
The lower part contains the results of your actions in two views:

 - **List (tables)**. The left part is a list of result names (tables).
 - **View**. On the right is the content visible (cells) of the currently selected table on the left.
  
### Tools  
The tools pane is a dockable windows that holds several tools for indication of the load progress and working with the results.

![panes]













## Quick start [^toc](#toc) {#quick-start}
>There are three types of messages that can be retrieved: XML messages and (multi-)line messages.
>- **XML messages** the message configuration is derived from the log data during the first load (zero configuration). Basically everything between the first opening tag and the matching close tag is considered a XML messages. The preceding so called XML ‘prolog' is not taken into account. The found message types are added to the message list. Now the message configuration can be modified as needed (add or remove fields, change fieldname, alter the calculation of field values, etc.) and the messages may be loaded again according this new message specification.
>- **Line messages** the configuration (very straightforward) is done prior to the first load. Add a message, give the message a name and specify on which criterion the message should be filtered (the trigger). Then add the number of fields that you expect and configure this fields: assign a field name and specify the calculation of the field value.
>- **Multi-line messages** are configured similar to line messages except that the trigger now is entered as comma separated begin and end strings.

### XML messages {#xml-messages}
- if nessecary clear all configuration
- select one or more files in tab **Files** (click button or use shortcut)
- in tab **Messages** add a new message (click button or use shortcut)
- right click on the message and set message type to XML 
- click the load button and watch the progress (indicator, progress bar and status bar text)
- found XML message configurations (and fields) will be created automatically and added to the list. After the first load the field calculation can be changed (or more options see [Calculation](#calculation)).
- the content of the messages is loaded to tables (one table for each message type by default)
- In the **Result** pane (lower part of the screen) the tables are added to the list on the left, their content is visible in the view on the right
- in the **Script** tab add a new script, SQL type by default, right click on it will let you change the type
- now enter some SQL query and run it, (use button or shortcut) to run the complete script or only the marked/selected text
- the query output is stored in a table with the name of the script

### Line messages {#line-messages}
- again, if necessary clear all configuration
- select one or more files in tab **Files** (click button or use shortcut)
- in tab **Messages** add a new message, it is Line type by default
- give the message a name (in column **Message**) and in the column **Trigger** enter a text that must exist in the message (line) that you want to find, e.g. `'Error'`.
- Add fields (upper right screen area), give each field a name (in row **Field**) and in row **Edit** specify how the field is calculated. An integer value will take that much characters from the line (for more options see [Calculation](#calculation))
- click the load button and watch the progress (indicator, progress bar and status bar text)
- the content of the messages is loaded to tables (one table for each message type by default)
- In the **Result** pane (lower part of the screen) the tables are added to the list on the left, their content is visible in the view on the right
- in the **Script** tab add a new script, SQL type by default, right click on it will let you change the type
- now enter some SQL query and run it, (use button or shortcut) to run the complete script or only the marked/selected text
- the query output is stored in a table with the name of the script

### Multi-line messages {#multi-line-messages}
- Follow the description for Line messages but in the column **Trigger** specify the begin and end string for the message (e.g. `'BSM, ENDBSM'`) in the Trigger field.

### Buttons {#buttons}

icon|function|icon|function  
---|---|---|---
![add]|add new item|![delete]|remove item
![run]|test message|![list_files]|list selected files
![run_start]|load messages|![repeat]|repeat load
![led_red]|loading active|![stop]|stop action
![run_script]|run all script|![run_statement]|run marked/selected


## File selection [^toc](#toc) {#file-selection}

The **Files** tab gives the option to specify the location and names of the files to use as well as some selection criteria:

- Location/filename. Use the add button (or shorcut) to add new files to the list. A file dialog is shown to select one or more files. Once the files are in the list they can be changed manually. Wild-cards `'*'` and `'?'` are allowed here so that multiple files may be selected at once. Each line can be enabled/disabled to be used for loading.
- File order. Files can be processed in alphabetical order (ascending) or in modification date order (oldest to newest). It depends on the file naming which one fits best.
- Text filter. Files are scanned prior to loading and only files containing the text will be selected.
- Date filter. Only files that are newer than the specified (modification) date are selected.
- Number of files. Only the last specified number of files are selected. For example, if the file date order is selected, only the most recent files will be selected.

For all the above selection criteria the effective result can be shown with the **List Files** option (button or shortcut). A result table (named `FILES`) is shown with a list of all *selected* files and their details. Also note that the total size of the selected files is shown in the status bar.


## Configuration [^toc](#toc) {#configuration}

### Messages {#messages}
---

**Name (message)**  
In the column **Message** the name of the message can be specified. The found messages will be stored in a table with the name of this message. If the table doesn't exist yet, it will be created during loading.

**Trigger**  
Messages of any type are selected by specifying a text in the **Trigger** column (or to select all by leaving empty). 
- For Line messages this is the text that should be existing in the line in order to be selected. 
- For XML messages this specifies the root tag of the XML message to be selected.


Note: The text is a regular expression so it is possible to specify something like `'FSC.*?01'`
The trigger can be used in two ways:  

1. Pre-fix with `'='` (default for Line messages) all matches will be assigned to the same message you are currently configuring.
2. Pre-fix with `'@'` (default for XML messages) matches will be assigned to a message with a corresponding trigger. This is only valid if the trigger expression yields multiple matches. For example, if trigger is `'.*?FSC.'` the matching messages could be assigned to messages with trigger `'12:00 FSC1'`, `'12:01 FSC2'`, etc. If nessecary new messages are created automatically during loading. To limit number of possible matches, use a group expression, e.g. `'.*?(FSC.)'`


**Edit**  
Depending on the message type, the message content is one string (Line messages) or already broken up in fields (XML messages). If necessary additional calculation is possible. This is done by specifying the name of a function  in the **Edit** column of the message configuration. They are used in two ways:

1. As function - (re)calculate the message before the fields are assigned. Use pre-fix '='
2. As generator - generate one or more new messages. Pre-fix with '@'

See **Variables** and **Functions** under [Calculation](#calculation) for an overview of the options here. It is also possible to create a custom function (in  tab **Scripts**) as a Python script (`def MyFunction: ...`), see [Scripts](#scripts).

**Count**  
In the **Count** column the found number of messages is displayed in real time. At the start of a new load command the table is emptied and the count is reset (unless a paused load was continued)

### Fields {#fields}
---

**Name (field)**  
In the row **Field** the name of the field is specified. The value of the field will be stored in a table column with the name of this field. If the column doesn't exist yet, it will be created during loading.

**Id**  
The fields are identified by this text. This is only valid if the fields are created during the message calculation. For example, in the case of XML messages the text will be the xpath, e.g. `'PackageInstruction/Header/ProcessID'` of a tag in the message or if Line messages are calculated with the function split(msg), it will be `'split[0]'` for the first field. Otherwise it is left blank.

**Edit**  
If necessary additional calculation of the field value is possible. This is done by specifying the name of a function  in the **Edit** row of the field configuration. They are used in two ways:

1. As function - (re)calculate the message before the fields are assigned. Use pre-fix '='
2. As generator - generate one or more new messages. Pre-fix with '@'

See **Regular expressions**, **Variables** and **Functions** under [Calculation](#calculation) for an overview of the options here. It is also possible to create a custom function (in  tab **Scripts**) as a Python script (`def MyFunction: ...`), see [Scripts](#scripts).

**Value**  
In the row **Value** the value of the last field calculation is visible. It is this value that will be stored in the result table. Note: there is an option to test the calculations by reading messages one by one. In this case the values are not stored in the table but only shown here for inspection and debugging.

## Calculation [^toc](#toc) {#calculation}

### Regular expressions {#regular-expressions}

expression|comment|example|details
---|---|---|---
`<integer>`|	Get fixed number of characters from the message|`'18'`|	Consumes fixed number of characters
`<delimiter>`	|Get all characters up to but not including `<delimiter>`|`';'`| 	Consumes the characters up to <delimiter> plus one
`<characters>`|Get (nested) match of the regular expression|`'FSC\s(...)-in:'`|Consumes all characters of the complete regular expression


### Variables {#variables}
variable |  	purpose|example
---|---|---
`msg`|Current message. Note: all attributes of the message (and its fields) as listed in [Data objects](#data-objects) are accessible|`msg.name`, `msg.fields[0].value`
`msg[index]`|Short for `msg.fields[index]`, or use the field name: `msg['name']`
`prev`|Message that was previously read
`fld`|Current field. Note: all attributes of the field as listed in [Data objects](#data-objects) are accessible|`fld.values[0]`
`fld[index]`|Short for: `fld.values[index]`
`val`|The current value of the current field. Note: the value initially is `fld.values[0]` and is always a string|`'31'` 
`file.path`|Fully qualified path of the file|`'L:\Log\CM\message.log'`
`file.folder`|Folder of the file containing the current message|`'CM'` 
`file.name`|Name of the file containing the current message|`'message.log'` 



### Functions {#functions}

built-in functions|comment|result
:---|---|---
`'Log' + 'wise'`|literals|'Logwise'
`val[0:4]`|slicing|'Logw'
`val > 'Just another one'`|operators|True
`val.upper()`|object methods|'LOGWISE'
`len(val)`|functions|7


field functions|comment|result
:---|---|---
`attr(key, attributes)`|Get attribute 'value' for attribute 'key'|
`cdate(val, fmt)`|Convert 'val' in format 'fmt' to standard date string |`'YYYY-MM-DD hh:mm:ss.fff'`
`utc(date_str, offset=0)`|Convert common date expression to datetime object|
`evalx(expr='')`|Evaluate expression after expanding date formats: `now()`, `now_year()`, `now_month()`, `now_day()`, `now_hour()`, `d(nof)`, `h(nof)`, `m(nof)`, `s(nof)`
`h2s(val)`|hex to string, e.g. `h2s('4c6f6777697365')`|`'Logwise'` 
`s2h(val)`|string to hex, e.g. `s2h('Logwise')`|`'4c6f6777697365'`
`h2i(val)`|hex to integer, e.g. `h2i('2f')`|`'47'` 
`i2b(val)`|integer to binary, e.g. `i2b('47')`|`'101111'`
`h2b(val)`|hex to binary, e.g. `h2b('2f')`|`'101111'`
`b2h(val)`|binary to hex, e.g. `'b2h('101111')`|`'2f'` 
`to_html(val)`|Convert entities to html, e.g. `'to_html('&lt;Logwise_&euro;0,00&gt;')`|`'<Logwise_€0,00>'`
`lup(key, table, header=False)`|Lookup 'key' in first column of 'table' and return the value of the second column
`mask(val, mask_pattern)`|Return masked val e.g. `mask('FF', '11111110')`|`'254'` 
`state(key, table, mask_pattern='', header=False)`|Lookup each bit of 'key' (possibly masked by 'mask') in 'table' and return the corresponding (comma separated) values
`unpack(val)`(Generator)|Convert val to html and read (xml) message, to be used for enveloped xml messages

message functions|comment
:---|---
`glue(msg, sel, delim='.')`|Concatenate previous sel number of fields or ('field1','field2',...)
`split(msg, delim)`|Split message value on 'delim' character
`combine(msg, names)`|Combine the values of the fields in [names] and insert as new field
`lup_comb(msg, names, table`)|Combine the values of the fields in [names], lookup the combined value in 'table' and insert as new field
`exclude(msg, names)`|Exclude the fields in [names]
`beumer(msg)`(Generator)|Search for 'Beumer messages' and yield sub messages if there are multiple in one line
`iata(msg)`|Parses IATA formatted messages to fields, e.g. '.N', '.P', etc.
`subtree(msg, xpath='')`(Generator)|Generate repeating sub trees at xpath in xml message, searches for xpath if not specified
`start_load()`|Start loading messages according to current configuration
`start_repeat()`|Start repeat loading messages according to current configuration
`file_append(msg)`|Append field values of message 'msg' to csv file

### Data objects {#data-objects}

> When a message is found the message value is set to the line text (for Line messages) or the fields values are set to the tag values (in case of XML messages) without any further configuration. When using the pre-defined functions or when writing custom script functions it is good to now the data object and the processing steps as described below.

Before each load process and after each change in the configuration, the messages are **compiled** and a function is called with the message as argument, `user_compile(msg)`, that may be defined in a script. In this function the configuration can be changed: delete or create fields, define calculation steps, etc.

The selected **files are read** from begin to end (for the order of the files, see [File selection](#file-selection)). When a fragment of the text matches the message (type and trigger), the text is forwarded to the message in the form of a data dictionary. The read text fragment is stored in the value and, depending on the type of the message, already broken up in parts.

data ^\(keys)|description
---|---
`'id'`|data identifier
`'range'()`|begin and end position
`'name'`|name 
`'value'`|text value (read from the file)
`'parts'[]`|list of parts (can be empty [])

The parts are simple data dictionaries that contain name, id and values:

part ^\(keys)|description  
---|---  
`'id'`|part identifier
`'name'`|name
`'values'[]`|list of values

Now the **data is assigned to a message** where the data id matches the message trigger. If such a message doesn't exist, it is created.

In this stage a **function is called** with the message as argument, `user_calculate(msg)`, that may be defined in a script (use it to perform extended calculations).

If there is a **calculation specified for the message**, the data [parts] can be re-calculated, removed or created. For example: lines can be split in different parts (on a delimiter) or an xml message can be broken up in multiple messages (if an xpath occurs multiple times).


message ^\(attributes)|description
---|---
`trigger`|message trigger
`range()`|begin and end position
`name`|message name (messages will be stored in a table with this name)
`value`|message text
`parts[]`|data parts
`edit`|message calculation
`fields[]`|list of field objects

After that, the **parts are assigned to fields** where the part id matches the field id. If such a message doesn't exist, it is created.

If there is a **calculation specified for the field**, the part [values] can be re-calculated. For example, if it is a date string it can be converted to a standard date/time format. Default values[0] will be assigned to 'value'.

In this stage a **function is called** with the message as argument, `user_finalize(msg)`, that may be defined in a script (use it to perform extended calculations).

field ^\(attributes)|description
---|---
`id`|field identifier
`name`|name (fields will be stored in a column with this name)
`values[]`|list of field values
`edit`|field calculation
`value`|calculated field value


To summarize the steps in calculating a message:

1. *user_compile()*
1. assign data to message
1. *user_calculate()*
1. calculate message
1. assign parts to fields
1. calculate fields
1. *user_finalize()*

 
## Scripting [^toc](#toc) {#scripting}
Scripts can be used to query the database (SQL type scripts) or to perform advanced calculations (Python type scripts):
- The SQL type scripts handle standard SQL statements as understood by SQlite. The output is stored in a table with the name of the script. There is one convenience macro to use: `{<table name>}` is substituted with a concatenation of all columns in the table `"column1"||'.'||"column2"||'.'||"column3" etc.`
- Python type scripts can use all built-in functions and operations as well as all pre-defined [functions](#functions). Some of these functions make only sense when used in a function called during loading such as `def user_calculate(msg)`. Below some additional functions that are used only in scripts (database functions/general functions)


Some script are, when existing, automatically run at certain moments. If the script name ++starts with++:

- `#startup` - run at application startup
- `#load_begin` - run before loading starts
- `#load_end` - run after loading has finished
- `#script_begin` - run before another script starts
- `#script_end` - run after another script has finished

As described in [Data objects](#data-objects) some specific functions are called during the loading of messages. These functions are empty by default and may be overridden in a script (to assert this functions are always compiled put them in `#startup` or `#load_begin`). These functions are:

1. user_compile() ==> called at every configuration change and before each load
1. user_calculate() => called before a message is calculated
1. user_finalize() ===> called after message is calculated

### Database functions {#database-functions}
Database functions	|comment
:---|---
`commit(self)`|Commit current transaction
`rollback(self)`|Rollback any changes since last commit
`query(self, sql_script, table='', commit=False)`| Execute query and return [rows] or, if 'table' specified, drop 'table', execute each statement in 'sql_script' and store results in 'table'
`insert(self, table, rows, commit=False)`|Insert [rows] to 'table'
`select(self, table, columns='*', where='', groupby='', orderby='')`| Select 'columns' from 'table' 'where' 'groupby' 'orderby'
`update(self, table, column, expr, where='', commit=False)`|Update 'column' of 'table' 'where'
`row_count(self, table)`|Return number of rows in 'table'
`db_tables(self)`|Return list of names of all tables
`column_names(self, table)`|Return list of names of all columns in 'table'
`create_field_index(self, table, col)`|Create index for column 'col' of 'table'
`create_table_indexes(self, table)`|Create index for each column of 'table'
`drop_table_indexes(self, tables='')`|Drop all indexes of all tables in [tables]
`drop_tables(self, tables)`|Drop all tables in [tables]
`importCSV(self, file='import.csv', table='', delim=',', quote='"', term='\r\n', head=False)`|Import csv 'file' to 'table'
`exportCSV(self, table='export', file='', delim=',', quote='"', term='\r\n', head=False)`|Export 'table' to csv 'file'
`insert_msg(self, msg)`|Insert field values of 'msg' to table (create if not exists)

### General functions {#general-functions}
General functions	|comment
:---|---|---
`msg_box(txt)`|Show information 'txt' message box
`set_info(txt)`|Display info 'txt' in statusbar
`set_error(txt)`|Show warning 'txt' in message box
`set_debug(*txt)`|Print *txt to console (if debug is active)
`run_current(self,partial=False)`|Execute the current selected script (partial: only marked/selected code)
`current_table()`|Current selected table
`current_column()`|Current selected column

## Tools [^toc](#toc) {#tools}
The tools dockable window has some useful option for analysing the result data in the tables.

- **Load Target** - This setting determines how the the messages will be stored in tables:
  - Combined - all messages in the same table
  - Merged - also in one table but with default column names
  - Mapped - each message type will be stored in its own table

- **Highlighting**- Enter text in the search text field and add it to the highlight list with the add button. If the text is found in any field of the visible cells it will be highlighted in a distinctive color. To remove it, select the text and use the remove button. The highlighting applies for all tables.

- **Search** - Enter text in the search text field and use the next/previous button (or shortcuts) to find the text in any field of the current table. The table is searched forwards or backwards from the current selected cell. If found the cell is scrolled into the visible area.

- **Display Filter**
  - Enter text in the filter field and press return to activate the filter. Only rows containing the text (in any field) will be displayed. If the text contains spaces or symbols, surround the text with double quotes, e.g. `"This & that"`.
  - Enter a valid SQL WHERE expression and press return to only display rows that match the expression, e.g. `'column in ('a', 'b' ,'c')'`. 
  - A combination of both options above is possible when separated by the `'OR'` or `'AND'` keywords, e.g. `'error OR name = 'Logwise''`
  - The filter expression is valid for one table only.


## Results [^toc](#toc) {#results}
The results (tables) as generated by loading messages or executing scripts can be modified in several ways. Select the options from the context menu (right click on headers, result list or view).

View context menu
- `Single Record` - open window with details of the selected row
- `Graph` - display the table content as graph
- `Selection To Table` - copy selected cells to table

Horizontal header context menu
- `Rename Column`
- `Update Column` - change all values of a column
- `Group By Column` - run a GROUP BY query and copy the results to a new table
- `Insert Column(s)`
- `Delete Column(s)`
- `Index Column` - create an index for the selected column

Vertical header context menu
- `Insert Row(s)`
- `Delete Row(s)`

Result list context menu
- `Import...` - select csv file and import to new table
- `Export...` - export  selected table to csv file
- `Combine Tables` - combine the selected tables to a new table (keep column names)
- `Merge Tables` - merge selected table to a new table (default column names)
- `Index Table` - create index for each column of selected table

Tools pane options
- `Highlight` - highlight text in the result view
- `Search` - search text in every column of the table
- `Display Filter` - filter table on text or expression
- `Screenshot` - take a screenshot of the view and copy to clipboard
- `Display last row`

## Console [^toc](#toc) {#console}
Logwise can be started as GUI (default) or as console application (argument -c). Below the output as started from console with argument -h. The options are pretty self explanatory. The option -s (silent) surpresses all output to console except for errors and output generated with a print() statement. Use this option to control the output when Logwise is called by another program/script.

```
>Logwise -h
usage: Logwise [-h] [-v] [-c] [-s] [-config [CONFIG]] [files [files ...]]

Get wiser from your log data!

positional arguments:
  files             log files to load

optional arguments:
  -h, --help        show this help message and exit
  -v                show program's version number and exit
  -c                run as console (no GUI)
  -s                run silent
  -config [CONFIG]  absolute/relative path to configuration
```

## Date formats [^toc](#toc) {#date-formats}
For use in some date/time function the following format is used.

Directive	|Meaning	|Example
---|---|---
`%d`	|Day of the month as a zero-padded decimal number	|`01, 02, ..., 31`
`%m`	|Month as a zero-padded decimal number|	`01, 02, ..., 12`
`%y`	|Year without century as a zero-padded decimal number| ` 00, 01, ..., 99`
`%Y`	|Year with century as a decimal number| `0001, 0002, ..., 2013, 2014, ..., 9998, 9999`
`%H`	|Hour (24-hour clock) as a zero-padded decimal number|	`00, 01, ..., 23`
`%I`	|Hour (12-hour clock) as a zero-padded decimal number|	`01, 02, ..., 12`
`%M`	|Minute as a zero-padded decimal number| `00, 01, ..., 59`
`%S`	|Second as a zero-padded decimal number| `00, 01, ..., 59`
`%f`	|Microsecond as a decimal number, zero-padded on the left| `000000, 000001, ..., 999999`
`%z`	|UTC offset in the form +HHMM or -HHMM (empty string if the the object is naive)| `(empty), +0000, -0400, +1030`


[add]: ./icons/add186.png
[run_start]: ./icons/forward11.png
[run]: ./icons/play106.png
[repeat]: ./icons/repeat2.png
[list_files]: ./icons/documents7.png
[stop]: ./icons/stop23.png
[xml]: ./icons/xml.png
[line]: ./icons/line.png
[multi]: ./icons/multi.png
[delete]: ./icons/minus.png
[run_script]: ./icons/run_script.png
[run_statement]: ./icons/run_statement.png
[sql]: ./icons/sql.png
[python]: ./icons/python.png
[led_red]: ./icons/led_red.png
[panes]: ./icons/panes.jpg
[logo]: ./icons/blade.ico

[add]: qrc:/add
[run_start]: qrc:/run_start
[run]: qrc:/run
[repeat]: qrc:/repeat
[list_files]: qrc:/list_files
[stop]: qrc:/stop
[xml]: qrc:/xml
[line]: qrc:/line
[multi]: qrc:/multi
[delete]: qrc:/delete
[run_script]: qrc:/run_script
[run_statement]: qrc:/run_statement
[sql]: qrc:/sql
[python]: qrc:/python
[led_red]: qrc:/led_red
[panes]: qrc:/panes
[logo]: qrc:/app

