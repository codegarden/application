# -------------------------------------------------------------------------------
# Name:        common
# Purpose:     share common variables between modules
#
# Author:      nlrgo
#
# Created:     10-10-2015
# Copyright:   (c) nlrgo 2015
# Licence:     <your licence>
# -------------------------------------------------------------------------------


def msg_box(txt):
    """Show information 'txt' message box"""
    if gui:
        msg_box(txt)   # display in GUI (if available)


def set_info(txt):
    """Display info 'txt' in statusbar"""
    if gui:
        display_info(txt)   # display in status bar (if available)
    print('[Info]', txt)    # write to console


def set_error(txt):
    """Show warning 'txt' in message box"""
    global busy, cancel

    busy = False
    if gui:
        display_error(txt)  # display in GUI (if available)
    print('[Error]', txt)   # write to console
    cancel = True


def set_debug(*txt):
    """Print *txt to console (if debug is active)"""
    if settings.get('debug', True):
        print('[Debug]', ' '.join([str(t) for t in txt]))   # write to console

LOCK_FILE = 'LW_LOCK'
INBOX_FILE = 'LW_INBOX'

# configuration data
settings = {}
locations = []
messages = []
scripts = []
results = []

# status
busy = False
cancel = False

# internal
db = None
gui = None
file_index_base = 0
total_index_base = 0
default_dbase_folder = None
current_config_file = None
current_table = None
current_column = None
current_folder = None
current_path = None
current_file = None
current_msg = None
prev_msg = None

cmd_files = []                  # files specified in command line

