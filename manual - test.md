# Header 1 ![alti](qrc:/app)
## Header 2
### Header 3
#### Header 4
---                
#### Table

built-in functions|comment|example
:---|---|---
`<Python expression>`|**Python syntax**|*see below*
`'Logwise'`|(literals)|
`val[0:10]`|(slicing)|
`val<10`|(operators)|
`val.upper()`|(object methods)|Use standard Python expressions|
`abs(val)`||
`max(val,10`)|	|Use standard Python functions|



#### Unordered list
- **Simply** select the `files` that you want to scan for messages and press the button to load them
  - een
  - twee
- XML messages are loaded from your text files without any configuration. Single line messages are read after minimal and easy configuration
  1. een
  2. twee 
- The messages are stored to an embedded database that can be queried with standard SQL and presented in a graphical way

#### Ordered list
1. **Browse** for `files`, drop them on the files window or open from Windows Explorer
  - een
  - twee
2. Use wildcards to specify files or folders
  1. een
  2. twee
3. Expand to see the effective selected files (and possibly open them in a text external editor)


[link](#features)

[TOC]

## Block quote

> **block quote** (greater than)  
> select files  
> load messages  
> analyse data

### Header 3

#### Code block


    
    block code (back ticks)
    usage: Logwise [-h] [-v] [-c] [-s] [-config [CONFIG]] [files [files ...]]
    
    Get wiser from your log data!
    
    positional arguments:
      files log files to load
    
    optional arguments:
      -h, --helpshow this help message and exit
      -vshow program's version number and exit
      -crun as console (no GUI)
      -srun silent
      -config [CONFIG]  absolute/relative path to configuration
    
    
#### Code
**Ever** needed to `browse` through your log files to get something out of it but got lost in the ocean of raw data?




