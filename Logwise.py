# -------------------------------------------------------------------------------
# Name:        Logwise
# Purpose:     load and analyse messages from text files
#
# Author:      nlrgo
#
# Created:     12-10-2013
# Copyright:   (c) nlrgo 2013
# Licence:     <your licence>
# -------------------------------------------------------------------------------

import sys, os, time
import argparse, traceback
import common
from LW_BUILD_INFO import app_name, version, build


def handle_exception(exc_type, exc_value, exc_traceback):   # Qt > 5.4 no longer prints unhandled Python exceptions
    err_msg = ''.join(traceback.format_exception(exc_type, exc_value, exc_traceback))

    common.set_error('<b>Unhandled exception, application will be closed.</b><br><br>\n{}'.format(err_msg))   # write to GUI and console

    with open('error_dump', 'a') as dumpfile:
        dumpfile.writelines('\nUnhandled exception on {}\n{}'.format(time.strftime('%d/%m/%Y %H:%M:%S'), err_msg))     # write to file

    sys.exit(1)     # exit in shame

sys.excepthook = handle_exception   # Qt > 5.4 no longer prints unhandled Python exceptions


# set working directory
application_path = ''
if getattr(sys, 'frozen', False):   # frozen exe path
    application_path = os.path.dirname(sys.executable)
elif __file__:      # script file path
    application_path = os.path.dirname(__file__)

if application_path:
    os.chdir(application_path)


# parse arguments
default_config = 'configs/Default.cfg'
parser = argparse.ArgumentParser(prog=app_name, description='Get wiser from your log data!')
parser.add_argument('-v', action='version', version='{} {}, build {}'.format(app_name, version, build))
parser.add_argument('-c', dest='console', action='store_true', help='run as console (no GUI)')
parser.add_argument('-s', dest='silent', action='store_true', help='run silent')
parser.add_argument('-config', nargs='?', const=default_config, default=default_config, help='absolute/relative path to configuration')
parser.add_argument('files', nargs='*', default='', help='log files to load')
args = parser.parse_args()

# messages received check
if os.path.exists(common.LOCK_FILE) and args.files:    # lock file and args.files exist -> save to inbox and exit
    common.set_debug('Running instance receiving message(s)')
    with open(common.INBOX_FILE, 'a') as fa:
        for file in args.files:
            fa.write(str(file) + '\n')  # write inbox file
    sys.exit()

# start application ==================================================================================

from modDB import DatabaseClassSqlite
from modLoader2 import loaderClass
from modClasses import OutputClass, exec_script
from modFunc import load_config_from, read_reg

with open(common.LOCK_FILE, 'w') as f:  # write lock file
    f.write(str(os.getpid()))

if os.path.exists(common.INBOX_FILE):    # remove possible inbox file
    os.remove(common.INBOX_FILE)

common.current_config_file = args.config
common.cmd_files = args.files
common.default_dbase_folder = read_reg('dbase_folder', os.path.abspath('.'))
common.db = DatabaseClassSqlite(common.default_dbase_folder)        # todo: handle if database file (or folder) doesn't exist (anymore)
common.ldr = loaderClass(common.db)

if args.console:  # run from command line

    OutputClass(args.silent)
    load_config_from(common.current_config_file)        # load config (default if not specified)
    exec_script('#startup')                           # run script at startup
    common.set_info('{} finished'.format(app_name))     # report application finished

else:  # run with GUI
    from PyQt5.QtWidgets import QApplication
    from modGUI import FrmClass

    app = QApplication(sys.argv)                        # init Qt
    app.setStyle('fusion')

    frmMain = FrmClass(args.silent)                     # init GUI
    frmMain.load_config(common.current_config_file)     # load config (default if not specified)
    frmMain.show()                                      # show GUI

    common.busy = True
    exec_script('#startup')                           # run script at startup
    common.busy = False
    frmMain.display_messages()
    frmMain.refresh_results()

    sys.exit(app.exec_())                               # enter loop

# For console only build out comment the else block above and include the print statement below and remove QApplication from modLoader2
# and change build specfile: dont't copy stylesheets and icudt32.dll and set console = True

# print('This is the console only executable, use: Logwise.exe -c')

